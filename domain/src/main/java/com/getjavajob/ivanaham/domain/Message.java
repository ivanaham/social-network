package com.getjavajob.ivanaham.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class Message implements Serializable {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private Account author;

    @NotNull
    private String text;

    @NotNull
    @Column(name = "date_time_sent")
    private LocalDateTime dateTimeSent;

    @Embedded
    @AttributeOverride(name = "imageBytes", column = @Column(name = "attached_image"))
    @AttributeOverride(name = "name", column = @Column(name = "attached_image_name"))
    private Image attachedImage;

    public Image getAttachedImage() {
        return attachedImage;
    }

    public void setAttachedImage(Image attachedImage) {
        this.attachedImage = attachedImage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDateTimeSent() {
        return dateTimeSent;
    }

    public void setDateTimeSent(LocalDateTime dateTimeSent) {
        this.dateTimeSent = dateTimeSent;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(author, message.author) && Objects.equals(text, message.text);
    }

    @Override
    public String toString() {
        return "Message{" +
                "author=" + author +
                ", text='" + text + '\'' +
                ", dateTimeSent=" + dateTimeSent +
                ", attachedImage=" + attachedImage +
                '}';
    }

    public enum Type {

        ACCOUNT(1), GROUP(2), DIRECT(3);

        private final int value;

        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    }

}
