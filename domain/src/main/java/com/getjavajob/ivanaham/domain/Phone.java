package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.nonNull;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "phones")
public class Phone implements Serializable {

    @NotNull
    @Column(name = "phone_type_id")
    private Type type;

    @NotNull
    @Column(unique = true)
    private String number;

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    @JsonBackReference
    private Account account;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "phone_generator")
    @SequenceGenerator(name = "phone_generator", sequenceName = "phones_id_seq", allocationSize = 1)
    private long id;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return hash(type, number);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone = (Phone) o;
        return type == phone.type && number.equals(phone.number);
    }

    @Override
    public String toString() {
        Long accountId = nonNull(account) ? account.getId() : null;
        return "Phone{" +
                "id=" + id +
                ", type=" + type +
                ", number='" + number + "'" +
                ", accountId=" + accountId +
                "}";
    }

    public enum Type {

        WORK(2), HOME(1);

        private final int value;

        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    }

}
