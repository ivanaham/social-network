package com.getjavajob.ivanaham.domain.converters;

import static java.util.stream.Stream.of;

import com.getjavajob.ivanaham.domain.Message.Type;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MessageTypeConverter implements AttributeConverter<Type, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Type type) {
        if (type == null) {
            return null;
        }
        return type.getValue();
    }

    @Override
    public Type convertToEntityAttribute(Integer typeValue) {
        if (typeValue == null) {
            return null;
        }
        return of(Type.values())
                .filter(type -> type.getValue() == typeValue)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
