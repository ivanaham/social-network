package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.nonNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public class Image implements Serializable {

    private byte[] imageBytes;
    private String name;

    public byte[] getImageBytes() {
        return nonNull(imageBytes) ? imageBytes.clone() : null;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int result = hash(name);
        result = 31 * result + Arrays.hashCode(imageBytes);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Image image = (Image) o;
        return Arrays.equals(imageBytes, image.imageBytes) && Objects.equals(name, image.name);
    }

    @Override
    public String toString() {
        long imageByteLength = nonNull(imageBytes) ? imageBytes.length : 0L;
        return "Image{" +
                "imageBytes length=" + imageByteLength +
                ", name='" + name + '\'' +
                '}';
    }

}
