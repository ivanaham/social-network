package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.nonNull;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "groups")
public class Group implements Serializable {

    private static final long serialVersionUID = 12;

    @NotNull
    @Column(name = "creation_date")
    private Date creationDate;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "creator_id", referencedColumnName = "id")
    private Account creator;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "group_generator")
    @SequenceGenerator(name = "group_generator", sequenceName = "groups_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @Column(unique = true)
    private String name;

    private String description;

    @Embedded
    @AttributeOverride(name = "imageBytes", column = @Column(name = "profile_image"))
    @AttributeOverride(name = "name", column = @Column(name = "profile_image_name"))
    private Image profileImage;

    public Image getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Image profileImage) {
        this.profileImage = profileImage;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public Date getCreationDate() {
        return nonNull(creationDate) ? (Date) creationDate.clone() : null;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = (Date) creationDate.clone();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        return hash(creationDate, id, name, description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return id == group.id && Objects.equals(creationDate, group.creationDate) && Objects.equals(name, group.name)
                && Objects.equals(description, group.description);
    }

    @Override
    public String toString() {
        return "Group{" +
                "creationDate=" + creationDate +
                ", creator=" + creator +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
