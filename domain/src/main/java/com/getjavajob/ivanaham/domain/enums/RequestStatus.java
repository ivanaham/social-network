package com.getjavajob.ivanaham.domain.enums;

public enum RequestStatus {

    PENDING(1), ACCEPTED(2), DECLINED(3);

    private final int value;

    RequestStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}