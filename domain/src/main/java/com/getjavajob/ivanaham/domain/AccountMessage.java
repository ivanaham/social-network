package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "account_messages")
public class AccountMessage extends Message implements Serializable {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "account_message_generator")
    @SequenceGenerator(name = "account_message_generator", sequenceName = "account_messages_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "recipient_id", referencedColumnName = "id")
    private Account recipient;

    @NotNull
    @Column(name = "message_type_id")
    private Type messageType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Type getMessageType() {
        return messageType;
    }

    public void setMessageType(Type messageType) {
        this.messageType = messageType;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }

    @Override
    public int hashCode() {
        return hash(super.hashCode(), recipient);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AccountMessage that = (AccountMessage) o;
        return Objects.equals(recipient, that.recipient);
    }

    @Override
    public String toString() {
        Long recipientId = isNull(recipient) ? null : recipient.getId();
        Long authorId = isNull(getAuthor()) ? null : getAuthor().getId();
        return "AccountMessage{" +
                "recipientId=" + recipientId +
                ", messageType=" + messageType +
                ", authorId=" + authorId +
                ", text='" + this.getText() + '\'' +
                ", dateTimeSent=" + this.getDateTimeSent() +
                ", messageType=" + this.getMessageType() +
                ", id=" + this.getId() +
                ", attachedImage=" + this.getAttachedImage() +
                '}';
    }

}
