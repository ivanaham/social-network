package com.getjavajob.ivanaham.domain;

import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.SEQUENCE;

import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "friends", uniqueConstraints = {@UniqueConstraint(columnNames = {"account_id_a", "account_id_b"})})
public class Friendship implements Serializable {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "account_id_a", referencedColumnName = "id")
    private Account accountA;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "account_id_b", referencedColumnName = "id")
    private Account accountB;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "friendship_generator")
    @SequenceGenerator(name = "friendship_generator", sequenceName = "friends_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @Column(name = "request_status_id")
    private RequestStatus status;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "last_acted_account_id", referencedColumnName = "id")
    private Account lastActedAccount;

    public Account getAccountA() {
        return accountA;
    }

    public void setAccountA(Account accountA) {
        this.accountA = accountA;
    }

    public Account getAccountB() {
        return accountB;
    }

    public void setAccountB(Account accountB) {
        this.accountB = accountB;
    }

    public Account getLastActedAccount() {
        return lastActedAccount;
    }

    public void setLastActedAccount(Account lastActedAccount) {
        this.lastActedAccount = lastActedAccount;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountA, accountB, id, status, lastActedAccount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Friendship that = (Friendship) o;
        return id == that.id && Objects.equals(accountA, that.accountA) && Objects.equals(accountB, that.accountB)
                && status == that.status && Objects.equals(lastActedAccount, that.lastActedAccount);
    }

    @Override
    public String toString() {
        Long accountIdA = isNull(accountA) ? null : accountA.getId();
        Long accountIdB = isNull(accountB) ? null : accountB.getId();
        Long lastActedAccountId = isNull(lastActedAccount) ? null : lastActedAccount.getId();
        return "Friendship{" +
                "accountIdA=" + accountIdA +
                ", accountIdB=" + accountIdB +
                ", id=" + id +
                ", status=" + status +
                ", lastActedAccountId=" + lastActedAccountId +
                '}';
    }

}
