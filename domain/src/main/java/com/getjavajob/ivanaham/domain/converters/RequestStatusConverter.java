package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.values;
import static java.util.stream.Stream.of;

import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RequestStatusConverter implements AttributeConverter<RequestStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(RequestStatus requestStatus) {
        if (requestStatus == null) {
            return null;
        }
        return requestStatus.getValue();
    }

    @Override
    public RequestStatus convertToEntityAttribute(Integer statusValue) {
        if (statusValue == null) {
            return null;
        }
        return of(values())
                .filter(status -> status.getValue() == statusValue)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}