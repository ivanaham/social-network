package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.enums.Rights.values;
import static java.util.stream.Stream.of;

import com.getjavajob.ivanaham.domain.enums.Rights;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RightsConverter implements AttributeConverter<Rights, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Rights rights) {
        if (rights == null) {
            return null;
        }
        return rights.getValue();
    }

    @Override
    public Rights convertToEntityAttribute(Integer rightsValue) {
        if (rightsValue == null) {
            return null;
        }
        return of(values())
                .filter(rights -> rights.getValue() == rightsValue)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
