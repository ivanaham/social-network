package com.getjavajob.ivanaham.domain.enums;

public enum Rights {

    MODERATOR(1), COMMON(2);

    private final int value;

    Rights(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
