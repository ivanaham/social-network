package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.isNull;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "group_messages")
public class GroupMessage extends Message implements Serializable {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "group_message_generator")
    @SequenceGenerator(name = "group_message_generator", sequenceName = "group_messages_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "recipient_id", referencedColumnName = "id")
    private Group recipient;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Group getRecipient() {
        return recipient;
    }

    public void setRecipient(Group recipient) {
        this.recipient = recipient;
    }

    @Override
    public int hashCode() {
        return hash(super.hashCode(), recipient);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GroupMessage that = (GroupMessage) o;
        return Objects.equals(recipient, that.recipient);
    }

    @Override
    public String toString() {
        Long recipientId = isNull(recipient) ? null : recipient.getId();
        Long authorId = isNull(getAuthor()) ? null : getAuthor().getId();
        return "GroupMessage{" +
                "recipientId=" + recipientId +
                ", authorId=" + authorId +
                ", text='" + this.getText() + '\'' +
                ", dateTimeSent=" + this.getDateTimeSent() +
                ", id=" + this.getId() +
                ", attachedImage=" + this.getAttachedImage() +
                '}';
    }

}
