package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static java.util.Objects.nonNull;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

@Entity
@Table(name = "accounts")
@JsonIgnoreProperties("password")
public class Account implements Serializable {

    private static final long serialVersionUID = 15;

    @OneToMany(mappedBy = "account", fetch = EAGER, cascade = {REMOVE, MERGE}, orphanRemoval = true)
    @JsonManagedReference
    @JacksonXmlProperty(localName = "phone")
    @JacksonXmlElementWrapper(localName = "phones")
    private Set<Phone> phones = new HashSet<>();

    @NotNull
    @Column(name = "register_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date registerDate;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "account_generator")
    @SequenceGenerator(name = "account_generator", sequenceName = "accounts_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(unique = true)
    private String email;

    @NotNull
    private String password;

    @Column(name = "home_address")
    private String homeAddress;

    @Column(name = "work_address")
    private String workAddress;

    @Past
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date birthdate;

    @Embedded
    @AttributeOverride(name = "imageBytes", column = @Column(name = "profile_image"))
    @AttributeOverride(name = "name", column = @Column(name = "profile_image_name"))
    private Image profileImage;

    @Column(unique = true)
    private Integer icq;

    @Column(unique = true)
    private String skype;

    private String additional;

    @ManyToMany(cascade = MERGE, fetch = EAGER)
    @JoinTable(
            name = "account_role",
            joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private Set<Role> roles;

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Image getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Image profileImage) {
        this.profileImage = profileImage;
    }

    public Date getBirthdate() {
        return nonNull(birthdate) ? (Date) birthdate.clone() : null;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = nonNull(birthdate) ? (Date) birthdate.clone() : null;
    }

    public Date getRegisterDate() {
        return nonNull(registerDate) ? (Date) registerDate.clone() : null;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = (Date) registerDate.clone();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        phones.forEach(phone -> phone.setAccount(this));
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public Integer getIcq() {
        return icq;
    }

    public void setIcq(Integer icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public boolean addPhone(Phone phone) {
        phone.setAccount(this);
        return phones.add(phone);
    }

    public boolean addRole(Role role) {
        if (nonNull(roles)) {
            return roles.add(role);
        }
        roles = new HashSet<>();
        return roles.add(role);
    }

    public boolean removePhone(Phone phone) {
        return phones.remove(phone);
    }

    @Override
    public int hashCode() {
        return hash(phones, registerDate, id, firstName, middleName, lastName, email, homeAddress, workAddress,
                birthdate, icq, skype, additional, roles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return id == account.id && Objects.equals(phones, account.phones)
                && Objects.equals(registerDate, account.registerDate) && Objects.equals(firstName, account.firstName)
                && Objects.equals(middleName, account.middleName) && Objects.equals(lastName, account.lastName)
                && Objects.equals(email, account.email) && Objects.equals(homeAddress, account.homeAddress)
                && Objects.equals(workAddress, account.workAddress) && Objects.equals(birthdate, account.birthdate)
                && Objects.equals(icq, account.icq) && Objects.equals(skype, account.skype)
                && Objects.equals(additional, account.additional) && Objects.equals(roles, account.roles);
    }

    @Override
    public String toString() {
        return "Account{" +
                "phones=" + phones +
                ", registerDate=" + registerDate +
                ", id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", birthdate=" + birthdate +
                ", profileImage=" + profileImage +
                ", icq=" + icq +
                ", skype='" + skype + '\'' +
                ", additional='" + additional + '\'' +
                ", roles=" + roles +
                '}';
    }

}