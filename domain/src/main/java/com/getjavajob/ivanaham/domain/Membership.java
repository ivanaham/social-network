package com.getjavajob.ivanaham.domain;

import static java.util.Objects.hash;
import static javax.persistence.GenerationType.SEQUENCE;

import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "group_members", uniqueConstraints = {@UniqueConstraint(columnNames = {"account_id", "group_id"})})
public class Membership {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private Group group;

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "membership_generator")
    @SequenceGenerator(name = "membership_generator", sequenceName = "group_members_id_seq", allocationSize = 1)
    private long id;

    @NotNull
    @Column(name = "request_status_id")
    private RequestStatus status;

    @Column(name = "account_rights_id")
    private Rights rights;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Rights getRights() {
        return rights;
    }

    public void setRights(Rights accountRights) {
        this.rights = accountRights;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return hash(id, status, rights);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membership that = (Membership) o;
        return id == that.id && status == that.status && rights == that.rights;
    }

    @Override
    public String toString() {
        return "Membership{" +
                "account=" + account +
                ", group=" + group +
                ", id=" + id +
                ", status=" + status +
                ", rights=" + rights +
                '}';
    }

}
