package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.Phone.Type.HOME;
import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static java.sql.Date.valueOf;
import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class AccountTest {

    @Test
    void testGetterAndSetter_RegisterDate() {
        Account account = new Account();
        Date registerDate = valueOf("2022-01-01");
        account.setRegisterDate(registerDate);
        assertEquals(registerDate, account.getRegisterDate());
    }

    @Test
    void testGetterAndSetter_Password() {
        Account account = new Account();
        String password = "Banana0@";
        account.setPassword(password);
        assertEquals(password, account.getPassword());
    }

    @Test
    void testGetterAndSetter_Phones() {
        Account account = new Account();
        Phone phoneA = new Phone();
        phoneA.setType(HOME);
        phoneA.setNumber("38093789074");
        Phone phoneB = new Phone();
        phoneB.setType(WORK);
        phoneB.setNumber("38067543672");
        Set<Phone> phones = Stream.of(phoneA, phoneB).collect(toSet());
        account.setPhones(phones);
        assertEquals(phones, account.getPhones());
    }

    @Test
    void testAddPhone() {
        Account account = new Account();
        Phone phoneA = new Phone();
        phoneA.setType(HOME);
        phoneA.setNumber("38093789074");
        Phone phoneB = new Phone();
        phoneB.setType(WORK);
        phoneB.setNumber("38067543672");
        assertTrue(account.addPhone(phoneA));
        assertTrue(account.addPhone(phoneB));
        assertTrue(account.getPhones().containsAll(asList(phoneA, phoneB)));
    }

    @Test
    void testGetterAndSetter_Email() {
        Account account = new Account();
        String email = "test@gmail.com";
        account.setEmail(email);
        assertEquals(email, account.getEmail());
    }

    @Test
    void testGetterAndSetter_Birthdate() {
        Account account = new Account();
        Date birthdate = valueOf(of(1999, 1, 1));
        account.setBirthdate(birthdate);
        assertEquals(birthdate, account.getBirthdate());
    }

    @Test
    void testGetterAndSetter_Id() {
        Account account = new Account();
        long id = 5000;
        account.setId(id);
        assertEquals(id, account.getId());
    }

    @Test
    void testGetterAndSetter_FirstName() {
        Account account = new Account();
        String firstName = "NewName";
        account.setFirstName(firstName);
        assertEquals(firstName, account.getFirstName());
    }

    @Test
    void testGetterAndSetter_LastName() {
        Account account = new Account();
        String lastName = "NewName";
        account.setLastName(lastName);
        assertEquals(lastName, account.getLastName());
    }

    @Test
    void testGetterAndSetter_MiddleName() {
        Account account = new Account();
        String middleName = "NewName";
        account.setMiddleName(middleName);
        assertEquals(middleName, account.getMiddleName());
    }

    @Test
    void testGetterAndSetter_HomeAddress() {
        Account account = new Account();
        String homeAddress = "home";
        account.setHomeAddress(homeAddress);
        assertEquals(homeAddress, account.getHomeAddress());
    }

    @Test
    void testGetterAndSetter_WorkAddress() {
        Account account = new Account();
        String workAddress = "work";
        account.setWorkAddress(workAddress);
        assertEquals(workAddress, account.getWorkAddress());
    }

    @Test
    void testGetterAndSetter_Icq() {
        Account account = new Account();
        int icq = 1010100;
        account.setIcq(icq);
        assertEquals(Integer.valueOf(icq), account.getIcq());
    }

    @Test
    void testGetterAndSetter_Skype() {
        Account account = new Account();
        String skype = "live:101010";
        account.setSkype(skype);
        assertEquals(skype, account.getSkype());
    }

    @Test
    void testGetterAndSetter_Additional() {
        Account account = new Account();
        String additional = "info";
        account.setAdditional(additional);
        assertEquals(additional, account.getAdditional());
    }

    @Test
    void testRemovePhone_True() {
        Account account = new Account();
        Phone phone = new Phone();
        phone.setType(HOME);
        phone.setNumber("38093789074");
        account.addPhone(phone);
        assertTrue(account.removePhone(phone));
        assertTrue(account.getPhones().isEmpty());
    }

    @Test
    void testRemovePhone_False() {
        Account account = new Account();
        assertFalse(account.removePhone(new Phone()));
    }

    @Test
    void testGetterAndSetter_ProfileImage() {
        Account account = new Account();
        Image profileImage = new Image();
        profileImage.setImageBytes(new byte[]{1, 4, 0, 34, 2, 67, 90});
        profileImage.setName("bruno.jpeg");
        account.setProfileImage(profileImage);
        assertEquals(profileImage, account.getProfileImage());
    }

    @Test
    void testGetterAndSetter_Roles() {
        Account account = new Account();
        Set<Role> roles = new HashSet<>();
        roles.add(new Role("USER"));
        roles.add(new Role("ADMIN"));
        account.setRoles(roles);
        assertEquals(roles, account.getRoles());
    }

    @Test
    void testAddRoleTrue() {
        Account account = new Account();
        assertTrue(account.addRole(new Role("USER")));
        Set<Role> roles = new HashSet<>();
        roles.add(new Role("USER"));
        assertEquals(roles, account.getRoles());
    }

    @Test
    void testAddRoleFalse() {
        Account account = new Account();
        account.addRole(new Role("USER"));
        assertFalse(account.addRole(new Role("USER")));
        assertEquals(1, account.getRoles().size());
    }

    @Test
    void testHashCode() {
        Account accountA = new Account();
        Account accountB = new Account();
        assertEquals(accountA, accountB);
        assertEquals(accountA.hashCode(), accountB.hashCode());
    }

    @Test
    void equalsIsReflexive() {
        Account account = new Account();
        assertEquals(account, account);
    }

    @Test
    void equalsIsSymmetric() {
        Account accountA = new Account();
        Account accountB = new Account();
        assertEquals(accountA, accountB);
        assertEquals(accountB, accountA);
    }

    @Test
    void equalsIsTransitive() {
        Account accountA = new Account();
        Account accountB = new Account();
        Account accountC = new Account();
        assertEquals(accountA, accountB);
        assertEquals(accountB, accountC);
        assertEquals(accountA, accountC);
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void equalsNullMustReturnFalse() {
        assertFalse(new Account().equals(null));
    }

    @Test
    void testToStringReliability() {
        Account account = new Account();
        account.setFirstName("Giorno");
        account.setLastName("Lap lass");
        String stringifiedAcct = account.toString();
        assertFalse(stringifiedAcct.isEmpty());
        assertTrue(stringifiedAcct.contains("Account"));
    }

}
