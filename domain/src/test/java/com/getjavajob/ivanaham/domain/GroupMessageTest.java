package com.getjavajob.ivanaham.domain;

import static java.time.LocalDateTime.now;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class GroupMessageTest {

    @Test
    void testAllFieldsGettersAndSetters() {
        LocalDateTime now = now();
        Account account = new Account();
        GroupMessage msg = getDummyMessage();
        msg.setDateTimeSent(now);
        assertEquals(3, msg.getId());
        assertEquals(account, msg.getAuthor());
        assertEquals(new Group(), msg.getRecipient());
        assertEquals("Hi man!", msg.getText());
        assertEquals(now, msg.getDateTimeSent());
        Image image = new Image();
        image.setName("test.jpg");
        image.setImageBytes(new byte[]{1, 2, 0});
        assertEquals(image, msg.getAttachedImage());
    }

    @Test
    void hashcodeShouldBeSameOnEqualMessage() {
        GroupMessage messageA = new GroupMessage();
        GroupMessage messageB = new GroupMessage();
        assertEquals(messageA.hashCode(), messageB.hashCode());
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void shouldBeTrueWhenEqualsOnSameMessage() {
        GroupMessage message = getDummyMessage();
        assertTrue(message.equals(message));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void shouldBeFalseWhenEqualsOnNull() {
        assertFalse(new GroupMessage().equals(null));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeTrueWhenEqualsOnEqualMessage() {
        assertTrue(new GroupMessage().equals(new GroupMessage()));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeFalseWhenEqualsOnDifferentMessage() {
        GroupMessage messageA = getDummyMessage();
        messageA.setText("Hello");
        GroupMessage messageB = getDummyMessage();
        assertFalse(messageA.equals(messageB));
    }

    @Test
    void verifyToStringReliability() {
        GroupMessage msg = getDummyMessage();
        assertTrue(msg.toString().contains("Message"));
    }

    private GroupMessage getDummyMessage() {
        GroupMessage message = new GroupMessage();
        message.setId(3);
        message.setDateTimeSent(now());
        Image image = new Image();
        image.setName("test.jpg");
        image.setImageBytes(new byte[]{1, 2, 0});
        message.setAttachedImage(image);
        message.setAuthor(new Account());
        message.setRecipient(new Group());
        message.setText("Hello! It is my 1st post");
        message.setText("Hi man!");
        return message;
    }

}