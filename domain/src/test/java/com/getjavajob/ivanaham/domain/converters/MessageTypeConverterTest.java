package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.Message.Type.ACCOUNT;
import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static com.getjavajob.ivanaham.domain.Message.Type.GROUP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class MessageTypeConverterTest {

    @Test
    void convertToDatabaseColumn_Account() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(1, converter.convertToDatabaseColumn(ACCOUNT));
    }

    @Test
    void convertToDatabaseColumn_Group() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(2, converter.convertToDatabaseColumn(GROUP));
    }

    @Test
    void convertToDatabaseColumn_Direct() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(3, converter.convertToDatabaseColumn(DIRECT));
    }

    @Test
    void convertToDatabaseColumn_Null() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertNull(converter.convertToDatabaseColumn(null));
    }

    @Test
    void convertToEntityAttribute_Null() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertNull(converter.convertToEntityAttribute(null));
    }

    @Test
    void convertToEntityAttribute_1() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(ACCOUNT, converter.convertToEntityAttribute(1));
    }

    @Test
    void convertToEntityAttribute_2() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(GROUP, converter.convertToEntityAttribute(2));
    }

    @Test
    void convertToEntityAttribute_3() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertEquals(DIRECT, converter.convertToEntityAttribute(3));
    }

    @Test
    void convertToEntityAttribute_Else() {
        MessageTypeConverter converter = new MessageTypeConverter();
        assertThrows(IllegalArgumentException.class, () -> converter.convertToEntityAttribute(4));
    }

}