package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class RequestStatusConverterTest {

    @Test
    void convertToDatabaseColumn_Pending() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(1, converter.convertToDatabaseColumn(PENDING));
    }

    @Test
    void convertToDatabaseColumn_Accepted() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(2, converter.convertToDatabaseColumn(ACCEPTED));
    }

    @Test
    void convertToDatabaseColumn_Declined() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(3, converter.convertToDatabaseColumn(DECLINED));
    }

    @Test
    void convertToDatabaseColumn_Null() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertNull(converter.convertToDatabaseColumn(null));
    }

    @Test
    void convertToEntityAttribute_Null() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertNull(converter.convertToEntityAttribute(null));
    }

    @Test
    void convertToEntityAttribute_1() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(PENDING, converter.convertToEntityAttribute(1));
    }

    @Test
    void convertToEntityAttribute_2() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(ACCEPTED, converter.convertToEntityAttribute(2));
    }

    @Test
    void convertToEntityAttribute_3() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertEquals(DECLINED, converter.convertToEntityAttribute(3));
    }

    @Test
    void convertToEntityAttribute_Else() {
        RequestStatusConverter converter = new RequestStatusConverter();
        assertThrows(IllegalArgumentException.class, () -> converter.convertToEntityAttribute(4));
    }

}