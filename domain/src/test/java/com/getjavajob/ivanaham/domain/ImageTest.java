package com.getjavajob.ivanaham.domain;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ImageTest {

    @Test
    void testGetterAndSetter_ImageBytes() {
        Image image = new Image();
        byte[] imageBytes = {1, 2, 3, 4, -125};
        image.setImageBytes(imageBytes);
        assertArrayEquals(imageBytes, image.getImageBytes());
    }

    @Test
    void testGetterAndSetter_Name() {
        Image image = new Image();
        String name = "cow.png";
        image.setName(name);
        assertEquals(name, image.getName());
    }

    @Test
    void testToStringReliability() {
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        image.setName("cow.png");
        assertTrue(image.toString().contains("Image"));
    }

    @Test
    void testToStringReliability_EmptyBean() {
        Image image = new Image();
        assertTrue(image.toString().contains("Image"));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsTrue() {
        Image imageA = new Image();
        imageA.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        Image imageB = new Image();
        imageB.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        assertTrue(imageA.equals(imageB));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsFalse() {
        Image imageA = new Image();
        imageA.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        Image imageB = new Image();
        assertFalse(imageA.equals(imageB));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void testEqualsFalse_Null() {
        Image imageA = new Image();
        imageA.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        assertFalse(imageA.equals(null));
    }

    @Test
    void testHashCodeEquals() {
        Image imageA = new Image();
        imageA.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        Image imageB = new Image();
        imageB.setImageBytes(new byte[]{1, 2, 3, 4, -125});
        assertEquals(imageA, imageB);
        assertEquals(imageA.hashCode(), imageB.hashCode());
    }

}