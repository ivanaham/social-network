package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class MembershipTest {

    @Test
    void shouldBeAbleToGetAndSetGroupFields() {
        Account dummyAccount = new Account();
        Group dummyGroup = new Group();
        Membership membership = getDummyMembership();
        membership.setId(3);
        membership.setStatus(ACCEPTED);
        membership.setRights(COMMON);
        membership.setAccount(dummyAccount);
        membership.setGroup(dummyGroup);
        assertEquals(3, membership.getId());
        assertEquals(ACCEPTED, membership.getStatus());
        assertEquals(COMMON, membership.getRights());
        assertEquals(dummyAccount, membership.getAccount());
        assertEquals(dummyGroup, membership.getGroup());
    }

    @Test
    void hashcodeMustBeSameOnEqualObjects() {
        Membership membershipA = getDummyMembership();
        Membership membershipB = getDummyMembership();
        assertEquals(membershipA.hashCode(), membershipB.hashCode());
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void equalsMustBeTrueOnEqualObjects() {
        Membership membershipA = getDummyMembership();
        Membership membershipB = getDummyMembership();
        assertTrue(membershipA.equals(membershipB));
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void equalsMustBeTrueOnSameObjects() {
        Membership membership = getDummyMembership();
        assertTrue(membership.equals(membership));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void equalsMustBeFalseOnDifferentObjects() {
        Membership membership = getDummyMembership();
        membership.setRights(MODERATOR);
        assertFalse(getDummyMembership().equals(membership));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void equalsMustBeFalseOnNull() {
        Membership membership = getDummyMembership();
        assertFalse(membership.equals(null));
    }

    @Test
    void verifyToStringReliability() {
        Membership membership = getDummyMembership();
        assertTrue(membership.toString().contains("Membership"));
    }

    private Membership getDummyMembership() {
        Membership membership = new Membership();
        membership.setAccount(new Account());
        membership.setGroup(new Group());
        membership.setStatus(PENDING);
        membership.setRights(COMMON);
        return membership;
    }

}