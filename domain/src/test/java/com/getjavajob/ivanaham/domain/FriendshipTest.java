package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class FriendshipTest {

    @Test
    void testAllFieldsGettersAndSetters() {
        Account dummyAccount = new Account();
        dummyAccount.setId(100);
        Friendship friendship = getDummyFriendship();
        friendship.setId(3);
        friendship.setStatus(ACCEPTED);
        friendship.setAccountA(dummyAccount);
        friendship.setAccountB(dummyAccount);
        friendship.setLastActedAccount(dummyAccount);
        assertEquals(3, friendship.getId());
        assertEquals(ACCEPTED, friendship.getStatus());
        assertEquals(dummyAccount, friendship.getAccountA());
        assertEquals(dummyAccount, friendship.getAccountB());
        assertEquals(dummyAccount, friendship.getLastActedAccount());
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldGetTrueWhenFriendshipsAreEqual() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = getDummyFriendship();
        assertTrue(friendshipA.equals(friendshipB));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldGetFalseWhenFriendshipsAreNotEqual() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = getDummyFriendship();
        friendshipB.setStatus(ACCEPTED);
        assertFalse(friendshipA.equals(friendshipB));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void shouldGetFalseWhenEqualsOnNull() {
        Friendship friendship = getDummyFriendship();
        assertFalse(friendship.equals(null));
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void shouldGetTrueWhenEqualsOnSameObject() {
        Friendship friendship = getDummyFriendship();
        assertTrue(friendship.equals(friendship));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void hashcodeOfEqualObjectsMustBeSame() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = getDummyFriendship();
        assertTrue(friendshipA.equals(friendshipB));
        assertEquals(friendshipA.hashCode(), friendshipB.hashCode());
    }

    @Test
    void verifyToStringReliability() {
        Friendship friendship = getDummyFriendship();
        assertTrue(friendship.toString().contains("Friendship"));
    }

    private Friendship getDummyFriendship() {
        Friendship friendship = new Friendship();
        Account dummyAccount = new Account();
        friendship.setAccountA(dummyAccount);
        friendship.setAccountB(dummyAccount);
        friendship.setLastActedAccount(dummyAccount);
        friendship.setStatus(PENDING);
        return friendship;
    }

}