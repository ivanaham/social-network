package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static java.time.LocalDateTime.now;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class MessageTest {

    @Test
    void hashcodeShouldBeSameOnEqualMessage() {
        Message messageA = new AccountMessage();
        Message messageB = new AccountMessage();
        assertEquals(messageA.hashCode(), messageB.hashCode());
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void shouldBeTrueWhenEqualsOnSameMessage() {
        Message message = getDummyMessage();
        assertTrue(message.equals(message));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void shouldBeFalseWhenEqualsOnNull() {
        Message message = getDummyMessage();
        assertFalse(message.equals(null));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeTrueWhenEqualsOnEqualMessage() {
        Message messageA = getDummyMessage();
        Message messageB = getDummyMessage();
        assertTrue(messageA.equals(messageB));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeFalseWhenEqualsOnDifferentMessage() {
        Message messageA = getDummyMessage();
        messageA.setText("Hello");
        Message messageB = getDummyMessage();
        assertFalse(messageA.equals(messageB));
    }

    @Test
    void verifyToStringReliability() {
        Message msg = getDummyMessage();
        assertTrue(msg.toString().contains("Message"));
    }

    private Message getDummyMessage() {
        Account dummyAccount = new Account();
        AccountMessage message = new AccountMessage();
        message.setId(3);
        message.setDateTimeSent(now());
        Image image = new Image();
        image.setName("test.jpg");
        image.setImageBytes(new byte[]{1, 2, 0});
        message.setAttachedImage(image);
        message.setAuthor(dummyAccount);
        message.setRecipient(dummyAccount);
        message.setText("Hello! It is my 1st post");
        message.setMessageType(DIRECT);
        message.setText("Hi man!");
        return message;
    }

}