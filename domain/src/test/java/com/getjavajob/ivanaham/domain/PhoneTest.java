package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class PhoneTest {

    private static final Phone dummyPhone = new Phone();

    static {
        dummyPhone.setId(1);
        dummyPhone.setAccount(new Account());
        dummyPhone.setType(WORK);
        dummyPhone.setNumber("38093546343");
    }

    @Test
    void testGettersAndSetters() {
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(new Account());
        phone.setType(WORK);
        phone.setNumber("38093546343");
        assertEquals(1, phone.getId());
        assertEquals(new Account(), phone.getAccount());
        assertEquals(WORK, phone.getType());
        assertEquals("38093546343", phone.getNumber());
    }

    @Test
    void whenPhonesAreEqualHashcodeIsEqual() {
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(new Account());
        phone.setType(WORK);
        phone.setNumber("38093546343");
        assertEquals(dummyPhone.hashCode(), phone.hashCode());
    }

    @Test
    void equalsIsReflexive() {
        assertEquals(dummyPhone, dummyPhone);
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void equalsIsSymmetric() {
        Phone phone = new Phone();
        phone.setId(1);
        phone.setAccount(new Account());
        phone.setType(WORK);
        phone.setNumber("38093546343");
        assertTrue(dummyPhone.equals(phone));
        assertTrue(phone.equals(dummyPhone));
    }

    @Test
    void equalsIsTransitive() {
        Phone phoneA = new Phone();
        phoneA.setId(1);
        phoneA.setAccount(new Account());
        phoneA.setType(WORK);
        phoneA.setNumber("38093546343");
        Phone phoneB = new Phone();
        phoneB.setId(1);
        phoneB.setAccount(new Account());
        phoneB.setType(WORK);
        phoneB.setNumber("38093546343");
        assertEquals(dummyPhone, phoneB);
        assertEquals(phoneB, phoneA);
        assertEquals(dummyPhone, phoneA);
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void shouldGetFalseWhenEqualsOnNull() {
        assertFalse(dummyPhone.equals(null));
    }

    @Test
    void testToStringReliability() {
        assertTrue(dummyPhone.toString().contains("Phone"));
    }

}