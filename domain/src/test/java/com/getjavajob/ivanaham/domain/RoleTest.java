package com.getjavajob.ivanaham.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class RoleTest {

    @Test
    void testGetterAndSetter_Id() {
        Role role = new Role();
        role.setId(1);
        assertEquals(1, role.getId());
    }

    @Test
    void testGetterAndSetter_Name() {
        Role role = new Role();
        role.setName("USER");
        assertEquals("USER", role.getName());
    }

    @Test
    void testGetterAndSetter_Accounts() {
        Role role = new Role();
        Set<Account> accounts = new HashSet<>();
        accounts.add(new Account());
        role.setAccounts(accounts);
        assertEquals(accounts, role.getAccounts());
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void testEqualsTrue_SameObject() {
        Role role = new Role();
        role.setName("USER");
        assertTrue(role.equals(role));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsTrue() {
        Role roleA = new Role();
        Role roleB = new Role();
        roleA.setName("USER");
        roleB.setName("USER");
        assertTrue(roleA.equals(roleB));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsFalse() {
        Role roleA = new Role();
        roleA.setName("USER");
        assertFalse(roleA.equals(new Role()));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void testEqualsFalse_Null() {
        Role roleA = new Role();
        roleA.setName("USER");
        assertFalse(roleA.equals(null));
    }

    @Test
    void testToStringReliability() {
        Role role = new Role();
        role.setName("USER");
        assertTrue(role.toString().contains("Role"));
    }

}