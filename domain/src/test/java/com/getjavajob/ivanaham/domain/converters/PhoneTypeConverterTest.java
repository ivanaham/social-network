package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.Phone.Type.HOME;
import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class PhoneTypeConverterTest {

    @Test
    void convertToDatabaseColumn_Home() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertEquals(1, converter.convertToDatabaseColumn(HOME));
    }

    @Test
    void convertToDatabaseColumn_Work() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertEquals(2, converter.convertToDatabaseColumn(WORK));
    }

    @Test
    void convertToDatabaseColumn_Null() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertNull(converter.convertToDatabaseColumn(null));
    }

    @Test
    void convertToEntityAttribute_Null() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertNull(converter.convertToEntityAttribute(null));
    }

    @Test
    void convertToEntityAttribute_1() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertEquals(HOME, converter.convertToEntityAttribute(1));
    }

    @Test
    void convertToEntityAttribute_2() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertEquals(WORK, converter.convertToEntityAttribute(2));
    }

    @Test
    void convertToEntityAttribute_Else() {
        PhoneTypeConverter converter = new PhoneTypeConverter();
        assertThrows(IllegalArgumentException.class, () -> converter.convertToEntityAttribute(3));
    }

}