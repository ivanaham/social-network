package com.getjavajob.ivanaham.domain;

import static java.sql.Date.valueOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import org.junit.jupiter.api.Test;

class GroupTest {

    @Test
    void testAllFieldsGettersAndSetters() {
        final Date creationDate = valueOf("2020-01-01");
        Group group = new Group();
        group.setName("New name");
        group.setCreationDate(creationDate);
        group.setCreator(new Account());
        group.setId(12);
        group.setDescription("New desc");
        Image profileImage = new Image();
        profileImage.setName("test.png");
        profileImage.setImageBytes(new byte[]{1, 2, -1});
        group.setProfileImage(profileImage);
        assertEquals("New name", group.getName());
        assertEquals(creationDate, group.getCreationDate());
        assertEquals(new Account(), group.getCreator());
        assertEquals(12, group.getId());
        assertEquals("New desc", group.getDescription());
        assertEquals(profileImage, group.getProfileImage());
    }

    @Test
    void whenGroupsAreEqualHashcodeIsEqual() {
        Group groupA = new Group();
        groupA.setName("Tornado Lovers");
        Group groupB = new Group();
        groupB.setName("Tornado Lovers");
        assertEquals(groupA, groupB);
        assertEquals(groupA.hashCode(), groupB.hashCode());
    }

    @Test
    void equalsIsReflexive() {
        Group group = new Group();
        group.setName("Tornado Lovers");
        assertEquals(group, group);
    }

    @Test
    void equalsIsSymmetric() {
        Group groupA = new Group();
        groupA.setName("Tornado Lovers");
        Group groupB = new Group();
        groupB.setName("Tornado Lovers");
        assertEquals(groupA, groupB);
        assertEquals(groupB, groupA);
    }

    @Test
    void equalsIsTransitive() {
        Group groupA = new Group();
        groupA.setName("Tornado Lovers");
        Group groupB = new Group();
        groupB.setName("Tornado Lovers");
        Group groupC = new Group();
        groupC.setName("Tornado Lovers");
        assertEquals(groupA, groupB);
        assertEquals(groupB, groupC);
        assertEquals(groupA, groupC);
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void equalsNullMustReturnFalse() {
        assertFalse(new Group().equals(null));
    }

    @Test
    void testToStringReliability() {
        Group group = new Group();
        group.setName("James's Party");
        assertTrue(group.toString().contains("Group"));
    }

}