package com.getjavajob.ivanaham.domain.converters;

import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class RightsConverterTest {

    @Test
    void convertToDatabaseColumn_Common() {
        RightsConverter converter = new RightsConverter();
        assertEquals(2, converter.convertToDatabaseColumn(COMMON));
    }

    @Test
    void convertToDatabaseColumn_Moderator() {
        RightsConverter converter = new RightsConverter();
        assertEquals(1, converter.convertToDatabaseColumn(MODERATOR));
    }

    @Test
    void convertToDatabaseColumn_Null() {
        RightsConverter converter = new RightsConverter();
        assertNull(converter.convertToDatabaseColumn(null));
    }

    @Test
    void convertToEntityAttribute_Null() {
        RightsConverter converter = new RightsConverter();
        assertNull(converter.convertToEntityAttribute(null));
    }

    @Test
    void convertToEntityAttribute_1() {
        RightsConverter converter = new RightsConverter();
        assertEquals(MODERATOR, converter.convertToEntityAttribute(1));
    }

    @Test
    void convertToEntityAttribute_2() {
        RightsConverter converter = new RightsConverter();
        assertEquals(COMMON, converter.convertToEntityAttribute(2));
    }

    @Test
    void convertToEntityAttribute_Else() {
        RightsConverter converter = new RightsConverter();
        assertThrows(IllegalArgumentException.class, () -> converter.convertToEntityAttribute(3));
    }

}