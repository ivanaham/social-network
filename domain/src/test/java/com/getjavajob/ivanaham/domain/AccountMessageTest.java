package com.getjavajob.ivanaham.domain;

import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static java.time.LocalDateTime.now;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class AccountMessageTest {

    @Test
    void testAllFieldsGettersAndSetters() {
        LocalDateTime now = now();
        Account account = new Account();
        AccountMessage msg = getDummyMessage();
        msg.setDateTimeSent(now);
        assertEquals(3, msg.getId());
        assertEquals(account, msg.getAuthor());
        assertEquals(account, msg.getRecipient());
        assertEquals("Hi man!", msg.getText());
        assertEquals(now, msg.getDateTimeSent());
        assertEquals(DIRECT, msg.getMessageType());
        Image image = new Image();
        image.setName("test.jpg");
        image.setImageBytes(new byte[]{1, 2, 0});
        assertEquals(image, msg.getAttachedImage());
    }

    @Test
    void hashcodeShouldBeSameOnEqualMessage() {
        AccountMessage messageA = new AccountMessage();
        AccountMessage messageB = new AccountMessage();
        assertEquals(messageA.hashCode(), messageB.hashCode());
    }

    @SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself"})
    @Test
    void shouldBeTrueWhenEqualsOnSameMessage() {
        AccountMessage message = getDummyMessage();
        assertTrue(message.equals(message));
    }

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void shouldBeFalseWhenEqualsOnNull() {
        assertFalse(new AccountMessage().equals(null));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeTrueWhenEqualsOnEqualMessage() {
        assertTrue(new AccountMessage().equals(new AccountMessage()));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void shouldBeFalseWhenEqualsOnDifferentMessage() {
        AccountMessage messageA = getDummyMessage();
        messageA.setText("Hello");
        AccountMessage messageB = getDummyMessage();
        assertFalse(messageA.equals(messageB));
    }

    @Test
    void verifyToStringReliability() {
        AccountMessage msg = getDummyMessage();
        assertTrue(msg.toString().contains("Message"));
    }

    private AccountMessage getDummyMessage() {
        Account dummyAccount = new Account();
        AccountMessage message = new AccountMessage();
        message.setId(3);
        message.setDateTimeSent(now());
        Image image = new Image();
        image.setName("test.jpg");
        image.setImageBytes(new byte[]{1, 2, 0});
        message.setAttachedImage(image);
        message.setAuthor(dummyAccount);
        message.setRecipient(dummyAccount);
        message.setText("Hello! It is my 1st post");
        message.setMessageType(DIRECT);
        message.setText("Hi man!");
        return message;
    }

}