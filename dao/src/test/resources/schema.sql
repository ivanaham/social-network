CREATE TABLE IF NOT EXISTS roles
(
    id
    SERIAL
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    roles_name_unique
    UNIQUE
);

CREATE TABLE IF NOT EXISTS rights
(
    id
    SERIAL
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    rights_name_unique
    UNIQUE
);

CREATE TABLE IF NOT EXISTS message_types
(
    id
    SERIAL
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    message_types_name_unique
    UNIQUE
);

CREATE TABLE IF NOT EXISTS accounts
(
    id
    INTEGER
    PRIMARY
    KEY,
    first_name
    CHARACTER
    VARYING
    NOT
    NULL,
    middle_name
    CHARACTER
    VARYING,
    last_name
    CHARACTER
    VARYING
    NOT
    NULL,
    home_address
    CHARACTER
    VARYING,
    work_address
    CHARACTER
    VARYING,
    email
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    email
    UNIQUE,
    icq
    CHARACTER
    VARYING
    CONSTRAINT
    icq
    UNIQUE,
    skype
    CHARACTER
    VARYING
    CONSTRAINT
    skype
    UNIQUE,
    additional
    CHARACTER
    VARYING,
    password
    CHARACTER
    VARYING,
    birthdate
    DATE,
    register_date
    DATE
    NOT
    NULL,
    profile_image
    bytea,
    profile_image_name
    CHARACTER
    VARYING
);

CREATE TABLE IF NOT EXISTS account_role
(
    account_id
    INTEGER
    NOT
    NULL,
    role_id
    INTEGER
    NOT
    NULL,
    FOREIGN
    KEY
(
    account_id
) REFERENCES accounts
(
    id
),
    FOREIGN KEY
(
    role_id
) REFERENCES roles
(
    id
),
    PRIMARY KEY
(
    account_id,
    role_id
)
    );

CREATE TABLE IF NOT EXISTS group_messages
(
    id
    INTEGER
    PRIMARY
    KEY,
    author_id
    INTEGER
    NOT
    NULL,
    recipient_id
    INTEGER
    NOT
    NULL,
    text
    CHARACTER
    VARYING
    NOT
    NULL,
    date_time_sent
    TIMESTAMP
    NOT
    NULL,
    attached_image
    bytea,
    attached_image_name
    CHARACTER
    VARYING,
    FOREIGN
    KEY
(
    author_id
) REFERENCES accounts
(
    id
),
    FOREIGN KEY
(
    recipient_id
) REFERENCES groups
(
    id
)
    );

CREATE TABLE IF NOT EXISTS account_messages
(
    id
    INTEGER
    PRIMARY
    KEY,
    author_id
    INTEGER
    NOT
    NULL,
    recipient_id
    INTEGER
    NOT
    NULL,
    text
    CHARACTER
    VARYING
    NOT
    NULL,
    date_time_sent
    TIMESTAMP
    NOT
    NULL,
    message_type_id
    INTEGER
    NOT
    NULL,
    attached_image
    bytea,
    attached_image_name
    CHARACTER
    VARYING,
    FOREIGN
    KEY
(
    author_id
) REFERENCES accounts
(
    id
),
    FOREIGN KEY
(
    recipient_id
) REFERENCES groups
(
    id
),
    FOREIGN KEY
(
    message_type_id
) REFERENCES message_types
(
    id
)
    );

CREATE TABLE IF NOT EXISTS phone_types
(
    id
    SERIAL
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    phone_types_name_unique
    UNIQUE
);

CREATE TABLE IF NOT EXISTS phones
(
    id
    INTEGER
    PRIMARY
    KEY,
    account_id
    INTEGER
    NOT
    NULL,
    number
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    number_unique
    UNIQUE,
    phone_type_id
    INTEGER
    NOT
    NULL,
    FOREIGN
    KEY
(
    account_id
) REFERENCES accounts
(
    id
),
    FOREIGN KEY
(
    phone_type_id
) REFERENCES phone_types
(
    id
)
    );

CREATE TABLE IF NOT EXISTS request_statuses
(
    id
    SERIAL
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    request_statuses_name_unique
    UNIQUE
);

CREATE TABLE IF NOT EXISTS friends
(
    id
    INTEGER
    PRIMARY
    KEY,
    account_id_a
    INTEGER
    NOT
    NULL,
    account_id_b
    INTEGER
    NOT
    NULL,
    request_status_id
    INTEGER
    NOT
    NULL,
    last_acted_account_id
    INTEGER
    NOT
    NULL,
    FOREIGN
    KEY
(
    account_id_a
) REFERENCES accounts
(
    id
) ON DELETE CASCADE,
    FOREIGN KEY
(
    account_id_b
) REFERENCES accounts
(
    id
)
  ON DELETE CASCADE,
    FOREIGN KEY
(
    last_acted_account_id
) REFERENCES accounts
(
    id
)
  ON DELETE CASCADE,
    FOREIGN KEY
(
    request_status_id
) REFERENCES request_statuses
(
    id
),
    UNIQUE
(
    account_id_a,
    account_id_b
)
    );

CREATE TABLE IF NOT EXISTS groups
(
    id
    INTEGER
    PRIMARY
    KEY,
    name
    CHARACTER
    VARYING
    NOT
    NULL
    CONSTRAINT
    groups_name_unique
    UNIQUE,
    description
    CHARACTER
    VARYING,
    profile_image
    bytea,
    profile_image_name
    CHARACTER
    VARYING,
    creation_date
    DATE
    NOT
    NULL,
    creator_id
    INTEGER
    NOT
    NULL
    DEFAULT
    0,
    FOREIGN
    KEY
(
    creator_id
) REFERENCES accounts
(
    id
)
    );

CREATE TABLE IF NOT EXISTS group_members
(
    id
    INTEGER
    PRIMARY
    KEY,
    group_id
    INTEGER
    NOT
    NULL,
    account_id
    INTEGER
    NOT
    NULL,
    request_status_id
    INTEGER
    NOT
    NULL,
    account_rights_id
    INTEGER,
    FOREIGN
    KEY
(
    group_id
) REFERENCES groups
(
    id
),
    FOREIGN KEY
(
    account_id
) REFERENCES accounts
(
    id
),
    FOREIGN KEY
(
    request_status_id
) REFERENCES request_statuses
(
    id
),
    FOREIGN KEY
(
    account_rights_id
) REFERENCES rights
(
    id
),
    UNIQUE
(
    group_id,
    account_id
)
    );