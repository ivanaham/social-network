INSERT INTO request_statuses(name)
VALUES ('PENDING'),
       ('ACCEPTED'),
       ('DECLINED');

INSERT INTO phone_types(name)
VALUES ('HOME'),
       ('WORK');

INSERT INTO message_types(name)
VALUES ('ACCOUNT'),
       ('GROUP'),
       ('DIRECT');

INSERT INTO rights(name)
VALUES ('COMMON'),
       ('MODERATOR');

INSERT INTO roles(id, name)
VALUES (1, 'ROLE_USER'),
       (2, 'ROLE_ADMIN');

CREATE SEQUENCE "roles_id_seq"
    INCREMENT BY 1
    START WITH 3;