INSERT INTO accounts(id, first_name, last_name, email, register_date, password)
SELECT s.a AS id,
       arrays.firstnames[s.a % ARRAY_LENGTH(arrays.firstnames, 1) + 1] AS first_name, arrays.lastnames[s.a % ARRAY_LENGTH(arrays.lastnames, 1) + 1] AS last_name, arrays.firstnames[s.a % ARRAY_LENGTH(arrays.firstnames, 1) + 1] || s.a || '@gmail.com' AS email, TO_DATE(CASE
    WHEN s.a % 3 = 0 THEN '2022/10/29'
    WHEN s.a % 2 = 0 THEN '2022/10/26'
    ELSE '2022/10/10'
    END, 'YYYY/MM/DD') register_date, gen_random_uuid() password
FROM generate_series(1, 50000000) AS s(a)
    CROSS JOIN (
    SELECT ARRAY[
    'Adam', 'Bill', 'Bob', 'Calvin', 'Donald', 'Dwight', 'Frank', 'Fred', 'George', 'Howard', 'James', 'John', 'Jacob', 'Jack', 'Martin', 'Matthew', 'Max', 'Michael', 'Paul', 'Peter', 'Phil', 'Roland', 'Ronald', 'Samuel', 'Steve', 'Theo', 'Warren', 'William', 'Abigail', 'Alice', 'Allison', 'Amanda', 'Anne', 'Barbara', 'Betty', 'Carol', 'Cleo', 'Donna', 'Jane', 'Jennifer', 'Julie', 'Martha', 'Mary', 'Melissa', 'Patty', 'Sarah', 'Simone', 'Susan'
    ] AS firstnames, ARRAY[
    'Matthews', 'Smith', 'Jones', 'Davis', 'Jacobson', 'Williams', 'Donaldson', 'Maxwell', 'Peterson', 'Stevens', 'Franklin', 'Washington', 'Jefferson', 'Adams', 'Jackson', 'Johnson', 'Lincoln', 'Grant', 'Fillmore', 'Harding', 'Taft', 'Truman', 'Nixon', 'Ford', 'Carter', 'Reagan', 'Bush', 'Clinton', 'Hancock'
    ] AS lastnames
    ) AS arrays
