package com.getjavajob.ivanaham.dao.impl;

import static java.sql.Date.valueOf;
import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupMessageDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.GroupMessage;
import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.domain.Role;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class GroupMessageDaoImplTest {

    @Autowired
    private GroupMessageDao groupMessageDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private GroupDao groupDao;

    @Test
    void testInsertAndFindById() {
        GroupMessage message = new GroupMessage();
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        message.setAttachedImage(image);
        Account account = insertAndGetDummyAccount();
        message.setAuthor(account);
        message.setText("Hello");
        message.setRecipient(insertAndGetDummyGroup_1(account));
        groupMessageDao.insert(message);
        assertEquals(message, groupMessageDao.findById(message.getId()));
    }

    @Test
    void testUpdate() {
        GroupMessage message = new GroupMessage();
        Account account = insertAndGetDummyAccount();
        message.setAuthor(account);
        message.setRecipient(insertAndGetDummyGroup_1(account));
        message.setText("Hello");
        groupMessageDao.insert(message);
        message = groupMessageDao.findById(message.getId());
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        message.setAttachedImage(image);
        message.setText("Hello man)");
        groupMessageDao.update(message);
        assertEquals(message, groupMessageDao.findById(message.getId()));
    }

    @Test
    void testFindById_Absent() {
        assertNull(groupMessageDao.findById(100));
    }

    @Test
    void testFindAll() {
        GroupMessage messageA = new GroupMessage();
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        messageA.setAttachedImage(image);
        Account account = insertAndGetDummyAccount();
        Group group = insertAndGetDummyGroup_1(account);
        messageA.setAuthor(account);
        messageA.setText("Hello");
        messageA.setRecipient(group);
        GroupMessage messageB = new GroupMessage();
        messageB.setAuthor(account);
        messageB.setText("Hello again");
        messageB.setRecipient(group);
        groupMessageDao.insert(messageA);
        groupMessageDao.insert(messageB);
        assertEquals(asList(messageA, messageB), groupMessageDao.findAll());
    }

    @Test
    void testFindAllByRecipientId() {
        Account account = insertAndGetDummyAccount();
        Group groupA = insertAndGetDummyGroup_1(account);
        Group groupB = insertAndGetDummyGroup_2(account);
        GroupMessage messageA = new GroupMessage();
        messageA.setAuthor(account);
        messageA.setRecipient(groupA);
        messageA.setText("Hello");
        messageA.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 1));
        groupMessageDao.insert(messageA);

        GroupMessage messageB = new GroupMessage();
        messageB.setAuthor(account);
        messageB.setRecipient(groupB);
        messageB.setText("Hello again");
        messageB.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 2));
        groupMessageDao.insert(messageB);

        GroupMessage messageC = new GroupMessage();
        messageC.setAuthor(account);
        messageC.setRecipient(groupA);
        messageC.setText("other message");
        messageC.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 3));
        groupMessageDao.insert(messageC);
        assertEquals(asList(messageC, messageA), groupMessageDao.findByRecipientId(groupA.getId(), 2, 0));
        assertEquals(singletonList(messageB), groupMessageDao.findByRecipientId(groupB.getId(), 2, 0));
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(groupMessageDao.findAll().isEmpty());
    }

    @Test
    void testDeleteById() {
        GroupMessage message = new GroupMessage();
        Account account = insertAndGetDummyAccount();
        message.setAuthor(account);
        message.setText("Hello");
        message.setRecipient(insertAndGetDummyGroup_1(account));
        groupMessageDao.insert(message);
        assertNotNull(groupMessageDao.findById(message.getId()));
        assertEquals(message, groupMessageDao.deleteById(message.getId()));
        assertNull(groupMessageDao.findById(message.getId()));
    }

    private Account insertAndGetDummyAccount() {
        Account account = new Account();
        account.setFirstName("Jacob");
        account.setMiddleName("Vazovsky");
        account.setLastName("Junior");
        account.setEmail("jvazovsky@gmail.com");
        account.setPassword("pass");
        account.setRegisterDate(valueOf(of(2022, 4, 4)));
        Role role = new Role("ROLE_USER");
        role.setId(1);
        account.addRole(role);
        accountDao.insert(account);
        return account;
    }

    private Group insertAndGetDummyGroup_1(Account creator) {
        Group group = new Group();
        group.setName("Planets");
        group.setCreator(creator);
        group.setCreationDate(valueOf(of(2022, 4, 4)));
        groupDao.insert(group);
        return group;
    }

    private Group insertAndGetDummyGroup_2(Account creator) {
        Group group = new Group();
        group.setName("Suns");
        group.setCreator(creator);
        group.setCreationDate(valueOf(of(2022, 4, 4)));
        groupDao.insert(group);
        return group;
    }

}