package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.dao.impl.PhoneDaoImplTest.getDummyPhones;
import static com.getjavajob.ivanaham.domain.Phone.Type.HOME;
import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static java.sql.Date.valueOf;
import static java.time.LocalDate.now;
import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.dao.interfaces.PhoneDao;
import com.getjavajob.ivanaham.dao.interfaces.RoleDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Phone;
import com.getjavajob.ivanaham.domain.Role;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class AccountDaoImplTest {

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private FriendshipDao friendshipDao;

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PhoneDao phoneDao;

    @PersistenceContext
    private EntityManager em;

    protected static Account getDummyAccount() {
        Account account = new Account();
        account.setFirstName("Mia");
        account.setMiddleName("Vanda");
        account.setLastName("Dolche");
        account.setEmail("mia@mail.ru");
        account.setPassword("123456");
        Role role = new Role("ROLE_USER");
        role.setId(1);
        account.addRole(role);
        account.setRegisterDate(valueOf(of(2022, 1, 1)));
        return account;
    }

    protected static List<Account> getDummyAccounts() {
        List<Account> accounts = new ArrayList<>();
        Account accountA = new Account();
        accountA.setFirstName("Jacob");
        accountA.setMiddleName("Vazovsky");
        accountA.setLastName("Junior");
        accountA.setEmail("jvazovsky@gmail.com");
        accountA.setPassword("pass");
        accountA.setRegisterDate(valueOf(of(2022, 4, 4)));
        Role role = new Role("ROLE_USER");
        role.setId(1);
        accountA.addRole(role);
        accountA.setBirthdate(Date.valueOf(LocalDate.of(2000, 1, 1)));
        accountA.setHomeAddress("St. Petersburg");
        accountA.setWorkAddress("Moscow");
        accountA.setIcq(124423489);
        accountA.setBirthdate(valueOf(of(2000, 4, 4)));
        accountA.setSkype("live:9832013");
        accountA.setAdditional("love flowers");
        accounts.add(accountA);

        Account accountB = new Account();
        accountB.setBirthdate(Date.valueOf(LocalDate.of(2000, 1, 1)));
        accountB.setFirstName("Falcon");
        accountB.setMiddleName("Ibraghim");
        accountB.setLastName("Dealer");
        accountB.setEmail("fibraghim@mail.ru");
        accountB.setPassword("pass2");
        accountB.setRegisterDate(valueOf(of(2022, 1, 1)));
        accountB.addRole(role);
        accountB.setHomeAddress("Kiev");
        accountB.setWorkAddress("Harkov");
        accountB.setBirthdate(valueOf(of(1999, 1, 1)));
        accountB.setIcq(1726322);
        accountB.setSkype("live:28399");
        accountB.setAdditional("hate winter");
        accounts.add(accountB);

        Account accountC = new Account();
        accountC.setFirstName("Persi");
        accountC.setLastName("Jackson");
        accountC.setEmail("pjackson@gmail.com");
        accountC.setPassword("pass3");
        accountC.setRegisterDate(valueOf(of(2022, 2, 3)));
        accountC.addRole(role);
        accounts.add(accountC);

        Account accountD = new Account();
        accountD.setBirthdate(Date.valueOf(LocalDate.of(2000, 1, 2)));
        accountD.setFirstName("Todoroki");
        accountD.setLastName("Shouto");
        accountD.setEmail("tshouto@gmail.com");
        accountD.setPassword("pass4");
        accountD.setRegisterDate(valueOf(of(2022, 3, 3)));
        role.setId(2);
        role.setName("ROLE_ADMIN");
        accountD.addRole(role);
        accounts.add(accountD);
        return accounts;
    }

    @Test
    void testFindById() {
        Phone phone = new Phone();
        phone.setType(HOME);
        phone.setNumber("38845304545");
        Account account = new Account();
        account.setRegisterDate(Date.valueOf(now()));
        account.setFirstName("Barry");
        account.setLastName("Gordon");
        account.setEmail("barry@gmail.com");
        account.setPassword("123456");
        account.setRoles(new HashSet<>(asList(roleDao.findByName("ROLE_USER"), roleDao.findByName("ROLE_ADMIN"))));
        phone.setAccount(account);
        account.addPhone(phone);
        accountDao.insert(account);
        Account dbAccount = accountDao.findById(account.getId());
        assertEquals(account, dbAccount);
    }

    @Test
    void testFindById_AcctNotExist() {
        assertNull(accountDao.findById(101));
    }

    @Test
    void testFindAll() {
        List<Account> accounts = getDummyAccounts();
        accounts.forEach(account -> accountDao.insert(account));
        assertEquals(accounts, accountDao.findAll());
    }

    @Test
    @Disabled(value = "H2 has no DATE_PART function")
    void testFindCurrentBirthdays() {
        List<Account> accounts = getDummyAccounts();
        accounts.forEach(account -> accountDao.insert(account));
        assertEquals(asList(accounts.get(0), accounts.get(1)), accountDao.findCurrentBirthdays());
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(accountDao.findAll().isEmpty());
    }

    @Test
    void testInsert() {
        Account account = getDummyAccount();
        account.addPhone(getDummyPhones(account).get(0));
        accountDao.insert(account);
        assertEquals(account, accountDao.findById(account.getId()));
    }

    @Test
    void testFindByEmail() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        assertEquals(account, accountDao.findByEmail(account.getEmail()));
    }

    @Test
    void testFindByEmail_AcctNotExist() {
        assertNull(accountDao.findByEmail("another@gmail.com"));
    }

    @Test
    void testUpdate() {
        Account account = getDummyAccount();
        Phone phone = new Phone();
        phone.setType(HOME);
        phone.setNumber("38093341111");
        phone.setAccount(account);
        account.addPhone(phone);
        phone = new Phone();
        phone.setType(WORK);
        phone.setNumber("38093342222");
        phone.setAccount(account);
        account.addPhone(phone);
        accountDao.insert(account);

        account = accountDao.findById(account.getId());
        account.setFirstName("Giorno");
        account.setLastName("Jostar");
        account.setEmail("giorno@gmail.com");
        account.addRole(roleDao.findByName("ROLE_ADMIN"));
        account.setMiddleName("N/A");
        account.setHomeAddress("St. Petersburg");
        account.setWorkAddress("Moscow");
        account.setIcq(34341241);
        account.setBirthdate(valueOf(of(2000, 4, 4)));
        account.setSkype("live:2134124");
        account.setAdditional("love flowers");
        account.getPhones().clear();
        phone = new Phone();
        phone.setType(HOME);
        phone.setNumber("38093345555");
        phone.setAccount(account);
        account.addPhone(phone);
        accountDao.update(account);
        assertEquals(account, accountDao.findById(account.getId()));
    }

    @Test
    void testDeleteById() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        assertNotNull(accountDao.findById(account.getId()));
        accountDao.deleteById(account.getId());
        assertNull(accountDao.findById(account.getId()));
    }

    @Test
    void testDeleteById_CascadeCheck() {
        Account accountA = getDummyAccount();
        Phone phone = new Phone();
        phone.setType(HOME);
        phone.setNumber("38093345555");
        phone.setAccount(accountA);
        accountA.addPhone(phone);
        accountDao.insert(accountA);
        Friendship friendship = new Friendship();
        friendship.setAccountA(accountA);
        friendship.setLastActedAccount(accountA);
        friendship.setStatus(PENDING);
        Account accountB = getDummyAccounts().get(0);
        accountDao.insert(accountB);
        friendship.setAccountB(accountB);
        friendshipDao.insert(friendship);
        Group group = new Group();
        group.setCreator(accountA);
        group.setName("Watermelon");
        group.setCreationDate(Date.valueOf(now()));
        groupDao.insert(group);

        assertNotNull(accountDao.findById(accountA.getId()));
        assertNotNull(groupDao.findById(group.getId()));
        assertNotNull(friendshipDao.findById(friendship.getId()));
        assertNotNull(phoneDao.findById(phone.getId()));
        accountDao.deleteById(accountA.getId());
        assertNotNull(groupDao.findById(group.getId()));
        assertNotNull(friendshipDao.findById(friendship.getId()));
        assertNull(accountDao.findById(accountA.getId()));
        assertNull(phoneDao.findById(phone.getId()));
    }

    @Test
    void testDeleteById_NotExist() {
        assertNull(accountDao.deleteById(10));
    }

    @Test
    void testResetPassword() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        String previousPassword = account.getPassword();
        accountDao.resetPasswordById(account.getId(), "Banana");
        account = accountDao.findById(account.getId());
        assertNotEquals(previousPassword, account.getPassword());
        assertEquals("Banana", account.getPassword());
    }

    @Test
    void testFindByName_AcctNotExist() {
        assertTrue(accountDao.findByName("Gorilla Sanderson", 5, 0).isEmpty());
    }

    @ParameterizedTest
    @ValueSource(strings = {"mi", "va", "do", "Mia Vanda", "Mia Vanda Dolche"})
    void testFindByName(String name) {
        Account account = getDummyAccount();
        accountDao.insert(account);
        List<Account> resultAccounts = accountDao.findByName(name, 5, 0);
        assertTrue(resultAccounts.contains(account));
        assertEquals(1, resultAccounts.size());
    }

    @Test
    void testFindByName_UpToLimit() {
        List<Account> accounts = getDummyAccounts();
        accounts.forEach(account -> accountDao.insert(account));
        List<Account> resultAccounts = accountDao.findByName("J", 1, 0);
        assertTrue(resultAccounts.contains(accounts.get(0)));
        assertEquals(1, resultAccounts.size());
    }

    @Test
    void testFindByName_UpToLimitWithOffset() {
        List<Account> accounts = getDummyAccounts();
        accounts.forEach(account -> accountDao.insert(account));
        List<Account> resultAccounts = accountDao.findByName("J", 1, 1);
        assertTrue(resultAccounts.contains(accounts.get(2)));
        assertEquals(1, resultAccounts.size());
    }

}
