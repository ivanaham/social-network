package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccount;
import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccounts;
import static java.sql.Date.valueOf;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Image;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class GroupDaoImplTest {

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private AccountDao accountDao;

    protected static List<Group> getDummyGroups() {
        List<Group> groups = new ArrayList<>();
        Group groupA = new Group();
        groupA.setName("Hurricanes");
        groupA.setCreationDate(valueOf("2022-04-04"));
        groupA.setDescription("Amazing people here");
        groups.add(groupA);

        Group groupB = new Group();
        groupB.setName("Cute cats");
        groupB.setDescription("we love cats");
        groupB.setCreationDate(valueOf("2022-04-05"));
        groups.add(groupB);

        Group groupC = new Group();
        groupC.setName("Classic music");
        groupC.setCreationDate(valueOf("2022-04-06"));
        groups.add(groupC);

        Group groupD = new Group();
        groupD.setName("Soccer 2022");
        groupD.setCreationDate(valueOf("2022-04-07"));
        groups.add(groupD);
        return groups;
    }

    @Test
    void testInsert() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        assertEquals(group, groupDao.findById(group.getId()));
    }

    @Test
    void testUpdate() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        group = groupDao.findById(group.getId());
        group.setName("Ramen");
        group.setDescription("We are together!");
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 127});
        image.setName("Banana-glasses.png");
        group.setProfileImage(image);
        groupDao.update(group);
        assertEquals(group, groupDao.findById(group.getId()));
    }

    @Test
    void testFindById() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        assertEquals(group, groupDao.findById(group.getId()));
    }

    @Test
    void whenLookingByIdWhichNotExist() {
        assertNull(groupDao.findById(100));
    }

    @Test
    void testFindAll() {
        List<Group> groups = getDummyGroups();
        List<Account> accounts = getDummyAccounts();
        for (int i = 0; i < groups.size(); i++) {
            Group currentGroup = groups.get(i);
            Account currentAccount = accounts.get(i);
            currentGroup.setCreator(currentAccount);
            accountDao.insert(currentAccount);
            groupDao.insert(currentGroup);
        }
        assertEquals(groups, groupDao.findAll());
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(groupDao.findAll().isEmpty());
    }

    @Test
    void testDeleteById() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        groupDao.deleteById(group.getId());
        assertNull(groupDao.findById(group.getId()));
    }

    @Test
    void testDeleteById_NotExist() {
        assertNull(groupDao.deleteById(10));
    }

    @Test
    void testFindByName_GroupNotExist() {
        assertTrue(groupDao.findByName("Some group", 5, 0).isEmpty());
    }

    @Test
    void testFindByName() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        assertTrue(groupDao.findByName("hur", 5, 0).contains(group));
    }

    @Test
    void testFindByName_Pagination() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Group groupA = getDummyGroups().get(1);
        groupA.setCreator(account);
        groupDao.insert(groupA);
        Group groupB = getDummyGroups().get(2);
        groupB.setCreator(account);
        groupDao.insert(groupB);
        assertEquals(singletonList(groupA), groupDao.findByName("c", 1, 0));
        assertEquals(singletonList(groupB), groupDao.findByName("c", 1, 1));
    }

}
