package com.getjavajob.ivanaham.dao.impl;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.RoleDao;
import com.getjavajob.ivanaham.domain.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class RoleDaoImplTest {

    @Autowired
    private RoleDao roleDao;

    @Test
    void testInsertAndFindById() {
        Role role = new Role("SUPERMAN");
        roleDao.insert(role);
        assertTrue(role.getId() > 0);
        assertEquals(role, roleDao.findById(role.getId()));
    }

    @Test
    void testFindAll() {
        Role user = new Role("ROLE_USER");
        user.setId(1);
        Role admin = new Role("ROLE_ADMIN");
        admin.setId(2);
        assertTrue(roleDao.findAll().containsAll(asList(user, admin)));
        assertEquals(2, roleDao.findAll().size());
    }

    @Test
    void testUpdate() {
        Role role = new Role("SUPERMAN");
        roleDao.insert(role);
        role.setName("ROLE_TREE");
        roleDao.update(role);
        assertEquals(role, roleDao.findById(role.getId()));
    }

    @Test
    void testDeleteById() {
        Role role = new Role("SUPERMAN");
        roleDao.insert(role);
        roleDao.deleteById(role.getId());
        assertNull(roleDao.findById(role.getId()));
    }

    @Test
    void testDeleteById_NotExist() {
        assertNull(roleDao.deleteById(10));
    }

    @Test
    void testFindByName() {
        Role role = roleDao.findById(1);
        assertEquals(role, roleDao.findByName(role.getName()));
    }

    @Test
    void testFindByName_NotExist() {
        assertNull(roleDao.findByName("ROLE_POTATO"));
    }

}