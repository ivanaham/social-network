package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccounts;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.RequestType.INCOMING;
import static com.getjavajob.ivanaham.domain.enums.RequestType.OUTGOING;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Friendship;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class FriendshipDaoImplTest {

    @Autowired
    private FriendshipDao friendshipDao;

    @Autowired
    private AccountDao accountDao;

    private Account accountA;

    @Test
    void testInsert() {
        Friendship friendship = getDummyFriendship();
        friendshipDao.insert(friendship);
        assertEquals(friendship,
                friendshipDao.findFriendship(friendship.getAccountA().getId(), friendship.getAccountB().getId()));
    }

    @Test
    void testFindFriendship_NotExist() {
        assertNull(friendshipDao.findFriendship(1, 2));
    }

    @Test
    void testUpdate() {
        Friendship friendship = getDummyFriendship();
        friendshipDao.insert(friendship);
        friendship = friendshipDao.findFriendship(friendship.getAccountA().getId(), friendship.getAccountB().getId());
        friendship.setStatus(DECLINED);
        friendship.setLastActedAccount(friendship.getAccountB());
        friendshipDao.update(friendship);
        assertEquals(friendship,
                friendshipDao.findFriendship(friendship.getAccountA().getId(), friendship.getAccountB().getId()));
    }

    @Test
    void testDeleteByFriendsId_True() {
        Friendship friendship = getDummyFriendship();
        friendshipDao.insert(friendship);
        assertTrue(friendshipDao.deleteFriendship(friendship.getAccountA().getId(), friendship.getAccountB().getId()));
        assertNull(friendshipDao.findFriendship(friendship.getAccountA().getId(), friendship.getAccountB().getId()));
    }

    @Test
    void testDeleteByFriendsId_False() {
        assertFalse(friendshipDao.deleteFriendship(10, 20));
    }

    @Test
    void testFindById() {
        Friendship friendship = getDummyFriendship();
        friendshipDao.insert(friendship);
        assertEquals(friendship, friendshipDao.findById(friendship.getId()));
    }

    @Test
    void testFindById_NotExist() {
        assertNull(friendshipDao.findById(10));
    }

    @Test
    void testDeleteById() {
        Friendship friendship = getDummyFriendship();
        friendshipDao.insert(friendship);
        assertEquals(friendship, friendshipDao.deleteById(friendship.getId()));
        assertNull(friendshipDao.findById(friendship.getId()));
    }

    @Test
    void testDeleteById_NotExist() {
        assertNull(friendshipDao.deleteById(10));
    }

    @Test
    void testFind_WithPagination() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = new Friendship();
        Account accountB = getDummyAccounts().get(2);
        accountDao.insert(accountB);
        friendshipB.setAccountA(accountA);
        friendshipB.setAccountB(accountB);
        friendshipB.setStatus(PENDING);
        friendshipB.setLastActedAccount(accountA);
        friendshipDao.insert(friendshipA);
        friendshipDao.insert(friendshipB);
        assertEquals(singletonList(friendshipA), friendshipDao.find(accountA.getId(), PENDING, 1, 0));
        assertEquals(singletonList(friendshipB), friendshipDao.find(accountA.getId(), PENDING, 1, 1));
    }

    @Test
    void testFind() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = new Friendship();
        Account accountB = getDummyAccounts().get(2);
        accountDao.insert(accountB);
        friendshipB.setAccountA(accountA);
        friendshipB.setAccountB(accountB);
        friendshipB.setStatus(PENDING);
        friendshipB.setLastActedAccount(accountA);
        friendshipDao.insert(friendshipA);
        friendshipDao.insert(friendshipB);
        assertEquals(asList(friendshipA, friendshipB), friendshipDao.find(accountA.getId(), PENDING));
    }

    @Test
    void testFindPending() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = new Friendship();
        Account accountB = getDummyAccounts().get(2);
        accountDao.insert(accountB);
        friendshipB.setAccountA(accountA);
        friendshipB.setAccountB(accountB);
        friendshipB.setStatus(PENDING);
        friendshipB.setLastActedAccount(accountA);
        friendshipDao.insert(friendshipA);
        friendshipDao.insert(friendshipB);
        assertEquals(asList(friendshipA, friendshipB), friendshipDao.findPending(accountA.getId(), OUTGOING, 2, 0));
        assertEquals(singletonList(friendshipB), friendshipDao.findPending(accountA.getId(), OUTGOING, 1, 1));
        assertEquals(emptyList(), friendshipDao.findPending(accountA.getId(), INCOMING, 1, 0));
    }

    @Test
    void testFindByAccountId_Empty() {
        assertTrue(friendshipDao.find(1, PENDING, 1, 1).isEmpty());
    }

    @Test
    void testFindAll() {
        Friendship friendshipA = getDummyFriendship();
        Friendship friendshipB = new Friendship();
        Account accountB = getDummyAccounts().get(2);
        accountDao.insert(accountB);
        friendshipB.setAccountA(accountA);
        friendshipB.setAccountB(accountB);
        friendshipB.setStatus(PENDING);
        friendshipB.setLastActedAccount(accountA);
        friendshipDao.insert(friendshipA);
        friendshipDao.insert(friendshipB);
        assertEquals(asList(friendshipA, friendshipB), friendshipDao.findAll());
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(friendshipDao.findAll().isEmpty());
    }

    private Friendship getDummyFriendship() {
        Friendship friendship = new Friendship();
        accountA = getDummyAccounts().get(0);
        Account accountB = getDummyAccounts().get(1);
        accountDao.insert(accountA);
        accountDao.insert(accountB);
        friendship.setAccountA(accountA);
        friendship.setAccountB(accountB);
        friendship.setLastActedAccount(accountA);
        friendship.setStatus(PENDING);
        return friendship;
    }

}
