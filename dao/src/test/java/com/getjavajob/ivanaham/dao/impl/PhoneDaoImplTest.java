package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccount;
import static com.getjavajob.ivanaham.domain.Phone.Type.HOME;
import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.PhoneDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Phone;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class PhoneDaoImplTest {

    @Autowired
    private PhoneDao phoneDao;

    @Autowired
    private AccountDao accountDao;

    protected static List<Phone> getDummyPhones(Account account) {
        List<Phone> phones = new ArrayList<>();
        Phone dummyPhoneA = new Phone();
        Phone dummyPhoneB = new Phone();
        dummyPhoneA.setType(HOME);
        dummyPhoneA.setNumber("38093452121");
        dummyPhoneA.setAccount(account);
        dummyPhoneB.setType(WORK);
        dummyPhoneB.setNumber("38093454444");
        dummyPhoneB.setAccount(account);
        phones.add(dummyPhoneA);
        phones.add(dummyPhoneB);
        return phones;
    }

    @Test
    void testInsertAndFindAccountPhones() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Set<Phone> phones = new HashSet<>(getDummyPhones(account));
        account.setPhones(phones);
        phoneDao.insert(account.getPhones());
        assertEquals(phones, phoneDao.findByAccountId(account.getId()));
    }

    @Test
    void testFindAll() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        List<Phone> phones = getDummyPhones(account);
        account.setPhones(new HashSet<>(phones));
        phoneDao.insert(account.getPhones());
        assertTrue(phoneDao.findAll().containsAll(phones));
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(phoneDao.findAll().isEmpty());
    }

    @Test
    void testInsertAndFindById() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Phone phone = getDummyPhones(account).get(0);
        phone.setAccount(account);
        phoneDao.insert(phone);
        assertEquals(phone, phoneDao.findById(phone.getId()));
    }

    @Test
    void testDeleteById() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Phone phone = getDummyPhones(account).get(0);
        phone.setAccount(account);
        phoneDao.insert(phone);
        assertEquals(phone, phoneDao.deleteById(phone.getId()));
        assertNull(phoneDao.findById(phone.getId()));
    }

    @Test
    void testDeleteById_NotExist() {
        assertNull(phoneDao.deleteById(10));
    }

    @Test
    void testUpdate() {
        Account account = getDummyAccount();
        accountDao.insert(account);
        Phone phone = getDummyPhones(account).get(0);
        phone.setAccount(account);
        phoneDao.insert(phone);
        phone.setType(WORK);
        phone.setNumber("1120333423");
        phoneDao.update(phone);
        assertEquals(phone, phoneDao.findById(phone.getId()));
    }

    @Test
    void testFindById_NotExist() {
        assertNull(phoneDao.findById(10));
    }

    @Test
    void testFindByAccountId_Empty() {
        assertTrue(phoneDao.findByAccountId(10).isEmpty());
    }

    @Test
    void testDeleteByAccountId() {
        Account account = getDummyAccount();
        List<Phone> phones = getDummyPhones(account);
        account.setPhones(new HashSet<>(phones));
        accountDao.insert(account);
        assertTrue(phoneDao.deleteByAccountId(account.getId()) > 0);
        assertTrue(phoneDao.findByAccountId(account.getId()).isEmpty());
    }

}
