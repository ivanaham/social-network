package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.domain.Message.Type.ACCOUNT;
import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static java.sql.Date.valueOf;
import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.AccountMessageDao;
import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.domain.Role;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class AccountMessageDaoImplTest {

    @Autowired
    private AccountMessageDao accountMessageDao;

    @Autowired
    private FriendshipDao friendshipDao;

    @Autowired
    private AccountDao accountDao;

    @Test
    void testInsertAndFindById() {
        AccountMessage message = new AccountMessage();
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        message.setAttachedImage(image);
        message.setAuthor(insertAndGetDummyAccount_1());
        message.setText("Hello");
        message.setRecipient(insertAndGetDummyAccount_2());
        accountMessageDao.insert(message);
        assertEquals(message, accountMessageDao.findById(message.getId()));
    }

    @Test
    void testUpdate() {
        AccountMessage message = new AccountMessage();
        message.setAuthor(insertAndGetDummyAccount_1());
        message.setRecipient(insertAndGetDummyAccount_2());
        message.setText("Hello");
        accountMessageDao.insert(message);
        message = accountMessageDao.findById(message.getId());
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        message.setAttachedImage(image);
        message.setText("Hello man)");
        accountMessageDao.update(message);
        assertEquals(message, accountMessageDao.findById(message.getId()));
    }

    @Test
    void testFindById_Absent() {
        assertNull(accountMessageDao.findById(100));
    }

    @Test
    void testFindAll() {
        AccountMessage messageA = new AccountMessage();
        Image image = new Image();
        image.setImageBytes(new byte[]{1, 2, 3, 4, 45, -120});
        image.setName("something.png");
        messageA.setAttachedImage(image);
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        messageA.setAuthor(account1);
        messageA.setText("Hello");
        messageA.setRecipient(account2);
        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account1);
        messageB.setText("Hello again");
        messageB.setRecipient(account2);
        accountMessageDao.insert(messageA);
        accountMessageDao.insert(messageB);
        assertEquals(asList(messageA, messageB), accountMessageDao.findAll());
    }

    @Test
    void testFindAll_Empty() {
        assertTrue(accountMessageDao.findAll().isEmpty());
    }

    @Test
    void testDeleteById() {
        AccountMessage message = new AccountMessage();
        Account account = insertAndGetDummyAccount_1();
        message.setAuthor(account);
        message.setText("Hello");
        message.setRecipient(insertAndGetDummyAccount_2());
        accountMessageDao.insert(message);
        assertNotNull(accountMessageDao.findById(message.getId()));
        assertEquals(message, accountMessageDao.deleteById(message.getId()));
        assertNull(accountMessageDao.findById(message.getId()));
    }

    @Test
    void testFindByRecipientId() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account2);
        messageA.setText("Hello");
        messageA.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account1);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageC);
        assertEquals(asList(messageA, messageB), accountMessageDao.findByRecipientId(account2.getId(), ACCOUNT, 2, 0));
    }

    @Test
    void testFindByAuthorId() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account2);
        messageA.setText("Hello");
        messageA.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account1);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageC);
        assertEquals(asList(messageA, messageB), accountMessageDao.findByAuthorId(account1.getId(), ACCOUNT, 2, 0));
    }

    @Test
    void testFindFeedMessages() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account2);
        messageA.setText("Hello");
        messageA.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account1);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageC);
        assertEquals(emptyList(), accountMessageDao.findFeedMessages(account2.getId(), 2, 0));
        Friendship friendship = new Friendship();
        friendship.setAccountA(account1);
        friendship.setAccountB(account2);
        friendship.setLastActedAccount(account1);
        friendship.setStatus(ACCEPTED);
        friendshipDao.insert(friendship);
        assertEquals(asList(messageA, messageB), accountMessageDao.findFeedMessages(account2.getId(), 2, 0));
    }

    @Test
    void testFindAllByRecipientId_Empty() {
        assertTrue(accountMessageDao.findByRecipientId(10, ACCOUNT, 2, 0).isEmpty());
    }

    @Test
    void testFindByParticipantIds() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account2);
        messageA.setText("Hello");
        messageA.setMessageType(ACCOUNT);
        messageA.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 1));
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account1);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(ACCOUNT);
        messageB.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 2));
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 3));
        messageC.setMessageType(ACCOUNT);
        accountMessageDao.insert(messageC);
        assertEquals(asList(messageC, messageB, messageA),
                accountMessageDao.findByParticipantIds(account1.getId(), account2.getId(), ACCOUNT, 3, 0));
        assertEquals(asList(messageC, messageB),
                accountMessageDao.findByParticipantIds(account1.getId(), account2.getId(), ACCOUNT, 2, 0));
        assertEquals(singletonList(messageA),
                accountMessageDao.findByParticipantIds(account1.getId(), account2.getId(), ACCOUNT, 1, 2));
    }

    @Test
    void testFindByParticipantIds_Empty() {
        assertTrue(accountMessageDao.findByParticipantIds(10, 15, ACCOUNT, 3, 0).isEmpty());
    }

    @Test
    void testFindConversationsLastMessagesByAccountId() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        Account account3 = insertAndGetDummyAccount_3();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account3);
        messageA.setText("Hello");
        messageA.setMessageType(DIRECT);
        messageA.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 1));
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account3);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(DIRECT);
        messageB.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 2));
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 3));
        messageC.setMessageType(DIRECT);
        accountMessageDao.insert(messageC);
        assertEquals(asList(messageB, messageA),
                accountMessageDao.findLastMessagesByAccountId(account3.getId(), 2, 0));
        assertEquals(singletonList(messageB),
                accountMessageDao.findLastMessagesByAccountId(account3.getId(), 1, 0));
        assertEquals(singletonList(messageA),
                accountMessageDao.findLastMessagesByAccountId(account3.getId(), 1, 1));
    }

    @Test
    void testFindConversationsLastMessagesByAccountIdExcluding() {
        Account account1 = insertAndGetDummyAccount_1();
        Account account2 = insertAndGetDummyAccount_2();
        Account account3 = insertAndGetDummyAccount_3();
        AccountMessage messageA = new AccountMessage();
        messageA.setAuthor(account1);
        messageA.setRecipient(account3);
        messageA.setText("Hello");
        messageA.setMessageType(DIRECT);
        messageA.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 1));
        accountMessageDao.insert(messageA);

        AccountMessage messageB = new AccountMessage();
        messageB.setAuthor(account3);
        messageB.setRecipient(account2);
        messageB.setText("Hello again");
        messageB.setMessageType(DIRECT);
        messageB.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 2));
        accountMessageDao.insert(messageB);

        AccountMessage messageC = new AccountMessage();
        messageC.setAuthor(account2);
        messageC.setRecipient(account1);
        messageC.setText("other message");
        messageC.setDateTimeSent(LocalDateTime.of(2000, 1, 1, 1, 3));
        messageC.setMessageType(DIRECT);
        accountMessageDao.insert(messageC);
        assertEquals(singletonList(messageB),
                accountMessageDao.findLastMessagesByAccountIdExcluding(account3.getId(), account1.getId(), 2, 0));
    }

    @Test
    void testFindConversationsLastMessagesByAccountId_Empty() {
        assertTrue(accountMessageDao.findLastMessagesByAccountId(10, 10, 0).isEmpty());
    }

    private Account insertAndGetDummyAccount_1() {
        Account account = new Account();
        account.setFirstName("Jacob");
        account.setMiddleName("Vazovsky");
        account.setLastName("Junior");
        account.setEmail("jvazovsky@gmail.com");
        account.setPassword("pass");
        account.setRegisterDate(valueOf(of(2022, 4, 4)));
        Role role = new Role("ROLE_USER");
        role.setId(1);
        account.addRole(role);
        accountDao.insert(account);
        return account;
    }

    private Account insertAndGetDummyAccount_2() {
        Account account = new Account();
        account.setFirstName("Jacob");
        account.setMiddleName("Vazovsky");
        account.setLastName("Junior");
        account.setEmail("junior@gmail.com");
        account.setPassword("pass");
        account.setRegisterDate(valueOf(of(2022, 4, 4)));
        Role role = new Role("ROLE_USER");
        role.setId(1);
        account.addRole(role);
        accountDao.insert(account);
        return account;
    }

    private Account insertAndGetDummyAccount_3() {
        Account account = new Account();
        account.setFirstName("Jacob");
        account.setMiddleName("Vazovsky");
        account.setLastName("Junior");
        account.setEmail("jacob@gmail.com");
        account.setPassword("pass");
        account.setRegisterDate(valueOf(of(2022, 4, 4)));
        Role role = new Role("ROLE_USER");
        role.setId(1);
        account.addRole(role);
        accountDao.insert(account);
        return account;
    }

}