package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccount;
import static com.getjavajob.ivanaham.dao.impl.AccountDaoImplTest.getDummyAccounts;
import static com.getjavajob.ivanaham.dao.impl.GroupDaoImplTest.getDummyGroups;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.dao.DataConfig;
import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Membership;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = DataConfig.class)
class MembershipDaoImplTest {

    @Autowired
    private MembershipDao membershipDao;

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private GroupDao groupDao;

    @Test
    void testInsert() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        assertEquals(membership, membershipDao.findById(membership.getId()));
    }

    @Test
    void testDeleteById() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        membershipDao.deleteById(membership.getId());
        assertNull(membershipDao.findById(membership.getId()));
    }

    @Test
    void testFindById() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        assertEquals(membership, membershipDao.findById(membership.getId()));
    }

    @Test
    void testFindMembership_NotExist() {
        assertNull(membershipDao.findMembership(10, 10));
    }

    @Test
    void testUpdate() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        membership.setStatus(ACCEPTED);
        membership.setRights(MODERATOR);
        membershipDao.update(membership);
        assertEquals(membership, membershipDao.findById(membership.getId()));
    }

    @Test
    void testDeleteMembership_True() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group group = getDummyGroups().get(0);
        group.setCreator(account);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        membershipDao.deleteMembership(account.getId(), group.getId());
        assertNull(membershipDao.findMembership(account.getId(), group.getId()));
    }

    @Test
    void testDeleteByAccountId() {
        Membership membership = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membership.setAccount(account);
        Group groupA = getDummyGroups().get(0);
        groupA.setCreator(account);
        groupDao.insert(groupA);
        membership.setGroup(groupA);
        membershipDao.insert(membership);
        membership = getDummyMembership();
        membership.setAccount(account);
        Group groupB = getDummyGroups().get(1);
        groupB.setCreator(account);
        groupDao.insert(groupB);
        membership.setGroup(groupB);
        membershipDao.insert(membership);
        membershipDao.deleteByAccountId(account.getId());
        assertNull(membershipDao.findMembership(account.getId(), groupA.getId()));
        assertNull(membershipDao.findMembership(account.getId(), groupB.getId()));
    }

    @Test
    void testDeleteByGroupId() {
        Membership membership = getDummyMembership();
        Account accountA = getDummyAccounts().get(0);
        accountDao.insert(accountA);
        membership.setAccount(accountA);
        Group group = getDummyGroups().get(0);
        group.setCreator(accountA);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        membership = getDummyMembership();
        membership.setGroup(group);
        Account accountB = getDummyAccounts().get(1);
        accountDao.insert(accountB);
        membership.setAccount(accountB);
        membershipDao.insert(membership);
        membershipDao.deleteByGroupId(group.getId());
        assertNull(membershipDao.findMembership(accountA.getId(), group.getId()));
        assertNull(membershipDao.findMembership(accountB.getId(), group.getId()));
    }

    @Test
    void testFindAll() {
        List<Membership> memberships = new ArrayList<>();
        Membership membership = getDummyMembership();
        Account accountA = getDummyAccounts().get(0);
        accountDao.insert(accountA);
        membership.setAccount(accountA);
        Group group = getDummyGroups().get(0);
        group.setCreator(accountA);
        groupDao.insert(group);
        membership.setGroup(group);
        membershipDao.insert(membership);
        memberships.add(membership);
        membership = getDummyMembership();
        membership.setGroup(group);
        Account accountB = getDummyAccounts().get(1);
        accountDao.insert(accountB);
        membership.setAccount(accountB);
        membershipDao.insert(membership);
        memberships.add(membership);
        assertEquals(memberships, membershipDao.findAll());
    }

    @Test
    void testFindByGroupId() {
        Membership membershipA = getDummyMembership();
        Account accountA = getDummyAccounts().get(0);
        accountDao.insert(accountA);
        membershipA.setAccount(accountA);
        Group group = getDummyGroups().get(0);
        group.setCreator(accountA);
        groupDao.insert(group);
        membershipA.setGroup(group);
        membershipDao.insert(membershipA);
        Membership membershipB = getDummyMembership();
        membershipB.setGroup(group);
        Account accountB = getDummyAccounts().get(1);
        accountDao.insert(accountB);
        membershipB.setAccount(accountB);
        membershipDao.insert(membershipB);
        assertEquals(singletonList(membershipA), membershipDao.findByGroupId(group.getId(), PENDING, 1, 0));
        assertEquals(singletonList(membershipB), membershipDao.findByGroupId(group.getId(), PENDING, 1, 1));
        assertEquals(asList(membershipA, membershipB),
                membershipDao.findByGroupId(group.getId(), PENDING, COMMON, 2, 0));
        assertEquals(emptyList(),
                membershipDao.findByGroupId(group.getId(), PENDING, MODERATOR, 2, 0));
    }

    @Test
    void testFindByAccountId() {
        Membership membershipA = getDummyMembership();
        Account account = getDummyAccount();
        accountDao.insert(account);
        membershipA.setAccount(account);
        Group groupA = getDummyGroups().get(0);
        groupA.setCreator(account);
        groupDao.insert(groupA);
        membershipA.setGroup(groupA);
        membershipDao.insert(membershipA);
        Membership membershipB = getDummyMembership();
        membershipB.setAccount(account);
        Group groupB = getDummyGroups().get(1);
        groupB.setCreator(account);
        groupDao.insert(groupB);
        membershipB.setGroup(groupB);
        membershipDao.insert(membershipB);
        assertEquals(singletonList(membershipA), membershipDao.findByAccountId(account.getId(), PENDING, 1, 0));
        assertEquals(singletonList(membershipB), membershipDao.findByAccountId(account.getId(), PENDING, 1, 1));
    }


    @Test
    void testFindByGroupId_Empty() {
        assertTrue(membershipDao.findByGroupId(10, PENDING, 1, 1).isEmpty());
    }

    private Membership getDummyMembership() {
        Membership membership = new Membership();
        membership.setRights(COMMON);
        membership.setStatus(PENDING);
        return membership;
    }

}
