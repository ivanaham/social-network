package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Role;

public interface RoleDao extends Dao<Role> {

    Role findByName(String name);

}
