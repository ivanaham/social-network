package com.getjavajob.ivanaham.dao.impl;

import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.PhoneDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Account_;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDaoImpl implements AccountDao {

    private static final Logger LOGGER = getLogger();

    private final PhoneDao phoneDao;

    @PersistenceContext
    private EntityManager em;

    public AccountDaoImpl(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }

    @Override
    public void insert(Account account) {
        LOGGER.debug("insert() is called; email = {}", account.getEmail());
        em.persist(account);
        LOGGER.debug("insert(): inserted account email = {}, id  {}", account.getEmail(), account.getId());
        account.getPhones().forEach(phone -> phone.setAccount(account));
        phoneDao.insert(account.getPhones());
    }

    @Override
    public Account findById(long id) {
        LOGGER.debug("findById() is called; id = {}", id);
        return em.find(Account.class, id);
    }

    @Override
    public List<Account> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        cq.from(Account.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Account account) {
        LOGGER.debug("update() is called; id = {}", account.getId());
        findById(account.getId());
        account.getPhones().forEach(phone -> phone.setAccount(account));
        em.merge(account);
    }

    @Override
    public Account deleteById(long id) {
        LOGGER.debug("deleteById() is called; id = {}", id);
        Account account = findById(id);
        if (nonNull(account)) {
            em.remove(account);
        }
        return account;
    }

    @Override
    public Account findByEmail(String email) {
        try {
            LOGGER.debug("findByEmail() is called; email = {}", email);
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Account> cq = cb.createQuery(Account.class);
            Root<Account> account = cq.from(Account.class);
            cq.where(cb.equal(account.get(Account_.EMAIL), email));
            return em.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.debug("findByEmail(): returned no result for email = {}", email);
            return null;
        }
    }

    @Override
    public List<Account> findCurrentBirthdays() {
        LOGGER.debug("findCurrentBirthdays() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> account = cq.from(Account.class);
        String datePartFuncName = "DATE_PART";
        cq.where(cb.and(
                cb.equal(cb.function(datePartFuncName, Long.class, cb.literal("day"), account.get(Account_.BIRTHDATE)),
                        cb.function(datePartFuncName, Long.class, cb.literal("day"), cb.currentDate())),
                cb.equal(cb.function(datePartFuncName, Long.class, cb.literal("month"), account.get(Account_.BIRTHDATE)),
                        cb.function(datePartFuncName, Long.class, cb.literal("month"), cb.currentDate()))));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<Account> findByName(String name, int limit, int offset) {
        LOGGER.debug("findByName() is called; name = {}, limit = {}, offset = {}", name, limit, offset);
        String[] splitNameRequest = name.toLowerCase().split(" ");
        Set<Account> accounts = new HashSet<>();
        for (String currentName : splitNameRequest) {
            int currentLimit = limit - accounts.size();
            int currentOffset = subtractTillZero(offset, accounts.size());
            accounts.addAll(findBySingleName(currentName, currentLimit, currentOffset));
            if (accounts.size() == limit) {
                break;
            }
        }
        return new ArrayList<>(accounts);
    }

    @Override
    public void resetPasswordById(long accountId, String password) {
        LOGGER.debug("resetPasswordById() is called; id = {}", accountId);
        Account account = em.find(Account.class, accountId);
        account.setPassword(password);
        em.merge(account);
    }

    private int subtractTillZero(int minuend, int subtrahend) {
        int difference = minuend - subtrahend;
        return difference < 0 ? 0 : difference;
    }

    private Set<Account> findBySingleName(String name, int limit, int offset) {
        Set<Account> accounts = new HashSet<>(findByName(Account_.FIRST_NAME, name, limit, offset));
        if (accounts.size() != limit) {
            int currentLimit = limit - accounts.size();
            int currentOffset = subtractTillZero(offset, accounts.size());
            accounts.addAll(findByName(Account_.MIDDLE_NAME, name, currentLimit, currentOffset));
            if (accounts.size() != limit) {
                currentLimit = limit - accounts.size();
                currentOffset = subtractTillZero(offset, accounts.size());
                accounts.addAll(findByName(Account_.LAST_NAME, name, currentLimit, currentOffset));
            }
        }
        return accounts;
    }

    private List<Account> findByName(String entityFieldName, String name, int limit, int offset) {
        LOGGER.debug("findByName() is called; name = {}, limit = {}, offset = {}", name, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> account = cq.from(Account.class);
        String likePattern = name.toLowerCase() + "%";
        cq.where(cb.like(cb.lower(account.get(entityFieldName)), likePattern));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

}