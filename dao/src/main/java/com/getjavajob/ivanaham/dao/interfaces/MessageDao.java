package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Message;
import java.util.List;

public interface MessageDao extends Dao<Message> {

    List<Message> findAllByRecipientId(long recipientId, Message.Type msgType, int limit, int offset);

    List<Message> findByParticipantIds(long accountA, long accountB, Message.Type msgType, int limit, int offset);

    List<Message> findLastMessagesByAccountId(long accountId, int limit, int offset);

    List<Message> findLastMessagesByAccountIdExcluding(long accountId, long accountIdToExclude, int limit, int offset);

    List<Message> findFeedMessages(long accountId, int limit, int offset);

}
