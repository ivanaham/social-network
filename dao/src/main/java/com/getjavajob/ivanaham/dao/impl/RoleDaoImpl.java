package com.getjavajob.ivanaham.dao.impl;

import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.RoleDao;
import com.getjavajob.ivanaham.domain.Role;
import com.getjavajob.ivanaham.domain.Role_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDaoImpl implements RoleDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Role role) {
        LOGGER.debug("insert() is called; name = {}", role.getName());
        em.persist(role);
        LOGGER.debug("insert(): name = {}, id = {}", role.getName(), role.getId());
    }

    @Override
    public Role findById(long id) {
        LOGGER.debug("findById() is called; id = {}", id);
        return em.find(Role.class, id);
    }

    @Override
    public List<Role> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Role> cq = cb.createQuery(Role.class);
        cq.from(Role.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Role role) {
        LOGGER.debug("update() is called; id = {}", role.getId());
        em.merge(role);
    }

    @Override
    public Role deleteById(long id) {
        LOGGER.debug("deleteById() is called; id = {}", id);
        Role role = em.find(Role.class, id);
        if (nonNull(role)) {
            em.remove(role);
        }
        return role;
    }

    @Override
    public Role findByName(String name) {
        try {
            LOGGER.debug("findByName() is called; name = {}", name);
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Role> cq = cb.createQuery(Role.class);
            Root<Role> role = cq.from(Role.class);
            cq.where(cb.equal(role.get(Role_.NAME), name));
            return em.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            LOGGER.debug("findByName(): returned no result for name = {}", name);
            return null;
        }
    }

}
