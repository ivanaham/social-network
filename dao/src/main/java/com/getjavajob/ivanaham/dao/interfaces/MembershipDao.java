package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import java.util.List;

public interface MembershipDao extends Dao<Membership> {

    List<Membership> findByAccountId(long accountId, RequestStatus status, int limit, int offset);

    List<Membership> findByGroupId(long groupId, RequestStatus status, int limit, int offset);

    List<Membership> findByGroupId(long groupId, RequestStatus status, Rights rights, int limit, int offset);

    Membership findMembership(long accountId, long groupId);

    void deleteByAccountId(long accountId);

    void deleteByGroupId(long groupId);

    void deleteMembership(long accountId, long groupId);

}