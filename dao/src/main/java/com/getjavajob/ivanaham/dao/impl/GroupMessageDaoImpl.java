package com.getjavajob.ivanaham.dao.impl;

import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.GroupMessageDao;
import com.getjavajob.ivanaham.domain.GroupMessage;
import com.getjavajob.ivanaham.domain.GroupMessage_;
import com.getjavajob.ivanaham.domain.Group_;
import com.getjavajob.ivanaham.domain.Message_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class GroupMessageDaoImpl implements GroupMessageDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(GroupMessage message) {
        LOGGER.debug("insert() is called; message = {}", message);
        em.persist(message);
    }

    @Override
    public GroupMessage findById(long messageId) {
        LOGGER.debug("findById() is called; id = {}", messageId);
        return em.find(GroupMessage.class, messageId);
    }

    @Override
    public List<GroupMessage> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<GroupMessage> cq = cb.createQuery(GroupMessage.class);
        cq.from(GroupMessage.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(GroupMessage message) {
        LOGGER.debug("update() is called; message = {}", message);
        em.merge(message);
    }

    @Override
    public GroupMessage deleteById(long messageId) {
        LOGGER.debug("deleteById() is called; id = {}", messageId);
        GroupMessage message = findById(messageId);
        if (nonNull(message)) {
            em.remove(message);
        }
        return message;
    }

    @Override
    public List<GroupMessage> findByRecipientId(long recipientId, int limit, int offset) {
        LOGGER.debug("findByRecipientId() is called; id = {}, limit = {}, offset = {}",
                recipientId, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<GroupMessage> cq = cb.createQuery(GroupMessage.class);
        Root<GroupMessage> message = cq.from(GroupMessage.class);
        cq.where(cb.equal(message.get(GroupMessage_.RECIPIENT).get(Group_.ID), recipientId));
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

}
