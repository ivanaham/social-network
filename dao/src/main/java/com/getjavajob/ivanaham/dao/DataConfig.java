package com.getjavajob.ivanaham.dao;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan("com.getjavajob.ivanaham.dao")
@EntityScan("com.getjavajob.ivanaham.domain")
@EnableJpaRepositories(basePackages = "com.getjavajob.ivanaham.dao.impl")
public class DataConfig {

}
