package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.Message;
import java.util.List;

public interface AccountMessageDao extends Dao<AccountMessage> {

    List<AccountMessage> findByRecipientId(long recipientId, Message.Type msgType, int limit, int offset);

    List<AccountMessage> findByAuthorId(long recipientId, Message.Type msgType, int limit, int offset);

    List<AccountMessage> findByParticipantIds(long accountA, long accountB, Message.Type msgType, int limit, int offset);

    List<AccountMessage> findLastMessagesByAccountId(long accountId, int limit, int offset);

    List<AccountMessage> findLastMessagesByAccountIdExcluding(long accountId, long accountIdToExclude, int limit, int offset);

    List<AccountMessage> findFeedMessages(long accountId, int limit, int offset);

}
