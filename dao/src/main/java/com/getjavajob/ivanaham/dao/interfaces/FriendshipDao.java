package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.RequestType;
import java.util.List;

public interface FriendshipDao extends Dao<Friendship> {

    Friendship findFriendship(long accountIdA, long accountIdB);

    List<Friendship> find(long accountId, RequestStatus status, int limit, int offset);


    List<Friendship> find(long accountId, RequestStatus status);

    /**
     * Find friendships by account id and request type (only for RequestStatus - pending)
     */
    List<Friendship> findPending(long accountId, RequestType type, int limit, int offset);

    boolean deleteFriendship(long accountIdA, long accountIdB);

}
