package com.getjavajob.ivanaham.dao.interfaces;

import java.util.List;

public interface Dao<T> {

    void insert(T t);

    T findById(long id);

    List<T> findAll();

    void update(T t);

    T deleteById(long id);

}
