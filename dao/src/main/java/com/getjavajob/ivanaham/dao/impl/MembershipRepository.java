package com.getjavajob.ivanaham.dao.impl;

import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
interface MembershipRepository extends PagingAndSortingRepository<Membership, Long> {

    List<Membership> findByAccountIdAndStatus(long accountId, RequestStatus status, Pageable pageable);

    List<Membership> findByGroupIdAndStatus(long groupId, RequestStatus status, Pageable pageable);

    List<Membership> findByGroupIdAndStatusAndRights(long groupId, RequestStatus status, Rights rights, Pageable pageable);

    Optional<Membership> findByAccountIdAndGroupId(long accountId, long groupId);

    void deleteByAccountId(long accountId);

    void deleteByGroupId(long groupId);

    void deleteByAccountIdAndGroupId(long accountId, long groupId);

}
