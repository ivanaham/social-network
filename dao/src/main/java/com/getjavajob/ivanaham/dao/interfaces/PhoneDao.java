package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Phone;
import java.util.Set;

public interface PhoneDao extends Dao<Phone> {

    void insert(Set<Phone> phones);

    Set<Phone> findByAccountId(long accountId);

    int deleteByAccountId(long accountId);

}
