package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.GroupMessage;
import java.util.List;

public interface GroupMessageDao extends Dao<GroupMessage> {

    List<GroupMessage> findByRecipientId(long recipientId, int limit, int offset);

}
