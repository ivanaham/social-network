package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Group;
import java.util.List;

public interface GroupDao extends Dao<Group> {

    List<Group> findByName(String name, int limit, int offset);

}
