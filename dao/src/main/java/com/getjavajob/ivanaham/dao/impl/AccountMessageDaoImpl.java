package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.domain.Message.Type.ACCOUNT;
import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static java.util.Collections.emptyList;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.AccountMessageDao;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.AccountMessage_;
import com.getjavajob.ivanaham.domain.Account_;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.Friendship_;
import com.getjavajob.ivanaham.domain.Message;
import com.getjavajob.ivanaham.domain.Message_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class AccountMessageDaoImpl implements AccountMessageDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(AccountMessage message) {
        LOGGER.debug("insert() is called; message = {}", message);
        em.persist(message);
    }

    @Override
    public AccountMessage findById(long messageId) {
        LOGGER.debug("findById() is called; id = {}", messageId);
        return em.find(AccountMessage.class, messageId);
    }

    @Override
    public List<AccountMessage> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        cq.from(AccountMessage.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(AccountMessage message) {
        LOGGER.debug("update() is called; message = {}", message);
        em.merge(message);
    }

    @Override
    public AccountMessage deleteById(long messageId) {
        LOGGER.debug("deleteById() is called; id = {}", messageId);
        AccountMessage message = findById(messageId);
        if (nonNull(message)) {
            em.remove(message);
        }
        return message;
    }

    @Override
    public List<AccountMessage> findByRecipientId(long recipientId, Message.Type msgType, int limit, int offset) {
        LOGGER.debug("findByRecipientId() is called; id = {}, msgType = {}, limit = {}, offset = {}",
                recipientId, msgType, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);
        cq.where(cb.and(cb.equal(message.get(AccountMessage_.MESSAGE_TYPE), msgType),
                cb.equal(message.get(AccountMessage_.RECIPIENT).get(Account_.ID), recipientId)));
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    @Override
    public List<AccountMessage> findByAuthorId(long authorId, Message.Type msgType, int limit, int offset) {
        LOGGER.debug("findByAuthorId() is called; id = {}, msgType = {}, limit = {}, offset = {}",
                authorId, msgType, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);
        cq.where(cb.and(cb.equal(message.get(AccountMessage_.MESSAGE_TYPE), msgType),
                cb.equal(message.get(Message_.AUTHOR).get(Account_.ID), authorId)));
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    @Override
    public List<AccountMessage> findByParticipantIds(long accountIdA, long accountIdB, Message.Type msgType, int limit, int offset) {
        LOGGER.debug("findByParticipantIds() is called; accountIdA = {}, accountIdB = {}, "
                + "limit = {}, offset = {}", accountIdA, accountIdB, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);
        cq.where(cb.and(cb.equal(message.get(AccountMessage_.MESSAGE_TYPE), msgType),
                cb.or(cb.and(cb.equal(message.get(Message_.AUTHOR).get(Account_.ID), accountIdA),
                                cb.equal(message.get(AccountMessage_.RECIPIENT).get(Account_.ID), accountIdB)),
                        cb.and(cb.equal(message.get(Message_.AUTHOR).get(Account_.ID), accountIdB),
                                cb.equal(message.get(AccountMessage_.RECIPIENT).get(Account_.ID), accountIdA)))));
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    @Override
    public List<AccountMessage> findLastMessagesByAccountId(long accountId, int limit, int offset) {
        LOGGER.debug("findLastMessagesByAccountId() is called; id = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return findLastMessages(accountId, 0, limit, offset);
    }

    @Override
    public List<AccountMessage> findLastMessagesByAccountIdExcluding(
            long accountId, long accountIdToExclude, int limit, int offset) {
        LOGGER.debug("findLastMessagesByAccountIdExcluding() is called; id = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return findLastMessages(accountId, accountIdToExclude, limit, offset);
    }

    public List<AccountMessage> findFeedMessages(long accountId, int limit, int offset) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);

        Subquery<Integer> sqA = cq.subquery(Integer.class);
        Root<Friendship> friendshipA = sqA.from(Friendship.class);
        sqA.select(friendshipA.get(Friendship_.ACCOUNT_B).get(Account_.ID)).where(
                cb.and(
                        cb.equal(friendshipA.get(Friendship_.STATUS), ACCEPTED),
                        cb.equal(friendshipA.get(Friendship_.ACCOUNT_A).get(Account_.ID), accountId)));

        Subquery<Integer> sqB = cq.subquery(Integer.class);
        Root<Friendship> friendshipB = sqB.from(Friendship.class);
        sqB.select(friendshipB.get(Friendship_.ACCOUNT_A).get(Account_.ID)).where(
                cb.and(
                        cb.equal(friendshipB.get(Friendship_.STATUS), ACCEPTED),
                        cb.equal(friendshipB.get(Friendship_.ACCOUNT_B).get(Account_.ID), accountId)));

        cq.where(cb.and(cb.equal(message.get(AccountMessage_.MESSAGE_TYPE), ACCOUNT),
                cb.or(
                        cb.in(message.get(Message_.AUTHOR).get(Account_.ID)).value(sqA),
                        cb.in(message.get(Message_.AUTHOR).get(Account_.ID)).value(sqB))));
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    private List<AccountMessage> findLastMessages(long accountId, long accountIdToExclude, int limit, int offset) {
        LOGGER.debug("findLastMessages() is called; id = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AccountMessage> cq = cb.createQuery(AccountMessage.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);
        List<Long> lastDirectMessagesIds = getLastDirectMessagesIds(accountId);
        if (lastDirectMessagesIds.isEmpty()) {
            return emptyList();
        }
        CriteriaBuilder.In<Long> inClause = cb.in(message.get(AccountMessage_.ID));
        for (Long messageId : lastDirectMessagesIds) {
            inClause.value(messageId);
        }
        if (accountIdToExclude != 0) {
            cq.where(cb.and(inClause, cb.and(
                    cb.notEqual(message.get(AccountMessage_.RECIPIENT).get(Account_.ID), accountIdToExclude),
                    cb.notEqual(message.get(Message_.AUTHOR).get(Account_.ID), accountIdToExclude))
            ));
        } else {
            cq.where(inClause);
        }
        cq.orderBy(cb.desc(message.get(Message_.DATE_TIME_SENT)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    private List<Long> getLastDirectMessagesIds(long accountId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<AccountMessage> message = cq.from(AccountMessage.class);
        cq.select(cb.max(message.get(AccountMessage_.ID)));
        cq.where(cb.and(
                cb.equal(message.get(AccountMessage_.MESSAGE_TYPE), DIRECT),
                cb.or(cb.equal(message.get(Message_.AUTHOR).get(Account_.ID), accountId),
                        cb.equal(message.get(AccountMessage_.RECIPIENT).get(Account_.ID), accountId))));
        cq.groupBy(cb.function("LEAST", Long.class,
                        message.get(Message_.AUTHOR).get(Account_.ID), message.get(AccountMessage_.RECIPIENT).get(Account_.ID)),
                cb.function("GREATEST", Long.class,
                        message.get(Message_.AUTHOR).get(Account_.ID), message.get(AccountMessage_.RECIPIENT).get(Account_.ID)));
        return em.createQuery(cq).getResultList();
    }

}
