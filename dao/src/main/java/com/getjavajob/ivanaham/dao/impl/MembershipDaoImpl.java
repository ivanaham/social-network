package com.getjavajob.ivanaham.dao.impl;

import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import java.util.List;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

@Repository
public class MembershipDaoImpl implements MembershipDao {

    private final MembershipRepository membershipRepository;

    public MembershipDaoImpl(MembershipRepository membershipRepository) {
        this.membershipRepository = membershipRepository;
    }

    public void insert(Membership membership) {
        membershipRepository.save(membership);
    }

    public Membership findById(long id) {
        return membershipRepository.findById(id).orElse(null);
    }

    public List<Membership> findAll() {
        return (List<Membership>) membershipRepository.findAll();
    }

    public void update(Membership membership) {
        membershipRepository.save(membership);
    }

    public Membership deleteById(long id) {
        membershipRepository.deleteById(id);
        return new Membership();
    }

    @Override
    public List<Membership> findByAccountId(long accountId, RequestStatus status, int limit, int offset) {
        int page = offset == 0 ? 0 : offset / limit;
        return membershipRepository.findByAccountIdAndStatus(accountId, status, PageRequest.of(page, limit));
    }

    @Override
    public List<Membership> findByGroupId(long groupId, RequestStatus status, int limit, int offset) {
        int page = offset == 0 ? 0 : offset / limit;
        return membershipRepository.findByGroupIdAndStatus(groupId, status, PageRequest.of(page, limit));
    }

    @Override
    public List<Membership> findByGroupId(long groupId, RequestStatus status, Rights rights, int limit, int offset) {
        int page = offset == 0 ? 0 : offset / limit;
        return membershipRepository.findByGroupIdAndStatusAndRights(groupId, status, rights, PageRequest.of(page, limit));
    }

    @Override
    public Membership findMembership(long accountId, long groupId) {
        return membershipRepository.findByAccountIdAndGroupId(accountId, groupId).orElse(null);
    }

    @Override
    public void deleteByAccountId(long accountId) {
        membershipRepository.deleteByAccountId(accountId);
    }

    @Override
    public void deleteByGroupId(long groupId) {
        membershipRepository.deleteByGroupId(groupId);
    }

    @Override
    public void deleteMembership(long accountId, long groupId) {
        membershipRepository.deleteByAccountIdAndGroupId(accountId, groupId);
    }

}
