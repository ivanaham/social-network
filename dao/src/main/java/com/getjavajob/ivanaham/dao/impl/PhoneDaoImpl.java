package com.getjavajob.ivanaham.dao.impl;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toSet;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.PhoneDao;
import com.getjavajob.ivanaham.domain.Account_;
import com.getjavajob.ivanaham.domain.Phone;
import com.getjavajob.ivanaham.domain.Phone_;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class PhoneDaoImpl implements PhoneDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Set<Phone> phones) {
        LOGGER.debug("insert() is called; phones = {}", phones);
        phones.forEach(em::persist);
    }

    @Override
    public Set<Phone> findByAccountId(long accountId) {
        LOGGER.debug("findByAccountId() is called; account id = {}", accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Phone> cq = cb.createQuery(Phone.class);
        Root<Phone> phone = cq.from(Phone.class);
        cq.where(cb.equal(phone.get(Phone_.ACCOUNT).get(Account_.ID), accountId));
        return em.createQuery(cq).getResultStream().collect(toSet());
    }

    @Override
    public int deleteByAccountId(long accountId) {
        LOGGER.debug("deleteByAccountId() is called; account id = {}", accountId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<Phone> delete = cb.createCriteriaDelete(Phone.class);
        Root<Phone> phone = delete.from(Phone.class);
        delete.where(cb.equal(phone.get(Phone_.ACCOUNT).get(Account_.ID), accountId));
        return em.createQuery(delete).executeUpdate();
    }

    @Override
    public void insert(Phone phone) {
        LOGGER.debug("insert() is called; phone = {}", phone);
        em.persist(phone);
    }

    @Override
    public Phone findById(long id) {
        LOGGER.debug("findById() is called; id = {}", id);
        return em.find(Phone.class, id);
    }

    @Override
    public List<Phone> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Phone> cq = cb.createQuery(Phone.class);
        cq.from(Phone.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Phone phone) {
        LOGGER.debug("update() is called; phone = {}", phone);
        em.merge(phone);
    }

    @Override
    public Phone deleteById(long id) {
        LOGGER.debug("deleteById() is called; id = {}", id);
        Phone phone = findById(id);
        if (nonNull(phone)) {
            em.remove(phone);
        }
        return phone;
    }

}
