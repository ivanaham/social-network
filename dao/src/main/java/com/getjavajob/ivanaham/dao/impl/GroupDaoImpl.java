package com.getjavajob.ivanaham.dao.impl;

import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Group_;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class GroupDaoImpl implements GroupDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Group group) {
        LOGGER.debug("insert() is called; name = {}", group.getName());
        em.persist(group);
        LOGGER.debug("insert(): name = {}, id = {}", group.getName(), group.getId());
    }

    @Override
    public Group findById(long id) {
        LOGGER.debug("findById() is called; id = {}", id);
        return em.find(Group.class, id);
    }

    @Override
    public List<Group> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        cq.from(Group.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Group group) {
        LOGGER.debug("update() is called; id = {}", group.getId());
        em.merge(group);
    }

    @Override
    public Group deleteById(long id) {
        LOGGER.debug("deleteById() is called; id = {}", id);
        Group group = em.find(Group.class, id);
        if (nonNull(group)) {
            em.remove(group);
        }
        return group;
    }

    @Override
    public List<Group> findByName(String name, int limit, int offset) {
        LOGGER.debug("findByName() is called; name = {}, limit = {}, offset = {}", name, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        Root<Group> group = cq.from(Group.class);
        String likePattern = "%" + name.toLowerCase() + "%";
        cq.where(cb.like(cb.lower(group.get(Group_.NAME)), likePattern))
                .distinct(true);
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

}
