package com.getjavajob.ivanaham.dao.impl;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Account_;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.Friendship_;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.RequestType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class FriendshipDaoImpl implements FriendshipDao {

    private static final Logger LOGGER = getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Friendship friendship) {
        LOGGER.debug("insert() is called; friendship = {}", friendship);
        Account accountA = friendship.getAccountA();
        Account accountB = friendship.getAccountB();
        long accountAId = accountA.getId();
        long accountBId = accountB.getId();
        friendship.setAccountA(getSmallerId(accountAId, accountBId) == accountAId ? accountA : accountB);
        friendship.setAccountB(getBiggerId(accountAId, accountBId) == accountAId ? accountA : accountB);
        em.persist(friendship);
    }

    @Override
    public Friendship findById(long id) {
        LOGGER.debug("findById() is called; id = {}", id);
        return em.find(Friendship.class, id);
    }

    @Override
    public List<Friendship> findAll() {
        LOGGER.debug("findAll() is called");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friendship> cq = cb.createQuery(Friendship.class);
        cq.from(Friendship.class);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void update(Friendship friendship) {
        LOGGER.debug("update() is called; friendship = {}", friendship);
        em.merge(friendship);
    }

    @Override
    public Friendship deleteById(long id) {
        LOGGER.debug("deleteById() is called; id = {}", id);
        Friendship friendship = findById(id);
        if (nonNull(friendship)) {
            em.remove(friendship);
        }
        return friendship;
    }

    /**
     * Checks and retrieves Friendship data between two accounts in db
     *
     * @param accountIdA first Account id
     * @param accountIdB second Account id
     * @return Friendship if record exists, otherwise - null
     */
    @Override
    public Friendship findFriendship(long accountIdA, long accountIdB) {
        try {
            LOGGER.debug("findFriendship() is called; accountIdA = {}, accountIdB = {}", accountIdA, accountIdB);
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Friendship> cq = cb.createQuery(Friendship.class);
            Root<Friendship> friendship = cq.from(Friendship.class);
            cq.where(cb.and(
                    cb.equal(friendship.get(Friendship_.ACCOUNT_A).get(Account_.ID),
                            getSmallerId(accountIdA, accountIdB)),
                    cb.equal(friendship.get(Friendship_.ACCOUNT_B).get(Account_.ID),
                            getBiggerId(accountIdA, accountIdB))));
            return em.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Friendship> find(long accountId, RequestStatus status, int limit, int offset) {
        LOGGER.debug("find() is called; accountId = {}, status = {}, limit = {}, offset = {}",
                accountId, status, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friendship> cq = cb.createQuery(Friendship.class);
        Root<Friendship> friendship = cq.from(Friendship.class);
        cq.where(cb.and(
                cb.or(cb.equal(friendship.get(Friendship_.ACCOUNT_A).get(Account_.ID), accountId),
                        cb.equal(friendship.get(Friendship_.ACCOUNT_B).get(Account_.ID), accountId)),
                cb.equal(friendship.get(Friendship_.STATUS), status)));
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    @Override
    public List<Friendship> find(long accountId, RequestStatus status) {
        LOGGER.debug("find() is called; accountId = {}, status = {}", accountId, status);
        return find(accountId, status, MAX_VALUE, 0);
    }

    public List<Friendship> findPending(long accountId, RequestType type, int limit, int offset) {
        LOGGER.debug("findPending() is called; accountId = {}, type = {}, limit = {}, offset = {}",
                accountId, type, limit, offset);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Friendship> cq = cb.createQuery(Friendship.class);
        Root<Friendship> friendship = cq.from(Friendship.class);
        if (type == RequestType.INCOMING) {
            cq.where(cb.and(
                    cb.or(cb.equal(friendship.get(Friendship_.ACCOUNT_A).get(Account_.ID), accountId),
                            cb.equal(friendship.get(Friendship_.ACCOUNT_B).get(Account_.ID), accountId)),
                    cb.equal(friendship.get(Friendship_.STATUS), PENDING),
                    cb.notEqual(friendship.get(Friendship_.LAST_ACTED_ACCOUNT).get(Account_.ID), accountId)));
        } else {
            cq.where(cb.and(
                    cb.equal(friendship.get(Friendship_.LAST_ACTED_ACCOUNT).get(Account_.ID), accountId),
                    cb.equal(friendship.get(Friendship_.STATUS), PENDING)));
        }
        return em.createQuery(cq).setMaxResults(limit).setFirstResult(offset).getResultList();
    }

    @Override
    public boolean deleteFriendship(long accountIdA, long accountIdB) {
        LOGGER.debug("deleteFriendship() is called; accountIdA = {}, accountIdB = {}", accountIdA, accountIdB);
        Friendship friendship = findFriendship(accountIdA, accountIdB);
        if (nonNull(friendship)) {
            em.remove(friendship);
            return true;
        }
        return false;
    }

    private long getSmallerId(long accountIdA, long accountIdB) {
        return accountIdA < accountIdB ? accountIdA : accountIdB;
    }

    private long getBiggerId(long accountIdA, long accountIdB) {
        return accountIdA > accountIdB ? accountIdA : accountIdB;
    }

}
