package com.getjavajob.ivanaham.dao.interfaces;

import com.getjavajob.ivanaham.domain.Account;
import java.util.List;

public interface AccountDao extends Dao<Account> {

    Account findByEmail(String email);

    List<Account> findCurrentBirthdays();

    List<Account> findByName(String name, int limit, int offset);

    void resetPasswordById(long accountId, String password);

}