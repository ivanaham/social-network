package com.getjavajob.ivanaham.web.config;

import static java.lang.Integer.MAX_VALUE;
import static org.springframework.http.HttpMethod.POST;

import com.getjavajob.ivanaham.service.security.CurrentUser;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

    private final CharacterEncodingFilter characterEncodingFilter;
    @Value("${pagination.messages}")
    private String recordsPerPage;

    public WebSecurityConfig(CharacterEncodingFilter encodingFilter) {
        characterEncodingFilter = encodingFilter;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .addFilterBefore(characterEncodingFilter, CsrfFilter.class)
                .authorizeHttpRequests(auth -> {
                    auth.antMatchers("/", "/registration", "/resources/**").permitAll();
                    auth.antMatchers(POST, "/account").permitAll();
                    auth.antMatchers("/messaging/**").permitAll();
                    auth.anyRequest().authenticated();
                })
                .formLogin(form -> form
                        .loginPage("/login")
                        .usernameParameter("email")
                        .failureUrl("/login?message=error")
                        .successHandler((request, response, authentication) -> {
                            addRecordsCookie(response);
                            CurrentUser currentUser = (CurrentUser) authentication.getPrincipal();
                            response.sendRedirect(request.getContextPath() + "/account?account-id=" + currentUser.getId());
                        })
                        .permitAll()
                )
                .rememberMe().and()
                .sessionManagement(session -> session.invalidSessionUrl("/login"))
                .logout(logout -> logout.deleteCookies("JSESSIONID"))
                .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private void addRecordsCookie(HttpServletResponse response) {
        Cookie records = new Cookie("recordsPerPage", recordsPerPage);
        records.setMaxAge(MAX_VALUE);
        records.setPath("/");
        response.addCookie(records);
    }

}
