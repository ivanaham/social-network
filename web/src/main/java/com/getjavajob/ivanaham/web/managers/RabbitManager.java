package com.getjavajob.ivanaham.web.managers;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.service.FriendshipService;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitManager {

    private static final Logger LOGGER = getLogger();

    private final RabbitTemplate rabbitTemplate;
    private final FriendshipService friendshipService;

    @Value("${routing.key.friend-request}")
    private String friendRequestRoutingKey;

    @Value("${routing.key.account-message}")
    private String accountWallRoutingKey;

    @Value("${exchange.emails.name}")
    private String emailExchange;

    public RabbitManager(
            RabbitTemplate rabbitTemplate, FriendshipService friendshipService) {
        this.rabbitTemplate = rabbitTemplate;
        this.friendshipService = friendshipService;
    }

    public void sendFriendRequest(long actionMakerId, long friendId) {
        LOGGER.debug("sendFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        rabbitTemplate.convertAndSend(
                emailExchange, friendRequestRoutingKey, friendshipService.findFriendship(actionMakerId, friendId));
    }

    public void sendAccountMessage(AccountMessage message, long currentUserId) {
        LOGGER.debug("sendAccountMessage() is called: message = {}, currentUserId = {}", message, currentUserId);
        if (message.getRecipient().getId() != currentUserId) {
            rabbitTemplate.convertAndSend(emailExchange, accountWallRoutingKey, message);
        }
    }


}
