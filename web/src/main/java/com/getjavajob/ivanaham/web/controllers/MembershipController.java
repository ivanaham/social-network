package com.getjavajob.ivanaham.web.controllers;

import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.service.GroupService;
import com.getjavajob.ivanaham.service.MembershipService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import com.getjavajob.ivanaham.web.dto.GroupDto;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MembershipController {

    private static final Logger LOGGER = getLogger();
    private static final String REDIRECT_PREFIX = "redirect:";

    private final GroupService groupService;
    private final MembershipService membershipService;

    @Autowired
    public MembershipController(GroupService groupService, MembershipService membershipService) {
        this.groupService = groupService;
        this.membershipService = membershipService;
    }

    @GetMapping(path = "/group/requests")
    public String getGroupRequestsPage(Model model, @RequestParam("group-id") long groupId) {
        LOGGER.debug("getGroupRequestsPage() is called: group id = {}", groupId);
        model.addAttribute("groupId", groupId);
        return "group/group-requests";
    }

    @GetMapping(path = {"/account/groups"}, params = {"status=pending", "type=outgoing"})
    public String getAccountRequestsPage(@RequestParam("account-id") long accountId) {
        LOGGER.debug("getAccountRequestsPage() is called: account id = {}", accountId);
        return "account/account-group-requests";
    }

    @GetMapping(path = {"/account/groups"}, params = {"status=accepted"})
    public String getAccountGroupsPage(@RequestParam("account-id") long accountId) {
        LOGGER.debug("getAccountGroupsPage() is called: account id = {}", accountId);
        return "account/account-groups";
    }

    @GetMapping(path = {"/group/members"}, params = {"status=accepted"})
    public String getGroupMembersPage(
            Model model,
            @RequestParam("group-id") long groupId,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("getGroupMembersPage() is called: group id = {}", groupId);
        model.addAttribute(GroupDto.toDto(groupService.findById(groupId)));
        model.addAttribute("groupId", groupId);
        model.addAttribute("membership",
                membershipService.findMembership(currentUser.getId(), groupId)
                        .orElse(membershipService.emptyMembership()));
        return "group/group-members";
    }

    @PostMapping(path = {"/group/members"}, params = {"action=rights"})
    public String promoteToModerator(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("promoteToModerator() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.updateRights(accountId, groupId, MODERATOR);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(path = {"/group/members"}, params = {"action=add"})
    public String sendMemberRequest(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("sendMemberRequest() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.sendMembershipRequest(accountId, groupId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(path = {"/group/members"}, params = {"action=accept"})
    public String acceptMemberRequest(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("acceptMemberRequest() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.acceptMembershipRequest(accountId, groupId);
        membershipService.updateRights(accountId, groupId, COMMON);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(path = {"/group/members"}, params = {"action=cancel"})
    public String cancelMemberRequest(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("cancelMemberRequest() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.declineMembershipRequest(accountId, groupId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(path = {"/group/members"}, params = {"action=decline"})
    public String declineMemberRequest(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("declineMemberRequest() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.declineMembershipRequest(accountId, groupId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(path = {"/group/members"}, params = {"action=delete"})
    public String deleteMemberRequest(
            @RequestParam("account-id") long accountId,
            @RequestParam("group-id") long groupId,
            @RequestHeader("referer") String referer) {
        LOGGER.debug("deleteMemberRequest() is called: account id = {}, group id = {}", accountId, groupId);
        membershipService.updateRights(accountId, groupId, null);
        membershipService.deleteMember(accountId, groupId);
        return REDIRECT_PREFIX + referer;
    }

}