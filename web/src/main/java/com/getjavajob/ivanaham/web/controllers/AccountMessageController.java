package com.getjavajob.ivanaham.web.controllers;

import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.service.AccountMessageService;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.FriendshipService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import com.getjavajob.ivanaham.web.managers.RabbitManager;
import com.getjavajob.ivanaham.web.managers.RedisFeedManager;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller
public class AccountMessageController {

    private static final Logger LOGGER = getLogger();

    private final AccountMessageService accountMessageService;
    private final FriendshipService friendshipService;
    private final AccountService accountService;
    private final RabbitManager rabbitManager;
    private final RedisFeedManager redisFeedManager;

    @Value("${pagination.messages.direct}")
    private int recordsPerPageInDirect;

    public AccountMessageController(
            AccountMessageService accountMessageService, FriendshipService friendshipService,
            AccountService accountService, RabbitManager rabbitManager, RedisFeedManager redisFeedManager) {
        this.accountMessageService = accountMessageService;
        this.friendshipService = friendshipService;
        this.accountService = accountService;
        this.rabbitManager = rabbitManager;
        this.redisFeedManager = redisFeedManager;

    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(LocalDateTime.class, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/account/messages")
    public String getAccountMessagesPage(
            Model model,
            @RequestParam(value = "account-to-id", defaultValue = "0") long buddyId) {
        LOGGER.debug("getAccountMessagesPage() is called; account-to-id = {}", buddyId);
        model.addAttribute("recordsPerPageInDirect", recordsPerPageInDirect);
        model.addAttribute(new AccountMessage());
        return "account/account-message";
    }

    @GetMapping("/feed")
    public String getFeedPage(@AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("getFeedPage() is called; current user id = {}", currentUser.getId());
        return "feed";
    }

    @PostMapping(value = "/account/messages", params = "type=account")
    public String createAccountTypeMessage(
            @RequestHeader("referer") String referer,
            @ModelAttribute AccountMessage message,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("createMessage() is called; message = {}, sender id = {}", message, currentUser.getId());
        createMessage(message, currentUser.getId());
        rabbitManager.sendAccountMessage(message, currentUser.getId());
        redisFeedManager.cacheMessageForEachIfKeyExist(getFriends(currentUser.getId()), message);
        return "redirect:" + referer;
    }

    @PostMapping(value = "/account/messages", params = "type=direct")
    public String createDirectTypeMessage(
            @RequestHeader("referer") String referer,
            @ModelAttribute AccountMessage message,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("createMessage() is called; message = {}, sender id = {}", message, currentUser.getId());
        createMessage(message, currentUser.getId());
        return "redirect:" + referer;
    }

    private void createMessage(AccountMessage message, long currentUserId) {
        Account recipient = accountService.findById(message.getRecipient().getId());
        message.setRecipient(recipient);
        Account author = accountService.findById(currentUserId);
        message.setAuthor(author);
        message.setDateTimeSent(now());
        accountMessageService.create(message);
    }

    private List<Account> getFriends(long accountId) {
        List<Friendship> friendships = friendshipService.find(accountId, ACCEPTED);
        return friendships.stream().map(friendship -> {
            long friendId = friendship.getAccountA().getId() == accountId
                    ? friendship.getAccountB().getId()
                    : friendship.getAccountA().getId();
            return accountService.findById(friendId);
        }).collect(toList());
    }

    @PreAuthorize("authentication.principal.id == #message.author.id or authentication.principal.id == #message.recipient.id")
    @MessageMapping("/accountIdA/{accountIdA}/accountIdB/{accountIdB}")
    @SendTo("/queue/accountIdA/{accountIdA}/accountIdB/{accountIdB}")
    public AccountMessage getMessages(AccountMessage message) {
        LOGGER.debug("getMessages() is called; message = {}", message);
        message.setDateTimeSent(now());
        message.setMessageType(DIRECT);
        accountMessageService.create(message);
        return message;
    }

}
