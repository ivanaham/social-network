package com.getjavajob.ivanaham.web.managers;

import static java.time.Instant.now;
import static java.time.ZoneId.systemDefault;
import static java.util.Arrays.copyOfRange;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.service.AccountMessageService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

@Component
public class RedisFeedManager {

    private static final Logger LOGGER = getLogger();

    private final RedisTemplate<String, AccountMessage> redisTemplate;
    private final AccountMessageService accountMessageService;

    @Value("${redis.feed.prefix}")
    private String feedPrefix;

    @Value("${redis.feed.max.size}")
    private int feedMaxSize;

    @SuppressWarnings("rawtypes")
    public RedisFeedManager(RedisTemplate redisTemplate, AccountMessageService accountMessageService) {
        this.redisTemplate = redisTemplate;
        this.accountMessageService = accountMessageService;
    }

    public Set<AccountMessage> findAllByAccountId(long accountId) {
        LOGGER.debug("findAllByAccountId() is called: accountId = {}", accountId);
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        return zSet.reverseRange(feedPrefix + accountId, 0, now().atZone(systemDefault()).toEpochSecond());
    }

    public void cacheMessages(List<AccountMessage> dbMessages, long accountId) {
        LOGGER.debug("cacheMessages() is called: dbMessages = {}, accountId = {}", dbMessages, accountId);
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        for (AccountMessage message : dbMessages) {
            zSet.addIfAbsent(feedPrefix + accountId, message, calcScore(message.getDateTimeSent()));
        }
        trimByMaxSize(accountId);
    }

    public void cacheMessagesIfKeyExist(List<AccountMessage> dbMessages, long accountId) {
        LOGGER.debug("cacheMessagesIfKeyExist() is called: dbMessages = {}, accountId = {}",
                dbMessages, accountId);
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        String key = feedPrefix + accountId;
        Long keyRecordsCount = zSet.size(key);
        if (nonNull(keyRecordsCount) && keyRecordsCount > 0) {
            for (AccountMessage message : dbMessages) {
                zSet.addIfAbsent(feedPrefix + accountId, message, calcScore(message.getDateTimeSent()));
            }
            trimByMaxSize(accountId);
        }
    }

    public void cacheMessagesOfNewFriendshipIfKeyExist(long accountIdA, long accountIdB) {
        LOGGER.debug("cacheMessagesOfNewFriendshipIfKeyExist() is called: accountIdA = {}, accountIdB = {}",
                accountIdA, accountIdB);
        List<AccountMessage> accountAMessages = accountMessageService.findByAuthorId(accountIdA, feedMaxSize, 0);
        cacheMessagesIfKeyExist(accountAMessages, accountIdB);
        List<AccountMessage> accountBMessages = accountMessageService.findByAuthorId(accountIdB, feedMaxSize, 0);
        cacheMessagesIfKeyExist(accountBMessages, accountIdA);
    }

    public void cleanMessagesOfDeletedFriendship(long accountIdA, long accountIdB) {
        LOGGER.debug("cacheMessagesOfNewFriendshipIfKeyExist() is called: accountIdA = {}, accountIdB = {}",
                accountIdA, accountIdB);
        cleanFeed(accountIdA, accountIdB);
        cleanFeed(accountIdB, accountIdA);
    }

    private void cleanFeed(long feedOwnerId, long accountIdToBeDeleted) {
        LOGGER.debug("cacheMessagesOfNewFriendshipIfKeyExist() is called: "
                + "feedOwnerId = {}, accountIdToBeDeleted = {}", feedOwnerId, accountIdToBeDeleted);
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        Set<AccountMessage> accountAMessages = zSet.range(feedPrefix + feedOwnerId, 0, -1);
        if (nonNull(accountAMessages)) {
            List<AccountMessage> result = new ArrayList<>();
            for (AccountMessage message : accountAMessages) {
                if (message.getAuthor().getId() == accountIdToBeDeleted) {
                    result.add(message);
                }
            }
            zSet.remove(feedPrefix + feedOwnerId, copyOfRange(result.toArray(), 0, feedMaxSize + 1));
        }
    }

    public void cacheMessageForEachIfKeyExist(List<Account> friends, AccountMessage message) {
        LOGGER.debug("cacheMessageForEachIfKeyExist() is called: friends = {}, message = {}",
                friends, message);
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        for (Account account : friends) {
            long accountId = account.getId();
            String key = feedPrefix + accountId;
            Long keyRecordsCount = zSet.size(key);
            if (nonNull(keyRecordsCount) && keyRecordsCount > 0) {
                zSet.add(key, message, calcScore(message.getDateTimeSent()));
                trimByMaxSize(accountId);
            }
        }
    }

    private void trimByMaxSize(long accountId) {
        ZSetOperations<String, AccountMessage> zSet = redisTemplate.opsForZSet();
        Long currentSize = zSet.size(feedPrefix + accountId);
        if (nonNull(currentSize) && currentSize > feedMaxSize) {
            zSet.popMin(feedPrefix + accountId, currentSize - feedMaxSize);
        }
    }

    private double calcScore(LocalDateTime dateTime) {
        return dateTime.atZone(systemDefault()).toEpochSecond();
    }

}
