package com.getjavajob.ivanaham.web.controllers;

import static com.getjavajob.ivanaham.web.controllers.ImageController.isEmpty;
import static com.getjavajob.ivanaham.web.dto.AccountDto.toDto;
import static java.time.LocalDate.now;
import static java.util.Objects.isNull;
import static java.util.ResourceBundle.Control.FORMAT_DEFAULT;
import static java.util.ResourceBundle.Control.getNoFallbackControl;
import static java.util.ResourceBundle.getBundle;
import static java.util.stream.Collectors.toList;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.domain.Role;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.FriendshipService;
import com.getjavajob.ivanaham.service.PhoneService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import com.getjavajob.ivanaham.web.dto.AccountDto;
import com.getjavajob.ivanaham.web.dto.PhoneDto;
import com.getjavajob.ivanaham.web.exceptions.NotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@SessionAttributes("updatableAccountDto")
public class AccountController {

    private static final String ACCOUNT_ID_PARAM = "account-id";
    private static final Logger LOGGER = getLogger();
    private static final String[] UNIQUE_CONSTRAINTS = {"email", "icq", "skype", "phone"};

    private final AccountService accountService;
    private final PhoneService phoneService;
    private final FriendshipService friendshipService;
    private final XmlMapper mapper;
    private final PasswordEncoder passwordEncoder;

    public AccountController(AccountService accountService, FriendshipService friendshipService,
                             PhoneService phoneService, XmlMapper mapper, PasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.friendshipService = friendshipService;
        this.phoneService = phoneService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping(path = "/account")
    public String getAccountPage(
            Model model,
            @RequestParam("account-id") long accountId,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("getAccountPage() is called: account id = {}", accountId);
        model.addAttribute("friendship", friendshipService.findFriendship(currentUser.getId(), accountId));
        Account account = accountService.findById(accountId);
        if (isNull(account)) {
            LOGGER.debug("getAccountPage(): NotFoundException - account id = {}", accountId);
            throw new NotFoundException();
        }
        model.addAttribute(toDto(account));
        model.addAttribute(new AccountMessage());
        return "account/account";
    }

    @GetMapping(value = "/account/edit")
    public String getAccountEditPage(@RequestParam("account-id") long accountId, Model model) {
        LOGGER.debug("getAccountEditPage() is called: account id = {}", accountId);
        model.addAttribute("updatableAccountDto", toDto(accountService.findById(accountId)));
        return "account/account-edit";
    }

    @PostMapping(path = "/account")
    public String createAccount(@ModelAttribute AccountDto accountDto, RedirectAttributes attributes) {
        LOGGER.debug("createAccount() is called: account email = {}", accountDto.getEmail());
        Account account = accountDto.toDomainEntity();
        account.setRegisterDate(Date.valueOf(now()));
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        accountService.create(account);
        attributes.addFlashAttribute("isRegistered", "true");
        return "redirect:login";
    }

    @PatchMapping(path = "/account")
    public String changePassword(
            @RequestParam("password") String password,
            @AuthenticationPrincipal CurrentUser currentUser,
            RedirectAttributes attributes) {
        LOGGER.debug("changePassword() is called: account id = {}", currentUser.getId());
        accountService.resetPasswordById(currentUser.getId(), passwordEncoder.encode(password));
        attributes.addAttribute(ACCOUNT_ID_PARAM, currentUser.getId());
        attributes.addFlashAttribute("passwordMessage", "success");
        return "redirect:account/edit";
    }

    @PutMapping(path = "/account")
    public String editAccount(
            @ModelAttribute("updatableAccountDto") AccountDto accountDto,
            @AuthenticationPrincipal CurrentUser currentUser,
            RedirectAttributes attributes, SessionStatus status) {
        LOGGER.debug("editAccount() is called: account id = {}", accountDto.getId());
        for (PhoneDto phoneDto : accountDto.getPhones()) {
            if (phoneDto.isDeleted()) {
                phoneService.deleteById(phoneDto.getId());
            }
        }
        updateAccount(accountDto.toDomainEntity(), currentUser, attributes);
        status.setComplete();
        return "redirect:account/edit";
    }

    private void updateAccount(Account account, CurrentUser currentUser, RedirectAttributes attributes) {
        verifyImage(account);
        accountService.edit(account);
        if (currentUser.getId() == account.getId() && !currentUser.getRoles().equals(account.getRoles())) {
            reloadRoles(currentUser, account);
        }
        attributes.addAttribute(ACCOUNT_ID_PARAM, account.getId());
        attributes.addFlashAttribute("message", "success");
    }

    @ResponseBody
    @RequestMapping(path = "/account/export", produces = APPLICATION_XML_VALUE)
    public Account getAccountFile(@RequestParam("account-id") long accountId) {
        LOGGER.debug("getAccountFile() is called: account id = {}", accountId);
        return accountService.findById(accountId);
    }

    @RequestMapping(path = "/account/import")
    public String editAccountFromXml(
            SessionStatus status,
            @RequestParam("account") MultipartFile file,
            @AuthenticationPrincipal CurrentUser currentUser,
            RedirectAttributes attributes) throws IOException {
        LOGGER.debug("editAccountFromXml() is called: file = {}", file);
        Account loadedAccount = mapper.readValue(file.getBytes(), Account.class);
        LOGGER.debug("editAccountFromXml(): parsed account id = {}", loadedAccount.getId());
        Account dbAccount = accountService.findById(loadedAccount.getId());
        dbAccount.getPhones().forEach(phone -> phoneService.deleteById(phone.getId()));
        loadedAccount.setPassword(dbAccount.getPassword());
        updateAccount(loadedAccount, currentUser, attributes);
        status.setComplete();
        return "redirect:edit";
    }

    private void verifyImage(Account account) {
        if (isEmpty(account.getProfileImage())) {
            Image profileImage = accountService.findById(account.getId()).getProfileImage();
            account.setProfileImage(profileImage);
        }
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public String getDuplicateViolationMessage(
            DataIntegrityViolationException e, Locale lang, HttpServletRequest request, RedirectAttributes attributes) {
        LOGGER.atDebug().withThrowable(e).log("getDuplicateViolationMessage() is called");
        String invalidConstraint = parseInvalidConstraint(e);
        ResourceBundle bundle = getBundle("messages", lang, getNoFallbackControl(FORMAT_DEFAULT));
        attributes.addFlashAttribute("message",
                bundle.getString("label.registration.error.unique." + invalidConstraint));
        return "redirect:" + request.getHeader("referer");
    }

    private String parseInvalidConstraint(DataIntegrityViolationException e) {
        String errorMsg = e.getMostSpecificCause().getMessage();
        for (String constraint : UNIQUE_CONSTRAINTS) {
            if (errorMsg.contains(constraint)) {
                return constraint;
            }
        }
        return null;
    }

    public void reloadRoles(CurrentUser currentUser, Account account) {
        Set<Role> roles = account.getRoles();
        List<GrantedAuthority> actualAuthorities
                = roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(toList());
        Authentication newAuth
                = new UsernamePasswordAuthenticationToken(currentUser, account.getPassword(), actualAuthorities);
        getContext().setAuthentication(newAuth);
    }

}