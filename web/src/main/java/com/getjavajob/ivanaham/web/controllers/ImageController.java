package com.getjavajob.ivanaham.web.controllers;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.springframework.http.MediaType.IMAGE_JPEG;
import static org.springframework.http.MediaType.IMAGE_PNG;
import static org.springframework.http.ResponseEntity.ok;

import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.service.AccountMessageService;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.GroupMessageService;
import com.getjavajob.ivanaham.service.GroupService;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller()
@RequestMapping("/images/get")
public class ImageController {

    private static final Logger LOGGER = getLogger();

    private final AccountService accountService;
    private final GroupService groupService;
    private final AccountMessageService accountMessageService;
    private final GroupMessageService groupMessageService;

    public ImageController(
            AccountService accountService, GroupService groupService,
            AccountMessageService accountMessageService, GroupMessageService groupMessageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.accountMessageService = accountMessageService;
        this.groupMessageService = groupMessageService;
    }

    public static boolean isEmpty(Image image) {
        if (nonNull(image)) {
            byte[] imageBytes = image.getImageBytes();
            String name = image.getName();
            return isNull(name) || name.isEmpty() && isNull(imageBytes) || imageBytes.length == 0;
        }
        return true;
    }

    @GetMapping(params = {"img-type=account"})
    @ResponseBody
    public ResponseEntity<byte[]> getAccountImage(@RequestParam("account-id") long accountId) {
        LOGGER.debug("getAccountImage() is called; account id = {}", accountId);
        Image image = accountService.findById(accountId).getProfileImage();
        return ok()
                .contentType(getContentType(image.getName()))
                .body((image.getImageBytes()));
    }

    @GetMapping(params = {"img-type=group"})
    @ResponseBody
    public ResponseEntity<byte[]> getGroupImage(@RequestParam("group-id") long groupId) {
        LOGGER.debug("getGroupImage() is called; group id = {}", groupId);
        Image image = groupService.findById(groupId).getProfileImage();
        return ok()
                .contentType(getContentType(image.getName()))
                .body((image.getImageBytes()));
    }

    @GetMapping(params = {"img-type=account-message"})
    @ResponseBody
    public ResponseEntity<byte[]> getAccountMessageImage(@RequestParam("message-id") long messageId) {
        LOGGER.debug("getMessageImage() is called; message id = {}", messageId);
        Image image = accountMessageService.findById(messageId).getAttachedImage();
        return ok()
                .contentType(getContentType(image.getName()))
                .body((image.getImageBytes()));
    }

    @GetMapping(params = {"img-type=group-message"})
    @ResponseBody
    public ResponseEntity<byte[]> getGroupMessageImage(@RequestParam("message-id") long messageId) {
        LOGGER.debug("getMessageImage() is called; message id = {}", messageId);
        Image image = groupMessageService.findById(messageId).getAttachedImage();
        return ok()
                .contentType(getContentType(image.getName()))
                .body((image.getImageBytes()));
    }

    private MediaType getContentType(String imgName) {
        LOGGER.debug("getContentType() is called; image name = {}", imgName);
        String imageFormat = imgName.substring(imgName.indexOf('.') + 1);
        return "jpeg".equals(imageFormat) ? IMAGE_JPEG : IMAGE_PNG;
    }

}
