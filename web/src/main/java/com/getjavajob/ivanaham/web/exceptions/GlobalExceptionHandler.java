package com.getjavajob.ivanaham.web.exceptions;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.service.security.CurrentUser;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = getLogger();

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception, @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.atError()
                .withThrowable(exception)
                .log("handleException() is called for account id {}", currentUser.getId());
        return new ModelAndView("error-pages/general-error");
    }

    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleException(NotFoundException exception, @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.atDebug()
                .withThrowable(exception)
                .log("handleException() is called for account id {}", currentUser.getId());
        return new ModelAndView("error-pages/not-found");
    }

}
