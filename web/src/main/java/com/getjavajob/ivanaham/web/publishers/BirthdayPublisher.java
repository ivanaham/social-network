package com.getjavajob.ivanaham.web.publishers;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.service.AccountService;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BirthdayPublisher {

    private static final Logger LOGGER = getLogger();

    private final AccountService accountService;
    private final RabbitTemplate rabbitTemplate;

    @Value("${exchange.emails.name}")
    private String emailExchange;

    @Value("${routing.key.birthday}")
    private String birthdayRoutingKey;

    public BirthdayPublisher(AccountService accountService, RabbitTemplate rabbitTemplate) {
        this.accountService = accountService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(cron = "${cron.birthday.publisher}")
    public void publishBirthdays() {
        LOGGER.debug("publishBirthdays() is called");
        List<Account> accounts = accountService.findCurrentBirthdays();
        for (Account account : accounts) {
            rabbitTemplate.convertAndSend(emailExchange, birthdayRoutingKey, account);
        }
    }

}