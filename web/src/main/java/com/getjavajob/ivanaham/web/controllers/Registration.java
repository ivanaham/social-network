package com.getjavajob.ivanaham.web.controllers;

import com.getjavajob.ivanaham.domain.Phone;
import com.getjavajob.ivanaham.web.dto.AccountDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Registration {

    @RequestMapping("/registration")
    public String getRegistrationPage(Model model) {
        model.addAttribute(new AccountDto());
        model.addAttribute(new Phone());
        return "registration";
    }

}
