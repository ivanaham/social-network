package com.getjavajob.ivanaham.web.controllers;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.service.FriendshipService;
import com.getjavajob.ivanaham.web.managers.RabbitManager;
import com.getjavajob.ivanaham.web.managers.RedisFeedManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/account/friends")
public class FriendshipController {

    private static final Logger LOGGER = getLogger();
    private static final String REDIRECT_PREFIX = "redirect:";

    private final FriendshipService friendshipService;
    private final RabbitManager rabbitManager;
    private final RedisFeedManager feedManager;

    public FriendshipController(
            FriendshipService friendshipService, RabbitManager rabbitManager, RedisFeedManager feedManager) {
        this.friendshipService = friendshipService;
        this.rabbitManager = rabbitManager;
        this.feedManager = feedManager;
    }

    static long getFriendId(long currentAcctId, Friendship friendship) {
        return currentAcctId == friendship.getAccountA().getId()
                ? friendship.getAccountB().getId()
                : friendship.getAccountA().getId();
    }

    @GetMapping(params = {"status=accepted"})
    public String getFriendsPage(@RequestParam("id") long accountId) {
        LOGGER.debug("getFriendsPage() is called; account id = {}", accountId);
        return "account/account-friends";
    }

    @GetMapping(params = "status=pending")
    public String getFriendRequestsPage(
            @RequestParam("id") long accountId) {
        LOGGER.debug("getFriendRequestsPage() is called; account id = {}", accountId);
        return "account/account-friend-requests";
    }

    @PostMapping(params = {"action=send"})
    public String sendFriendRequest(
            @RequestHeader("referer") String referer,
            @RequestParam("action-maker-id") long actionMakerId,
            @RequestParam("friend-id") long friendId) {
        LOGGER.debug("sendFriendRequest() is called: senderId = {}, recipientId = {}", actionMakerId, friendId);
        friendshipService.sendFriendRequest(actionMakerId, friendId);
        rabbitManager.sendFriendRequest(actionMakerId, friendId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(params = {"action=accept"})
    public String acceptFriendRequest(
            @RequestHeader("referer") String referer,
            @RequestParam("action-maker-id") long actionMakerId,
            @RequestParam("friend-id") long friendId) {
        LOGGER.debug("acceptFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        friendshipService.acceptFriendRequest(actionMakerId, friendId);
        feedManager.cacheMessagesOfNewFriendshipIfKeyExist(actionMakerId, friendId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(params = {"action=decline"})
    public String declineFriendRequest(
            @RequestHeader("referer") String referer,
            @RequestParam("action-maker-id") long actionMakerId,
            @RequestParam("friend-id") long friendId) {
        LOGGER.debug("declineFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        friendshipService.declineFriendRequest(actionMakerId, friendId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(params = {"action=cancel"})
    public String cancelFriendRequest(
            @RequestHeader("referer") String referer,
            @RequestParam("action-maker-id") long actionMakerId,
            @RequestParam("friend-id") long friendId) {
        LOGGER.debug("cancelFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        friendshipService.declineFriendRequest(actionMakerId, friendId);
        return REDIRECT_PREFIX + referer;
    }

    @PostMapping(params = {"action=delete"})
    public String deleteFriendRequest(
            @RequestHeader("referer") String referer,
            @RequestParam("action-maker-id") long actionMakerId,
            @RequestParam("friend-id") long friendId) {
        LOGGER.debug("deleteFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        friendshipService.deleteFriend(actionMakerId, friendId);
        feedManager.cleanMessagesOfDeletedFriendship(actionMakerId, friendId);
        return REDIRECT_PREFIX + referer;
    }

}
