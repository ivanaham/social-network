package com.getjavajob.ivanaham.web.dto;

import static java.util.Objects.isNull;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Phone;

public class PhoneDto {

    private Phone.Type type;
    private String number;
    private Account account;
    private long id;
    private boolean isDeleted;

    public static PhoneDto toDto(Phone phone) {
        PhoneDto phoneDto = new PhoneDto();
        phoneDto.setId(phone.getId());
        phoneDto.setType(phone.getType());
        phoneDto.setNumber(phone.getNumber());
        phoneDto.setAccount(phone.getAccount());
        return phoneDto;
    }

    public Phone toDomainEntity() {
        Phone phone = new Phone();
        phone.setId(this.getId());
        phone.setType(this.getType());
        phone.setNumber(this.getNumber());
        phone.setAccount(this.getAccount());
        return phone;
    }

    public Phone.Type getType() {
        return type;
    }

    public void setType(Phone.Type type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public String toString() {
        long accountId = isNull(account) ? 0 : account.getId();
        return "PhoneDto{" +
                "type=" + type +
                ", number='" + number + '\'' +
                ", accountId=" + accountId +
                ", id=" + id +
                ", isDeleted=" + isDeleted +
                '}';
    }

}
