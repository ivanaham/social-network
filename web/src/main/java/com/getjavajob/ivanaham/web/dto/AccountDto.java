package com.getjavajob.ivanaham.web.dto;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.domain.Phone;
import com.getjavajob.ivanaham.domain.Role;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AccountDto {

    private long id;
    private List<PhoneDto> phones = new ArrayList<>();
    private List<RoleDto> roles = new ArrayList<>();
    private Date registerDate;
    private Date birthdate;
    private Image profileImage;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String password;
    private String homeAddress;
    private String workAddress;
    private String skype;
    private String additional;
    private Integer icq;

    public static AccountDto toDto(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(account.getId());
        accountDto.setFirstName(account.getFirstName());
        accountDto.setMiddleName(account.getMiddleName());
        accountDto.setLastName(account.getLastName());
        accountDto.setRegisterDate(account.getRegisterDate());
        accountDto.setPhones(account.getPhones().stream().map(PhoneDto::toDto).collect(toList()));
        accountDto.setEmail(account.getEmail());
        accountDto.setPassword(account.getPassword());
        accountDto.setHomeAddress(account.getHomeAddress());
        accountDto.setWorkAddress(account.getWorkAddress());
        accountDto.setBirthdate(account.getBirthdate());
        accountDto.setProfileImage(account.getProfileImage());
        accountDto.setIcq(account.getIcq());
        accountDto.setAdditional(account.getAdditional());
        accountDto.setSkype(account.getSkype());
        accountDto.setRoles(account.getRoles().stream().map(RoleDto::toDto).collect(toList()));
        return accountDto;
    }

    public Account toDomainEntity() {
        Account account = new Account();
        account.setId(this.getId());
        account.setFirstName(this.getFirstName());
        account.setMiddleName(this.getMiddleName());
        account.setLastName(this.getLastName());
        if (nonNull(this.getRegisterDate())) {
            account.setRegisterDate(this.getRegisterDate());
        }
        Set<Phone> accountPhones = new HashSet<>();
        for (PhoneDto phoneDto : this.getPhones()) {
            if (!phoneDto.isDeleted()) {
                accountPhones.add(phoneDto.toDomainEntity());
            }
        }
        account.setPhones(accountPhones);
        Set<Role> accountRoles = new HashSet<>();
        for (RoleDto roleDto : this.getRoles()) {
            if (!roleDto.isDeleted()) {
                accountRoles.add(roleDto.toDomainEntity());
            }
        }
        account.setRoles(accountRoles);
        account.setEmail(this.getEmail());
        account.setPassword(this.getPassword());
        account.setHomeAddress(this.getHomeAddress());
        account.setWorkAddress(this.getWorkAddress());
        account.setBirthdate(this.getBirthdate());
        account.setProfileImage(this.getProfileImage());
        account.setIcq(this.getIcq());
        account.setAdditional(this.getAdditional());
        account.setSkype(this.getSkype());
        return account;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Image getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Image profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getIcq() {
        return icq;
    }

    public void setIcq(Integer icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditional() {
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }

    public List<RoleDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDto> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "phones=" + phones +
                ", registerDate=" + registerDate +
                ", id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", birthdate=" + birthdate +
                ", profileImage=" + profileImage +
                ", icq=" + icq +
                ", skype='" + skype + '\'' +
                ", additional='" + additional + '\'' +
                ", rolesDto=" + roles +
                '}';
    }

}
