package com.getjavajob.ivanaham.web.controllers;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.RequestType.INCOMING;
import static com.getjavajob.ivanaham.domain.enums.RequestType.OUTGOING;
import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static com.getjavajob.ivanaham.web.controllers.FriendshipController.getFriendId;
import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static org.apache.logging.log4j.LogManager.getLogger;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.GroupMessage;
import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.service.AccountMessageService;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.FriendshipService;
import com.getjavajob.ivanaham.service.GroupMessageService;
import com.getjavajob.ivanaham.service.GroupService;
import com.getjavajob.ivanaham.service.MembershipService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import com.getjavajob.ivanaham.web.dto.ConversationDto;
import com.getjavajob.ivanaham.web.managers.RedisFeedManager;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/records")
public class PaginationController {

    private static final Logger LOGGER = getLogger();

    private final GroupMessageService groupMessageService;
    private final AccountMessageService accountMessageService;
    private final FriendshipService friendshipService;
    private final AccountService accountService;
    private final MembershipService membershipService;
    private final GroupService groupService;
    private final RedisFeedManager redisFeedManager;

    @Value("${pagination.messages}")
    private int recordsPerPage;

    @Value("${pagination.messages.direct}")
    private int recordsPerPageInDirect;

    public PaginationController(
            GroupMessageService groupMessageService, AccountMessageService accountMessageService,
            FriendshipService friendshipService, AccountService accountService, MembershipService membershipService,
            GroupService groupService, RedisFeedManager redisFeedManager) {
        this.groupMessageService = groupMessageService;
        this.accountMessageService = accountMessageService;
        this.friendshipService = friendshipService;
        this.accountService = accountService;
        this.membershipService = membershipService;
        this.groupService = groupService;
        this.redisFeedManager = redisFeedManager;
    }

    @RequestMapping(path = "/account/messages", produces = APPLICATION_JSON_VALUE)
    public List<AccountMessage> getAccountPageMessages(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getAccountPageMessages() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return accountMessageService.findByRecipientId(accountId, recordsPerPage, offset);
    }

    @RequestMapping(path = "/feed", produces = APPLICATION_JSON_VALUE)
    public List<AccountMessage> getFeedPageMessages(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page") int page) {
        LOGGER.debug("getFeedPageMessages() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        Set<AccountMessage> cachedMessages = redisFeedManager.findAllByAccountId(accountId);
        List<AccountMessage> requestedRangeMessages =
                cachedMessages.stream().limit(recordsPerPage).skip(offset).collect(toList());
        if (!requestedRangeMessages.isEmpty()) {
            return requestedRangeMessages;
        }
        List<AccountMessage> dbMessages = accountMessageService.findFeedMessages(accountId, recordsPerPage, offset);
        redisFeedManager.cacheMessages(dbMessages, accountId);
        return dbMessages;
    }

    @RequestMapping(path = "/group/messages", produces = APPLICATION_JSON_VALUE)
    public List<GroupMessage> getGroupPageMessages(
            @RequestParam("group-id") long groupId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getAccountPageMessages() is called: account id = {}", groupId);
        int offset = (page - 1) * recordsPerPage;
        return groupMessageService.findByRecipientId(groupId, recordsPerPage, offset);
    }

    @RequestMapping(path = "/account/friends", produces = APPLICATION_JSON_VALUE, params = "status=accepted")
    public List<Account> getAccountFriends(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getAccountFriends() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return friendshipService.find(accountId, ACCEPTED, recordsPerPage, offset).stream()
                .map(friendship -> {
                    long friendId = getFriendId(accountId, friendship);
                    return accountService.findById(friendId);
                }).collect(toList());
    }

    @RequestMapping(path = "/account/friends", produces = APPLICATION_JSON_VALUE,
            params = {"status=pending", "type=incoming"})
    public List<Account> getIncomingFriendRequests(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getIncomingFriendRequests() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return friendshipService.findPending(accountId, INCOMING, recordsPerPage, offset).stream()
                .map(friendship -> {
                    long friendId = getFriendId(accountId, friendship);
                    return accountService.findById(friendId);
                }).collect(toList());
    }

    @RequestMapping(path = "/account/friends", produces = APPLICATION_JSON_VALUE,
            params = {"status=pending", "type=outgoing"})
    public List<Account> getOutgoingFriendRequests(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getOutgoingFriendRequests() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return friendshipService.findPending(accountId, OUTGOING, recordsPerPage, offset).stream()
                .map(friendship -> {
                    long friendId = getFriendId(accountId, friendship);
                    return accountService.findById(friendId);
                }).collect(toList());
    }

    @RequestMapping(path = "/account/groups", produces = APPLICATION_JSON_VALUE, params = "status=accepted")
    public List<Group> getAccountGroups(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getAccountGroups() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return membershipService.findByAccountId(accountId, ACCEPTED, recordsPerPage, offset).stream()
                .map(Membership::getGroup).collect(toList());
    }

    @RequestMapping(path = "/account/groups", produces = APPLICATION_JSON_VALUE,
            params = {"status=pending", "type=outgoing"})
    public List<Group> getAccountGroupRequests(
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getAccountGroupRequests() is called: account id = {}", accountId);
        int offset = (page - 1) * recordsPerPage;
        return membershipService.findByAccountId(accountId, PENDING, recordsPerPage, offset).stream()
                .map(Membership::getGroup).collect(toList());
    }

    @RequestMapping(path = "/group/requests", produces = APPLICATION_JSON_VALUE)
    public List<Account> getGroupJoinRequests(
            @RequestParam("group-id") long groupId,
            @RequestParam(value = "page", defaultValue = "1") int page) {
        LOGGER.debug("getGroupJoinRequests() is called: group id = {}", groupId);
        int offset = (page - 1) * recordsPerPage;
        return membershipService.findByGroupId(groupId, PENDING, recordsPerPage, offset).stream()
                .map(Membership::getAccount).collect(toList());
    }

    @RequestMapping(path = "/group/members", params = {"status=accepted", "rights=moderator"},
            produces = APPLICATION_JSON_VALUE)
    public List<Account> getGroupModerators(
            @RequestParam("group-id") long groupId,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        LOGGER.debug("getGroupMembersPage() is called: group id = {}", groupId);
        int offset = (page - 1) * recordsPerPage;
        return membershipService.findByGroupId(groupId, ACCEPTED, MODERATOR, recordsPerPage, offset).stream()
                .map(Membership::getAccount).collect(toList());
    }

    @RequestMapping(path = "/group/members", params = {"status=accepted", "rights=common"},
            produces = APPLICATION_JSON_VALUE)
    public List<Account> getGroupCommonMembers(
            @RequestParam("group-id") long groupId,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        LOGGER.debug("getGroupMembersPage() is called: group id = {}", groupId);
        int offset = (page - 1) * recordsPerPage;
        return membershipService.findByGroupId(groupId, ACCEPTED, COMMON, recordsPerPage, offset).stream()
                .map(Membership::getAccount).collect(toList());
    }

    @RequestMapping(path = "/search", params = {"entity=account"}, produces = APPLICATION_JSON_VALUE)
    public List<Account> getAccountsByName(
            @RequestParam("name") String requestedName,
            @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
        LOGGER.debug("getAccountsByName() is called: name = {}", requestedName);
        int offset = (page - 1) * recordsPerPage;
        return accountService.findByName(requestedName, recordsPerPage, offset);
    }

    @RequestMapping(path = "/search", params = {"entity=group"}, produces = APPLICATION_JSON_VALUE)
    public List<Group> getGroupsByName(
            @RequestParam("name") String requestedName,
            @RequestParam(value = "page") int page) {
        LOGGER.debug("getGroupsByName() is called: name = {}", requestedName);
        int offset = (page - 1) * recordsPerPage;
        return groupService.findByName(requestedName, recordsPerPage, offset);
    }

    @RequestMapping(path = "/account/conversations", produces = APPLICATION_JSON_VALUE)
    public List<ConversationDto> getAccountLastConversations(
            @AuthenticationPrincipal CurrentUser currentUser,
            @RequestParam(value = "page") int page) {
        LOGGER.debug("getAccountLastConversations() is called: account id = {}", currentUser.getId());
        int offset = (page - 1) * recordsPerPage;
        return accountMessageService.findLastMessagesByAccountId(currentUser.getId(), recordsPerPage, offset).stream()
                .map(message -> {
                    Account buddyAccount = getBuddyAccount(currentUser.getId(), message);
                    return new ConversationDto(message, buddyAccount);
                }).collect(toList());
    }

    @RequestMapping(path = "/account/conversations/excluding", produces = APPLICATION_JSON_VALUE)
    public List<ConversationDto> getAccountLastConversationsWithAccountTo(
            @AuthenticationPrincipal CurrentUser currentUser,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "account-to-id") long accountToId) {
        LOGGER.debug("getAccountLastConversationsWithAccountTo() is called: account id = {}, accountToId = {}",
                currentUser.getId(), accountToId);
        int offset = (page - 1) * recordsPerPage;
        List<AccountMessage> messages = accountMessageService.findLastMessagesByAccountIdExcluding(
                currentUser.getId(), accountToId, recordsPerPage, offset);
        if (page == 1) {
            messages = getMessagesWithAccountTo(currentUser.getId(), accountToId, messages);
        }
        return messages.stream()
                .map(message -> {
                    Account buddyAccount = getBuddyAccount(currentUser.getId(), message);
                    return new ConversationDto(message, buddyAccount);
                }).collect(toList());
    }

    private List<AccountMessage> getMessagesWithAccountTo(long currentUserId, long accountToId, List<AccountMessage> messages) {
        AccountMessage accountToMessage = getAccountLastMessageIn(currentUserId, accountToId);
        if (messages.isEmpty()) {
            return singletonList(accountToMessage);
        }
        messages.add(0, accountToMessage);
        return messages;
    }

    private Account getBuddyAccount(long currentUserId, AccountMessage message) {
        Account author = message.getAuthor();
        if (isNull(author)) {
            return message.getRecipient();
        }
        return author.getId() == currentUserId ? message.getRecipient() : author;
    }

    @RequestMapping(path = "/account/chat", produces = APPLICATION_JSON_VALUE)
    public List<AccountMessage> getAccountMessages(
            @AuthenticationPrincipal CurrentUser currentUser,
            @RequestParam("account-id") long accountId,
            @RequestParam(value = "offset") int offset) {
        LOGGER.debug("getAccountMessages() is called: account id = {}, offset = {}", accountId, offset);
        return accountMessageService.findByParticipantIds(currentUser.getId(), accountId, recordsPerPageInDirect, offset);
    }

    @RequestMapping(path = "/account", produces = APPLICATION_JSON_VALUE)
    public Account getAccount(@RequestParam("account-id") long accountId) {
        LOGGER.debug("getAccount() is called: account id = {}", accountId);
        return accountService.findById(accountId);
    }

    public AccountMessage getAccountLastMessageIn(long currentUserId, long accountId) {
        AccountMessage message = accountMessageService.findLastDirectMessage(currentUserId, accountId);
        message.setRecipient(accountService.findById(accountId));
        return message;
    }

}
