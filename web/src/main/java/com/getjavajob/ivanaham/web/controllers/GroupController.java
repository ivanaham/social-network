package com.getjavajob.ivanaham.web.controllers;

import static com.getjavajob.ivanaham.web.controllers.ImageController.isEmpty;
import static java.time.LocalDate.now;
import static java.util.Objects.isNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.GroupMessage;
import com.getjavajob.ivanaham.domain.Image;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.GroupService;
import com.getjavajob.ivanaham.service.MembershipService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import com.getjavajob.ivanaham.web.dto.GroupDto;
import com.getjavajob.ivanaham.web.exceptions.NotFoundException;
import java.sql.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class GroupController {

    private static final Logger LOGGER = getLogger();
    private static final String GROUP_ID_PARAM = "group-id";

    private final AccountService accountService;
    private final GroupService groupService;
    private final MembershipService membershipService;

    public GroupController(AccountService accountService, GroupService groupService,
                           MembershipService membershipService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.membershipService = membershipService;
    }

    protected static Group getGroupOrThrow(GroupService service, long groupId) {
        LOGGER.debug("getGroupOrThrow() is called; group id = {}", groupId);
        Group retrievedGroup = service.findById(groupId);
        if (isNull(retrievedGroup)) {
            throw new NotFoundException();
        }
        return retrievedGroup;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @ModelAttribute
    public GroupDto getGroup(@RequestParam(value = "group-id", defaultValue = "0") long groupId) {
        LOGGER.debug("getGroup() is called: group id = {}", groupId);
        if (groupId != 0) {
            return GroupDto.toDto(getGroupOrThrow(groupService, groupId));
        }
        return new GroupDto();
    }

    @GetMapping("/group")
    public String getGroupPage(
            Model model,
            @RequestParam("group-id") long groupId,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("getGroupPage() is called: group id = {}", groupId);
        Group group = getGroupOrThrow(groupService, groupId);
        model.addAttribute("membership",
                membershipService.findMembership(currentUser.getId(), group.getId())
                        .orElse(membershipService.emptyMembership()));
        model.addAttribute(new GroupMessage());
        return "group/group";
    }

    @GetMapping("/group/edit")
    public String getGroupEditPage(@RequestParam("group-id") long groupId) {
        LOGGER.debug("getGroupEditPage() is called: group id = {}", groupId);
        return "group/group-edit";
    }

    @GetMapping("/group/create")
    public String getGroupCreatePage() {
        return "group/group-create";
    }

    @PostMapping(path = "/group")
    public String createGroup(@ModelAttribute GroupDto groupDto, RedirectAttributes attributes,
                              @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("createGroup() is called: name = {}", groupDto.getName());
        Group group = groupDto.toDomainEntity();
        group.setCreationDate(Date.valueOf(now()));
        group.setCreator(accountService.findById(currentUser.getId()));
        groupService.create(group);
        attributes.addAttribute(GROUP_ID_PARAM, group.getId());
        return "redirect:group";
    }

    @PutMapping(path = "/group")
    public String editGroup(@ModelAttribute GroupDto groupDto, RedirectAttributes attributes) {
        LOGGER.debug("editGroup() is called: id = {}", groupDto.getId());
        Group group = groupDto.toDomainEntity();
        verifyImage(group);
        groupService.edit(group);
        attributes.addAttribute(GROUP_ID_PARAM, group.getId());
        attributes.addFlashAttribute("message", "success");
        return "redirect:group/edit";
    }

    private void verifyImage(Group group) {
        if (isEmpty(group.getProfileImage())) {
            Image profileImage = groupService.findById(group.getId()).getProfileImage();
            group.setProfileImage(profileImage);
        }
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public String getDuplicateViolationMessage(HttpServletRequest request,
                                               RedirectAttributes attributes) {
        LOGGER.debug("getDuplicateViolationMessage() is called");
        attributes.addFlashAttribute("message", "unique");
        return "redirect:" + request.getHeader("referer");
    }

}
