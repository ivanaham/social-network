package com.getjavajob.ivanaham.web.controllers;

import static org.apache.logging.log4j.LogManager.getLogger;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.service.AccountService;
import com.getjavajob.ivanaham.service.GroupService;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Search {

    private static final Logger LOGGER = getLogger();

    private final AccountService accountService;
    private final GroupService groupService;

    @Value("${pagination.messages}")
    private int recordsPerPage;

    public Search(AccountService accountService, GroupService groupService) {
        this.accountService = accountService;
        this.groupService = groupService;
    }

    @RequestMapping("/search")
    public String getSearchPage(Model model, @RequestParam("name") String requestedName) {
        LOGGER.debug("getSearchPage() is called; request = {}", requestedName);
        model.addAttribute("searchRequest", requestedName);
        return "search";
    }

    @ResponseBody
    @PostMapping(value = "/accounts/search", produces = APPLICATION_JSON_VALUE)
    public List<Account> getAccounts(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam("name") String requestedName) {
        LOGGER.debug("getAccounts() is called; request = {}", requestedName);
        int offset = (page - 1) * recordsPerPage;
        return accountService.findByName(requestedName, recordsPerPage, offset);
    }

    @ResponseBody
    @PostMapping(value = "/groups/search", produces = APPLICATION_JSON_VALUE)
    public List<Group> getGroups(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam("name") String requestedName) {
        LOGGER.debug("getGroups() is called; request = {}", requestedName);
        int offset = (page - 1) * recordsPerPage;
        return groupService.findByName(requestedName, recordsPerPage, offset);
    }

}
