package com.getjavajob.ivanaham.web.controllers;

import static java.time.LocalDateTime.now;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.GroupMessage;
import com.getjavajob.ivanaham.service.GroupMessageService;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller
public class GroupMessageController {

    private static final Logger LOGGER = getLogger();

    private final GroupMessageService groupMessageService;

    public GroupMessageController(GroupMessageService groupMessageService) {
        this.groupMessageService = groupMessageService;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(LocalDateTime.class, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @PostMapping("/group/messages")
    public String createMessage(
            @RequestHeader("referer") String referer,
            @ModelAttribute GroupMessage message,
            @AuthenticationPrincipal CurrentUser currentUser) {
        LOGGER.debug("createMessage() is called; message = {}, sender id = {}", message, currentUser.getId());
        Account author = new Account();
        author.setId(currentUser.getId());
        message.setAuthor(author);
        message.setDateTimeSent(now());
        groupMessageService.create(message);
        return "redirect:" + referer;
    }

}
