package com.getjavajob.ivanaham.web.dto;

import static java.util.Objects.nonNull;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Image;
import java.util.Date;

public class GroupDto {

    private long id;
    private Date creationDate;
    private Account creator;
    private Image profileImage;
    private String name;
    private String description;

    public static GroupDto toDto(Group group) {
        GroupDto groupDto = new GroupDto();
        groupDto.setId(group.getId());
        groupDto.setCreationDate(group.getCreationDate());
        groupDto.setCreator(group.getCreator());
        groupDto.setProfileImage(group.getProfileImage());
        groupDto.setName(group.getName());
        groupDto.setDescription(group.getDescription());
        return groupDto;
    }

    public Group toDomainEntity() {
        Group group = new Group();
        group.setId(this.getId());
        if (nonNull(this.getCreationDate())) {
            group.setCreationDate(this.getCreationDate());
        }
        group.setCreator(this.getCreator());
        group.setProfileImage(this.getProfileImage());
        group.setName(this.getName());
        group.setDescription(this.getDescription());
        return group;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public Image getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Image profileImage) {
        this.profileImage = profileImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        long creatorId = nonNull(creator) ? creator.getId() : 0;
        return "GroupDto{" +
                "id=" + id +
                ", creationDate=" + creationDate +
                ", creatorId=" + creatorId +
                ", profileImage=" + profileImage +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
