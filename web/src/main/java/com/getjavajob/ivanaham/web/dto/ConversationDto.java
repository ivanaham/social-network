package com.getjavajob.ivanaham.web.dto;

import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Message;

public class ConversationDto {

    private final Message message;
    private final Account buddyAccount;

    public ConversationDto(Message message, Account buddyAccount) {
        this.message = message;
        this.buddyAccount = buddyAccount;
    }

    public Message getMessage() {
        return message;
    }

    public Account getBuddyAccount() {
        return buddyAccount;
    }

    @Override
    public String toString() {
        return "ConversationDto{" +
                "message=" + message +
                ", buddyAccount=" + buddyAccount +
                '}';
    }

}
