class PhoneBoxManager {

    pattern = /^[+]?[(]?\d{3}[)]?[-\s.]?\d{3}[-\s.]?\d{4,6}$/;
    phoneLeftSelector = ".phone-left";
    phoneRightSelector = ".phone-right";

    addInvalidTypeStyles() {
        $("#type-button").addClass("red");
        $("#type").on("selectmenuchange", function () {
            if ($(".ui-selectmenu-text").text() !== $("option[value='null']").text()) {
                $("#type-button").removeClass("red");
            }
        });
    }

    addInvalidNumberStyles() {
        const numberField = $(".phone-left");
        numberField.first().addClass("red");
        $("input.red").on("keyup", function () {
            const pattern = /^[+]?[(]?\d{3}[)]?[-\s.]?\d{3}[-\s.]?\d{4,6}$/;
            if (pattern.test(numberField.val())) {
                $(".phone-left").first().removeClass("red");
            }
        });
    }

    tryAddPhone(action) {
        const phoneNumber = $(this.phoneLeftSelector).first().val();
        const phoneType = $(this.phoneRightSelector).first().find(":selected").text();
        const phoneTypeValue = $(this.phoneRightSelector).first().val();
        const isPhoneNumber = this.pattern.test(phoneNumber);
        if (!isPhoneNumber && phoneTypeValue === null) {
            this.addInvalidNumberStyles();
            this.addInvalidTypeStyles();
        } else if (!isPhoneNumber) {
            this.addInvalidNumberStyles();
        } else if (phoneTypeValue === null) {
            this.addInvalidTypeStyles();
        } else if (action === PhoneBoxManager.REGISTER) {
            $(".reg-phone-box").last().after(
                "<div class='reg-phone-box'>"
                + "<input readonly class='phone-left reg-field' type='tel' value='" + phoneNumber + "' name='numbers'>"
                + "<select class='phone-right input-field' name='types'>"
                + "<option selected value='" + phoneTypeValue + "'>" + phoneType + "</option>"
                + "</select>"
                + "<button type='button' class='remove input-field phone-btn'>"
                + "<i class='fa-solid fa-circle-minus'></i>"
                + "</button>"
                + "</div>"
            );
            this.onPhoneAdd(action);
        } else if (action === PhoneBoxManager.EDIT) {
            addPhoneOnEditPage(phoneNumber, phoneType, phoneTypeValue);
            this.onPhoneAdd(action);
        } else {
            $(".phone-left").first().addClass("red");
            $("#type-button").addClass("red");
        }
    }

    onPhoneAdd(action) {
        $(function () {
            $("select.phone-right").selectmenu();
        });
        $(this.phoneLeftSelector).first().removeClass("needed");
        $(this.phoneLeftSelector).first().val("");
        $(this.phoneRightSelector).first().val("Home");
        $(".remove").on("click", function () {
            if (action === PhoneBoxManager.EDIT) {
                const currentPhoneBox = $(this).parent(".phone-box");
                currentPhoneBox.find("input[name='isDeleted']").val(true);
                currentPhoneBox.hide();
            } else {
                $(this).parent(".reg-phone-box").remove();
            }
        });
        const typeSelect = $('#type');
        typeSelect.val("null");
        typeSelect.selectmenu("refresh");
    }

}

function addPhoneOnEditPage(number, type, typeValue) {
    $(".phone-box").last().parent().after(
        '<div class="phone-box-block">'
        + '<div class="phone-box">'
        + '<input readonly class="phone-left input-field" name="numbers"'
        + ' type="tel" value="' + number + '" />'
        + '<select name="types" class="phone-right input-field">'
        + "<option selected value='" + typeValue + "'>" + type + "</option>"
        + '</select>'
        + '<input name="isDeleted" hidden value="false"/>'
        + '<button type="button" class=" remove input-field phone-btn">'
        + '<i class="fa-solid fa-circle-minus"></i>'
        + '</button>'
        + '</div>'
        + '</div>'
    );
}

Object.defineProperties(PhoneBoxManager, {
    REGISTER: {enumerable: true, value: 0},
    EDIT: {enumerable: true, value: 1}
});