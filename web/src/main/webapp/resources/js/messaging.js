function onMessageSend() {
    $("#send").on("click", function () {
        const authorId = parseInt($(".current-account-id").text(), 10);
        const recipientId = parseInt($(".active-buddy-id").text(), 10);
        const files = $("#attached-image").prop('files');
        if (files.length) {
            const reader = new FileReader();
            reader.readAsArrayBuffer(files[0]);
            reader.onload = function () {
                stompClient.send('/app/accountIdA/' + getSmallerId(authorId, recipientId) + '/accountIdB/' +
                    getBiggerId(authorId, recipientId), {}, JSON.stringify({
                    'text': $("#text").val(),
                    'attachedImage': {
                        'imageBytes': Array.from(new Uint8Array(reader.result)),
                        'name': $("#attachedImage\\.name").val()
                    }, 'author': {
                        'id': authorId
                    }, 'recipient': {
                        'id': recipientId
                    }
                }));
                clearNoRecordsMessage();
                clearMessageForm();
            };
        } else {
            if ($("#text").val().trim().length > 0) {
                stompClient.send('/app/accountIdA/' + getSmallerId(authorId, recipientId) + '/accountIdB/' +
                    getBiggerId(authorId, recipientId), {}, JSON.stringify({
                    'text': $("#text").val(),
                    'attachedImage': {'imageBytes': []},
                    'author': {
                        'id': authorId
                    }, 'recipient': {
                        'id': recipientId
                    }
                }));
                clearNoRecordsMessage();
                clearMessageForm();
            }
        }
    });
}

function clearNoRecordsMessage() {
    $("div.chat-box > p.chat-middle").remove();
}

function clearMessageForm() {
    $("#text").val("");
    $("#attached-image").val(null);
    $("#attachedImage\\.name").val(null);
    $("label[for='attached-image']").html(
        "<span class='fa-solid fa-paperclip'></span> " + $(".attach-label").text());
}

let stompClient = null;
let currentSubscription = null;

function connect() {
    const socket = new SockJS($(".baseUrl").text() + "/messaging");
    stompClient = Stomp.over(socket);
    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    let headers = {};
    headers[header] = token;
    stompClient.connect(headers, subscribe, onError);
}

function subscribe() {
    const authorId = parseInt($(".current-account-id").text(), 10);
    const recipientId = parseInt($(".active-buddy-id").text(), 10);
    currentSubscription = stompClient.subscribe(
        '/queue/accountIdA/' + getSmallerId(authorId, recipientId) + '/accountIdB/' + getBiggerId(authorId, recipientId),
        onMessageReceived);
}

function unsubscribe() {
    currentSubscription.unsubscribe();
}

function onMessageReceived(payload) {
    const authorId = parseInt($(".current-account-id").text(), 10);
    const message = JSON.parse(payload.body);
    const currentDate = $(".message-date").last().text();
    const messageDate = parseLocalDate(message.dateTimeSent);
    if (currentDate !== messageDate) {
        $(".chat-box").append(
            "<p class='less-visible message-date'>" + messageDate + "</p>" +
            "<hr class='less-visible message-date-hr'>"
        );
    }
    if (message.author.id === authorId) {
        appendMessageWithClass(message, "msg-from");
    } else {
        appendMessageWithClass(message, "msg-to");
    }
    $("div[class^='item'].active").find(".item-text").text(message.text);
}

function getSmallerId(idA, idB) {
    return idA < idB ? idA : idB;
}

function getBiggerId(idA, idB) {
    return idA > idB ? idA : idB;
}

function onError(error) {
    console.error(error);
}