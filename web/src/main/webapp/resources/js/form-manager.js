jQuery(function () {
    let phoneManager;
    $("#edit-add").on("click", function () {
        if (phoneManager == null) {
            phoneManager = new PhoneBoxManager();
        }
        phoneManager.tryAddPhone(PhoneBoxManager.EDIT);
    });

    $("#edit-form, #reg-form").on("submit", function () {
        const accountFields = $(".phone-box, .reg-phone-box");
        accountFields.find("input[name='numbers']").each(function (index) {
            $(this).attr("id", "phones" + index + ".number");
            $(this).attr("name", "phones[" + index + "].number");
        });
        accountFields.find("select[name='types']").each(function (index) {
            $(this).attr("id", "phones" + index + ".type");
            $(this).attr("name", "phones[" + index + "].type");
        });
        accountFields.find("input[name='isDeleted']").each(function (index) {
            $(this).attr("id", "phones" + index + ".isDeleted");
            $(this).attr("name", "phones[" + index + "].isDeleted");
        });
    });

    $("#reg-add").on("click", function () {
        if (phoneManager == null) {
            phoneManager = new PhoneBoxManager();
        }
        phoneManager.tryAddPhone(PhoneBoxManager.REGISTER);
    });
    $(".remove").on("click", function () {
        const currentPhoneBox = $(this).parent(".phone-box");
        currentPhoneBox.find("input[name='isDeleted']").val(true);
        currentPhoneBox.hide();
        $(this).parent(".reg-phone-box").remove();
    });
    $("#upload-image").on("change", function () {
        if (this.files[0].size > 100000) {
            const errorText = $("#max-file-size").val();
            $(".common > label[for='upload-image']").parent().parent().append('<label for="profileImage.name" class="error">' +
                errorText + '</label>');
            $(".reg-field > label[for='upload-image']").parent().after('<label for="profileImage.name" class="error">' +
                errorText + '</label>');
            $("#upload-image").val(null);
        } else {
            const fileName = this.files[0].name;
            $(".upload-text").text(fileName);
            $("#profileImage\\.name").val(fileName);
        }
    });

    $("#import").on("change", function () {
        $("#import-submit").trigger("click");
    });

    $("#reg-form").validate({
        "submitHandler": function (form) {
            form.submit();
        }
    });
    $("#edit-form").validate({
        errorPlacement: function (error, element) {
            element.parent().after(error);
        }
    });
    $("#password-form").validate({
        rules: {
            password: {},
            "confirm-password": {
                equalTo: "#password"
            }
        },
        errorPlacement: function (error, element) {
            element.parent().after(error);
        }
    });
    $.datepicker.setDefaults($.datepicker.regional["en"]);
    $("#datepicker").datepicker($.extend({},
        $.datepicker.regional[$("html").attr("lang")], {
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            startDate: '01/01/1940',
            yearRange: '1940:2030'
        }));
    $("select.phone-right").selectmenu();

    function createDialog(dialogForm, submitBtn, dialogOpenBtn) {
        const dialog = $(dialogForm).dialog({
            autoOpen: false,
            modal: true,
            buttons: {
                "Save": function () {
                    $(submitBtn).trigger("click");
                    dialog.dialog("close");
                },
                "Cancel": function () {
                    dialog.dialog("close");
                }
            }
        });
        $(dialogOpenBtn).button().on("click", function () {
            dialog.dialog("open");
        });
    }

    createDialog(".dialog-form", ".submit", ".dialog-open");
    createDialog(".dialog-password-form", ".password-submit", ".password-dialog-open");

    const alertPassMsgSelector = ".pass-msg > div[class^='alert']";
    if ($(alertPassMsgSelector).length) {
        $('html, body').animate({
            scrollTop: $(alertPassMsgSelector).offset().top
        }, 'slow');
    }

    if ($(".account-edit-page").length) {
        if ($("input.role-name[value='ROLE_ADMIN']").length) {
            $("option[value='ADMIN']").prop('selected', true);
        } else {
            $("option[value='USER']").prop('selected', true);
        }
        $("select#role").selectmenu({
            change: function (_event, data) {
                if (data.item.value === "ADMIN") {
                    const adminNameElement = $("input.role-name[value='ROLE_ADMIN']");
                    if (adminNameElement.length === 0) {
                        $(".role-group").after(
                            "<div class='role-group'>"
                            + "<input class='role-id' hidden value='2'>"
                            + "<input class='role-name' hidden value='ROLE_ADMIN'>"
                            + "<input class='role-deleted' hidden value='false'/>"
                            + "</div>"
                        );
                    } else {
                        adminNameElement.siblings("input.role-deleted").val(false);
                    }
                } else {
                    $("input.role-name[value='ROLE_ADMIN']").siblings("input.role-deleted").val(true);
                }
            }
        });
    }
    $("#edit-form.account-form").on("submit", function () {
        $(this).find(".role-id").each(function (index) {
            $(this).attr("id", "roles" + index + ".id");
            $(this).attr("name", "roles[" + index + "].id");
        });
        $(this).find(".role-name").each(function (index) {
            $(this).attr("id", "roles" + index + ".name");
            $(this).attr("name", "roles[" + index + "].name");
        });
        $(this).find(".role-deleted").each(function (index) {
            $(this).attr("id", "roles" + index + ".isDeleted");
            $(this).attr("name", "roles[" + index + "].isDeleted");
        });
    });

});