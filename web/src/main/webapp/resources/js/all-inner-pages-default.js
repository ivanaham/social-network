jQuery(function () {
    /* Attached image size check, displaying its name */
    onImageAttach();

    /* Adding "active" style to the corresponding navigation tab */
    const title = $("title").text().trim();
    $(".nav-list").find("a").each(function () {
        if ($(this).text().trim() === title) {
            $(this).addClass("active");
        }
    });

    /* Handling locale switch */
    $(".locale-btn").on("click", function () {
        let url = new URL(window.location.href);
        const newLocale = $(this).attr("about");
        url.searchParams.set('locale', newLocale);
        window.location = url.href;
    });

    const baseUrl = $(".baseUrl").text();

    /* Handling ajax search */
    const searchField = $("input[type='search']");
    const tableContainer = $(".table-container");
    searchField.on("focusout", function () {
        $(".table-container").empty();
    });
    let xhrAccounts, xhrGroups;
    searchField.on("keyup", function () {
        const name = $(this).val();
        tableContainer.empty();
        if (name.length >= 2) {
            tableContainer.append("<table class='lookup'></table>");
            if (xhrAccounts) {
                xhrAccounts.abort();
            }
            if (xhrGroups) {
                xhrGroups.abort();
            }
            xhrAccounts = searchEntities(name, baseUrl + "/accounts/search", parseAndAppendAccountsJson);
            xhrGroups = searchEntities(name, baseUrl + "/groups/search", parseAndAppendGroupsJson);
        }
    });

    const token = $("meta[name='_csrf']").attr("content");
    const header = $("meta[name='_csrf_header']").attr("content");
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
        }
    });

    function searchEntities(name, url, successFunc) {
        return $.ajax({
            url: url,
            data: {
                name: name
            },
            method: "POST",
            dataType: "json",
            success: function (json) {
                successFunc(json, baseUrl);
            },
            error: function (e) {
                console.error(e);
            }
        });
    }

    function parseAndAppendAccountsJson(accounts) {
        $(".lookup").append("<div class='acct-section'></div>");
        const acctSection = $(".acct-section");
        acctSection.append("<tr class='lookup-row'><th>" + $("#accounts").val() + "</th></tr>");
        if (accounts.length !== 0) {
            $.each(accounts, function (_index, value) {
                const firstName = value.firstName;
                const middleName = value.middleName;
                const lastName = value.lastName;
                let fullName;
                if (middleName === null) {
                    fullName = firstName + ' ' + lastName;
                } else {
                    fullName = firstName + ' ' + middleName + ' ' + lastName;
                }
                acctSection.append("<tr class='lookup-row'><td onmousedown='location.href=\x22" +
                    baseUrl + "/account?account-id=" + value.id + "\x22;'>" + fullName + "</td></tr>");
            });
        } else {
            acctSection.append("<tr class='lookup-row'><td class='no-data'>" + $("#no-accounts").val() + "</td></tr>");
        }
    }

    function parseAndAppendGroupsJson(groups) {
        $(".lookup").append("<div class='group-section'></div>");
        const groupSection = $(".group-section");
        groupSection.append("<tr class='lookup-row'><th>" + $("#groups").val() + "</th></tr>");
        if (groups.length !== 0) {
            $.each(groups, function (_index, value) {
                groupSection.append("<tr class='lookup-row'><td onmousedown='location.href=\x22" +
                    baseUrl + "/group?group-id=" + value.id + "\x22;'>" + value.name + "</td></tr>");
            });
        } else {
            groupSection.append("<tr class='lookup-row'><td class='no-data'>" + $("#no-groups").val() + "</td></tr>");
        }
    }

    if (($("body.account-page").length)) {
        loadAccountMessages(1);
    }
    if (($("body.account-friends-page").length)) {
        loadFriends(1);
    }
    if (($("body.account-friends-requests-page").length)) {
        loadIncomingFriendRequests(1);
        loadOutgoingFriendRequests(1);
    }
    if (($("body.groups-page").length)) {
        loadGroups(1);
    }
    if (($("body.outgoing-group-requests-page").length)) {
        loadOutgoingGroupRequests(1);
    }
    if ($("body.group-page").length && $(".records-container").length) {
        loadGroupMessages(1);
    }
    if ($("body.group-requests-page").length) {
        loadGroupRequests(1);
    }
    if ($("body.group-members-page").length) {
        loadGroupModerators(1);
        loadGroupCommonMembers(1);
    }
    if ($("body.feed-page").length) {
        loadFeed(1);
    }
    if ($("body.search-page").length) {
        loadAccountsByName(1);
        loadGroupsByName(1);
    }
    if ($("body.account-messages-page").length) {
        loadAllConversations(1).done(function () {
            if ($("div[class^='item']").length > 0) {
                markActiveItem();
                connect();
                loadChat();
            }
        });
    }
});

function loadFeed(page) {
    const containerElement = $(".records-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/feed", {
        "account-id": accountId,
        "page": page
    }, function (data) {
        containerElement.empty();
        if (data.length !== 0) {
            $.each(data, function (_index, value) {
                appendFeedMessageBlock(containerElement, value, 'account-message');
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, data.length, loadFeed);
    }, "json");
}

function loadChat() {
    const activeChatAccountId = $(".active-buddy-id").text();
    getAccountById(activeChatAccountId).done(function (buddyAccount) {
        const chatContainer = $(".content.container");
        chatContainer.empty();
        chatContainer.append(
            "<h2 class='light edit-header'>" + $(".chat-with-label").text() + " " + buddyAccount.firstName + " " +
            buddyAccount.lastName + "</h2>" +
            "<div class='chat-box'></div>"
        );
        getMessagesByAccountId(buddyAccount.id).done(function (messages) {
            const recordsPerPage = parseInt($(".direct-records").text(), 10);
            if (messages.length > 0 && messages.length % recordsPerPage === 0) {
                prependLoadMoreButton();
            }
            if (messages.length === 0) {
                $(".chat-box").append("<p class='less-visible chat-middle'><em>" +
                    $(".messages-empty").text() + "</em></p>")
            } else {
                appendMessages(messages);
            }
            appendMessageForm(buddyAccount.id);
        });
    });
}

function loadMoreChat() {
    $("#load-more").remove();
    const activeChatAccountId = $("div[class^='item'].active").find(".item-id").text();
    getMessagesByAccountId(activeChatAccountId).done(function (messages) {
        const recordsPerPage = parseInt($(".direct-records").text(), 10);
        if (messages.length > 0) {
            prependMessages(messages);
        }
        if (messages.length === recordsPerPage) {
            prependLoadMoreButton();
        }
    });
}

function appendMessageForm(recipientId) {
    $(".chat-box").after(
        "<form id='message' name='spec-positioned' class='msg-form' " +
        "action='" + $(".baseUrl").text() + "/account/messages?type=direct' method='POST' enctype='multipart/form-data'>" +
        "<input id='messageType' name='messageType' value='DIRECT' type='hidden'>" +
        "<input id='recipient.id' name='recipient.id' value='" + recipientId + "' type='hidden'>" +
        "<textarea id='text' name='text' placeholder='" + $(".message-placeholder").text() + "' wrap='off' " +
        "rows='4'></textarea>" +
        "<label for='attached-image'><span class='fa-solid fa-paperclip'></span> " + $(".attach-label").text() +
        "</label>" +
        "<input id='attached-image' name='attachedImage.imageBytes' type='file' accept='image/png, image/jpeg'>" +
        "<input id='attachedImage.name' name='attachedImage.name' type='hidden'>" +
        "<button id='send' type='button'>" + $(".send-label").text() + "</button>" +
        "</form>"
    );
    onMessageSend();
    onImageAttach();
}

function appendMessages(messages) {
    let currentDate;
    $.each(messages, function (_index, message) {
        const messageDate = parseLocalDate(message.dateTimeSent);
        if (currentDate !== messageDate) {
            currentDate = messageDate;
            $(".chat-box").append(
                "<p class='less-visible message-date'>" + currentDate + "</p>" +
                "<hr class='less-visible message-date-hr'>"
            );
        }
        const currentAccountId = parseInt($(".current-account-id").text(), 10);
        if (message.author.id === currentAccountId) {
            appendMessageWithClass(message, "msg-from");
        } else {
            appendMessageWithClass(message, "msg-to");
        }
    });
}

function prependMessages(messages) {
    $.each(messages.reverse(), function (_index, message) {
        const lastDateElement = $(".message-date").first();
        let lastDate = lastDateElement.text();
        const newMessageDate = parseLocalDate(message.dateTimeSent);
        if (lastDate === newMessageDate) {
            lastDateElement.remove();
            $(".message-date-hr").first().remove();
        }
        const currentAccountId = parseInt($(".current-account-id").text(), 10);
        if (message.author.id === currentAccountId) {
            prependMessageWithClass(message, "msg-from");
        } else {
            prependMessageWithClass(message, "msg-to");
        }
        $(".chat-box").prepend(
            "<p class='less-visible message-date'>" + newMessageDate + "</p>" +
            "<hr class='less-visible message-date-hr'>"
        );
    });
}

function appendMessageWithClass(message, cssClass) {
    $(".chat-box").append(
        "<div class='msg " + cssClass + "'>" +
        "<pre>" + message.text + "</pre>" +
        getImageByTypeIfExist(message.attachedImage.imageBytes, message.id, 'account-message') +
        "<sub class='less-visible'>" + parseLocalTime(message.dateTimeSent) + "</sub></div>"
    );
}

function prependMessageWithClass(message, cssClass) {
    $(".chat-box").prepend(
        "<div class='msg " + cssClass + "'>" +
        "<pre>" + message.text + "</pre>" +
        getImageByTypeIfExist(message.attachedImage.imageBytes, message.id, "account-message") +
        "<sub class='less-visible'>" + parseLocalTime(message.dateTimeSent) + "</sub></div>"
    );
}

function prependLoadMoreButton() {
    $(".chat-box").prepend("<button id='load-more'>" + $(".load-more-label").text() + "</button>");
    $("#load-more").on("click", function () {
        loadMoreChat();
    });
}

function getMessagesByAccountId(accountId) {
    return $.post($(".baseUrl").text() + "/records/account/chat", {
        "account-id": accountId,
        "offset": $(".msg-from").length + $(".msg-to").length
    }, "json");
}

function loadAllConversations(page) {
    const accountId = $(".account-to-id").text();
    if (accountId === "") {
        return loadLastMessages(page);
    } else {
        return loadLastMessagesExcluding(page);
    }
}

function loadLastMessages(page) {
    const containerElement = $(".buddies-container");
    return $.post($(".baseUrl").text() + "/records/account/conversations", {
        "page": page
    }, function (conversations) {
        containerElement.empty();
        $.each(conversations, function (index, conversation) {
            appendConversationsBlock(containerElement, conversation, index);
            if (isActive(conversation.message)) {
                $(".item" + index).addClass("active");
            }
        });
        if (conversations.length === 0) {
            const recordsEmptyText = "<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>";
            containerElement.append(recordsEmptyText);
        }
        appendPageNavigation($(".pages"), page, conversations.length, loadLastMessages);
    }, "json");
}

function loadLastMessagesExcluding(page) {
    const accountId = $(".account-to-id").text();
    const containerElement = $(".buddies-container");
    return $.post($(".baseUrl").text() + "/records/account/conversations/excluding", {
            "page": page,
            "account-to-id": accountId
        }, function (conversations) {
            containerElement.empty();
            $.each(conversations, function (index, conversation) {
                appendConversationsBlock(containerElement, conversation, index);
                if (isActive(conversation.message)) {
                    $(".item" + index).addClass("active");
                }
            });
            const currentRecordsPage = page === 1 ? conversations.length - 1 : conversations.length;
            appendPageNavigation($(".pages"), page, currentRecordsPage, loadLastMessagesExcluding);
        }, "json"
    );
}

function isActive(message) {
    let activeAccountId = $(".active-buddy-id").text();
    const buddyAccountId = getBuddyAccountId($(".current-account-id").text(), message);
    return activeAccountId !== "" && buddyAccountId === parseInt(activeAccountId, 10);

}

function markActiveItem() {
    let activeItemElement;
    let activeAccountIdElement = $(".active-buddy-id");
    activeItemElement = $("div[class^='item']").first();
    activeAccountIdElement.text(activeItemElement.find(".item-id").text());
    activeItemElement.addClass("active");
}

function getAccountById(accountId) {
    return $.post($(".baseUrl").text() + "/records/account", {
        "account-id": accountId
    }, "json");
}

function loadGroupsByName(page) {
    const containerElement = $(".groups-container");
    const name = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/search", {
        "name": name,
        "page": page,
        "entity": "group"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendGroupBlock(containerElement, value, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".groups-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-groups"), page, json.length, loadGroupsByName);
    }, "json");
}

function loadAccountsByName(page) {
    const containerElement = $(".accounts-container");
    const name = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/search", {
        "name": name,
        "page": page,
        "entity": "account"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".accounts-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-accounts"), page, json.length, loadAccountsByName);
    }, "json");
}

function loadGroupCommonMembers(page) {
    const containerElement = $(".common-container");
    const groupId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/group/members", {
        "group-id": groupId,
        "page": page,
        "rights": "common",
        "status": "accepted"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendMessageButton(containerElement, value.id, index);
                appendRightsUpgradeButton(containerElement, value.id, groupId, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".common-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-common"), page, json.length, loadGroupCommonMembers);
    }, "json");
}

function loadGroupModerators(page) {
    const containerElement = $(".moderator-container");
    const groupId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/group/members", {
        "group-id": groupId,
        "page": page,
        "rights": "moderator",
        "status": "accepted"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendMessageButton(containerElement, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".moderator-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-moderator"), page, json.length, loadGroupModerators);
    }, "json");
}

function loadGroups(page) {
    const containerElement = $(".records-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/groups", {
        "account-id": accountId,
        "page": page,
        "status": "accepted"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendGroupBlock(containerElement, value, index);
                appendLeaveButton(containerElement, accountId, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, json.length, loadGroups);
    }, "json");
}

function loadGroupRequests(page) {
    const containerElement = $(".records-container");
    const groupId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/group/requests", {
        "group-id": groupId,
        "page": page
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendAcceptGroupRequestButton(containerElement, value.id, groupId, index);
                appendDeclineGroupRequestButton(containerElement, value.id, groupId, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, json.length, loadGroupRequests);
    }, "json");
}

function loadOutgoingGroupRequests(page) {
    const containerElement = $(".records-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/groups", {
        "account-id": accountId,
        "page": page,
        "status": "pending",
        "type": "outgoing"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendGroupBlock(containerElement, value, index);
                appendCancelGroupRequestButton(containerElement, accountId, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, json.length, loadOutgoingGroupRequests);
    }, "json");
}

function loadFriends(page) {
    const containerElement = $(".records-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/friends", {
        "account-id": accountId,
        "page": page,
        "status": "accepted"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendMessageButton(containerElement, value.id, index);
                appendFriendRemoveButton(containerElement, accountId, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, json.length, loadFriends);
    }, "json");
}

function loadIncomingFriendRequests(page) {
    const containerElement = $(".incoming-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/friends", {
        "account-id": accountId,
        "page": page,
        "status": "pending",
        "type": "incoming"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendAcceptFriendRequestButton(containerElement, accountId, value.id, index);
                appendDeclineFriendRequestButton(containerElement, accountId, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".incoming-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-incoming"), page, json.length, loadIncomingFriendRequests);
    }, "json");
}

function loadOutgoingFriendRequests(page) {
    const containerElement = $(".outgoing-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/friends", {
        "account-id": accountId,
        "page": page,
        "status": "pending",
        "type": "outgoing"
    }, function (json) {
        containerElement.empty();
        if (json.length !== 0) {
            $.each(json, function (index, value) {
                appendAccountBlock(containerElement, value, index);
                appendCancelFriendRequestButton(containerElement, accountId, value.id, index);
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".outgoing-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages-outgoing"), page, json.length, loadOutgoingFriendRequests);
    }, "json");
}

function loadAccountMessages(page) {
    const containerElement = $(".records-container");
    const accountId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/account/messages", {
        "account-id": accountId,
        "page": page
    }, function (data) {
        containerElement.empty();
        if (data.length !== 0) {
            $.each(data, function (_index, value) {
                appendWallMessageBlock(containerElement, value, 'account-message');
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, data.length, loadAccountMessages);
    }, "json");
}

function loadGroupMessages(page) {
    const containerElement = $(".records-container");
    const groupId = containerElement.attr("about");
    $.post($(".baseUrl").text() + "/records/group/messages", {
        "group-id": groupId,
        "page": page
    }, function (data) {
        containerElement.empty();
        if (data.length !== 0) {
            $.each(data, function (_index, value) {
                appendWallMessageBlock(containerElement, value, "group-message");
            });
        } else {
            containerElement.append("<p class='less-visible'><em>" + $(".records-empty").text() + "</em></p>");
        }
        appendPageNavigation($(".pages"), page, data.length, loadGroupMessages);
    }, "json");
}

function appendPageNavigation(pagesElement, page, currentRecordsNumber, traversalFunction) {
    pagesElement.empty();
    if (page > 1) {
        pagesElement.append("<li class='page-back'><a><span class='fa-solid fa-angle-left'></span></a></li>");
        pagesElement.children(".page-back").on("click", function () {
            traversalFunction(page - 1);
        });
    }
    pagesElement.append("<li class='active'>" + page + "</li>");
    const totalRecordsPerPage = parseInt($(".records-per-page").text(), 10);
    if (currentRecordsNumber === totalRecordsPerPage) {
        pagesElement.append("<li class='page-forward'><a><span class='fa-solid fa-angle-right'></span></a></li>");
        pagesElement.children(".page-forward").on("click", function () {
            traversalFunction(page + 1);
        });
    }
}

/**
 * @param container                         Jquery container, where account will be appended.
 * @param group                             Information about the group.
 * @param group.id                          Group's id.
 * @param group.name                        Group's name.
 * @param group.description                 Group's description.
 * @param group.profileImage                Group's profile image.
 * @param group.profileImage.imageBytes     Image's bytes.
 * @param index                             Current group's index.
 */
function appendGroupBlock(container, group, index) {
    const baseUrl = $(".baseUrl").text();
    container.append(
        "<div class='container entity-block'>" +
        "<div class='entity-content'>" +
        "<a href='" + baseUrl + "/group?group-id=" + group.id + "'>" +
        getGroupImageOrDefault(group.profileImage, group.id) +
        "</a>" +
        "<span class='to-gradient'>" +
        "<a href='" + baseUrl + "/group?group-id=" + group.id + "'>" +
        "<p class='entity-name'><strong>" + group.name + "</strong></p>" +
        "</a>" +
        addGroupDescriptionIfExist(group.description) +
        "</span>" +
        "</div>" +
        "<div class='group-actions" + index + "'>" +
        "</div>" +
        "</div>"
    );
}

/**
 * @param container                         Jquery container, where account will be appended.
 * @param account                           Information about the account.
 * @param account.id                        Account's id.
 * @param account.firstName                 Account's first name.
 * @param account.middleName                Account's middle name.
 * @param account.lastName                  Account's last name.
 * @param account.email                     Account's email.
 * @param account.registerDate              Account's register date.
 * @param account.profileImage              Account's profile image.
 * @param account.profileImage.imageBytes   Image's bytes.
 * @param index                             Current account's index.
 */
function appendAccountBlock(
    container, account, index) {
    const baseUrl = $(".baseUrl").text();
    container.append(
        "<div class='container entity-block'>" +
        "<div class='entity-content'>" +
        "<a href='" + baseUrl + "/account?account-id=" + account.id + "'>" +
        getAccountImageOrDefault(account.profileImage, account.id) +
        "</a>" +
        "<div class='to-gradient'>" +
        "<a href='" + baseUrl + "/account?account-id=" + account.id + "'>" +
        "<p class='entity-name'>" +
        "<strong>" + parseFullName(account.firstName, account.middleName, account.lastName) + "</strong>" +
        "</p>" +
        "</a>" +
        "<p><strong>" + $(".email-label").text() + ":</strong> " + account.email + "</p>" +
        "<p><strong>" + $(".register-date-label").text() + ":</strong> " + account.registerDate + "</p>" +
        "</div>" +
        "</div>" +
        "<div class='actions" + index + "'></div>" +
        "</div>"
    );
}

function appendConversationsBlock(container, conversation, index) {
    const buddyAccount = conversation.buddyAccount;
    container.append(
        "<div class='item" + index + "'>" +
        getConversationsImageOrDefault(buddyAccount.profileImage, buddyAccount.id) +
        "<div class='item-info'>" +
        "<p class='item-id' hidden>" + buddyAccount.id + "</p>" +
        "<p>" + buddyAccount.firstName + " " + buddyAccount.lastName + "</p>" +
        "<p class='item-text'>" + getMessageText(conversation.message) + "</p>" +
        "</div></div>"
    );
    onItemClick(index);
}

function getMessageText(message) {
    return message.text == null ? "" : message.text;
}

function onItemClick(index) {
    $(".item" + index).on("click", function () {
        unsubscribe();
        $("div[class^='item'].active").removeClass("active");
        $(this).addClass("active");
        $(".active-buddy-id").text($(this).find(".item-id").text());
        subscribe();
        loadChat();
    });
}

function getConversationsImageOrDefault(profileImage, id) {
    const baseUrl = $(".baseUrl").text();
    if (profileImage != null && profileImage.imageBytes.length !== 0) {
        return "<img alt='Profile image' src='" + baseUrl + "/images/get?account-id=" + id + "&img-type=account'/>";
    }
    return "<img class='block-image' alt='default-account-image' src='" + baseUrl + "/resources/images/icons/default-account-image.svg'/>";
}

function getBuddyAccountId(currentAccountId, message) {
    if (message.author == null) {
        return message.recipient.id;
    }
    return parseInt(currentAccountId, 10) === message.author.id ? message.recipient.id : message.author.id;
}

function appendMessageButton(recordsContainer, accountToId, index) {
    if (accountToId !== parseInt($(".currentUserId").text())) {
        recordsContainer.find(".actions" + index).append(
            "<button onclick=\"location.href='" + $(".baseUrl").text() + "/account/messages?account-to-id=" +
            accountToId + "';\" type='button' class='blue-btn'>" + $(".message-label").text() + "</button>");
    }
}

function appendFriendRemoveButton(recordsContainer, currentAccountId, friendId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/account/friends?action-maker-id=" +
        currentAccountId + "&friend-id=" + friendId + "&action=delete'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".remove-label").text() + "</button></form>");
}

function appendAcceptFriendRequestButton(recordsContainer, currentAccountId, friendId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/account/friends?action-maker-id=" +
        currentAccountId + "&friend-id=" + friendId + "&action=accept'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".accept-label").text() + "</button></form>"
    );
}

function appendDeclineFriendRequestButton(recordsContainer, currentAccountId, friendId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/account/friends?action-maker-id=" +
        currentAccountId + "&friend-id=" + friendId + "&action=decline'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".decline-label").text() + "</button></form>"
    );
}

function appendAcceptGroupRequestButton(recordsContainer, accountId, groupId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/group/members?account-id=" +
        accountId + "&group-id=" + groupId + "&action=accept'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".accept-label").text() + "</button></form>"
    );
}

function appendDeclineGroupRequestButton(recordsContainer, accountId, groupId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/group/members?account-id=" +
        accountId + "&group-id=" + groupId + "&action=decline'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".decline-label").text() + "</button></form>"
    );
}

function appendCancelFriendRequestButton(recordsContainer, currentAccountId, friendId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/account/friends?action-maker-id=" +
        currentAccountId + "&friend-id=" + friendId + "&action=cancel'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".cancel-label").text() + "</button></form>"
    );
}

function appendCancelGroupRequestButton(recordsContainer, currentAccountId, groupId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".group-actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/group/members?account-id=" +
        currentAccountId + "&group-id=" + groupId + "&action=cancel'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".cancel-label").text() + "</button></form>"
    );
}

function appendLeaveButton(recordsContainer, currentAccountId, groupId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    recordsContainer.find(".group-actions" + index).append(
        "<form method='POST' action='" + $(".baseUrl").text() + "/group/members?account-id=" +
        currentAccountId + "&group-id=" + groupId + "&action=delete'>" +
        "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
        "<button type='submit' class='blue-btn'>" + $(".leave-label").text() + "</button></form>"
    );
}

function appendRightsUpgradeButton(recordsContainer, accountId, groupId, index) {
    const csrfToken = $("meta[name='_csrf']").attr("content");
    if ($("p.rights").text() === "MODERATOR") {
        recordsContainer.find(".actions" + index).append(
            "<form method='POST' action='" + $(".baseUrl").text() + "/group/members?account-id=" +
            accountId + "&group-id=" + groupId + "&action=rights'>" +
            "<input type='hidden' name='_csrf' value='" + csrfToken + "'>" +
            "<button type='submit' class='blue-btn'>" + $(".rights-label").text() + "</button></form>"
        );
    }
}

function parseFullName(firstName, middleName, lastName) {
    if (middleName != null) {
        return firstName + " " + middleName + " " + lastName;
    }
    return firstName + " " + lastName;
}

function addGroupDescriptionIfExist(description) {
    if (description != null) {
        return "<p class='group-desc'>" + description + "</p>";
    }
    return "";
}

/**
 * @param containerElement                  Jquery container, where account will be appended.
 * @param message                           Information about the message.
 * @param message.id                        Message's id.
 * @param message.recipient.id               Message's recipient id.
 * @param message.author                    Message's author.
 * @param message.author.id                 Author's id.
 * @param message.author.firstName          Author's first name.
 * @param message.author.lastName           Author's last name.
 * @param message.text                      Message's text.
 * @param message.dateTimeSent              Message's date and time sent.
 * @param message.attachedImage             Message's attached image.
 * @param message.attachedImage.imageBytes  Image's
 * @param type                              message type - for image lookup.
 */
function appendWallMessageBlock(containerElement, message, type) {
    containerElement.append(
        "<div class='post container'>" +
        "<p class='post-header'>" +
        "<a href='" + $(".baseUrl").text() + "/account?account-id=" + message.author.id + "'>" +
        "<strong class='post-author'>" + message.author.firstName + " " + message.author.lastName + "</strong>" +
        "</a>" +
        "<span class='post-date'>" + parseLocalDateTime(message.dateTimeSent) + "</span>" +
        "</p>" +
        "<pre class='post-msg'>" + message.text + "</pre>" +
        getImageByTypeIfExist(message.attachedImage.imageBytes, message.id, type) +
        "</div>"
    );
}

function appendFeedMessageBlock(containerElement, message, type) {
    const messageLabel = $(".feed-message-label").text()
    const baseUrl = $(".baseUrl").text();
    containerElement.append(
        "<div class='post container'>" +
        "<div class='post-header'><p class='post-nested'>" +
        "<a href='" + baseUrl + "/account?account-id=" + message.author.id + "'>" +
        "<strong class='post-author'>" + message.author.firstName + " " + message.author.lastName + "</strong>" +
        "</a>" + " " + messageLabel + " " +
        "<a href='" + baseUrl + "/account?account-id=" + message.recipient.id + "'>" +
        "<strong class='post-recipient'>" + message.recipient.firstName + " " + message.recipient.lastName + "</strong>" +
        "</a></p>" +
        "<span class='post-date'>" + parseLocalDateTime(message.dateTimeSent) + "</span>" +
        "</div>" +
        "<pre class='post-msg'>" + message.text + "</pre>" +
        getImageByTypeIfExist(message.attachedImage.imageBytes, message.id, type) +
        "</div>"
    );
}

function parseLocalDateTime(dateTime) {
    const chosenLang = $("html").attr("lang");
    const localizedParticle = chosenLang === "ru" ? "в" : "at";
    let hours = dateTime[3];
    hours = hours >= 10 ? hours : "0" + hours;
    let minutes = dateTime[4];
    minutes = minutes >= 10 ? minutes : "0" + minutes;
    return dateTime[2] + "/" + dateTime[1] + "/" + dateTime[0] + " " + localizedParticle + " " + hours + ":" + minutes;
}

function parseLocalTime(dateTime) {
    let hours = dateTime[3];
    hours = hours >= 10 ? hours : "0" + hours;
    let minutes = dateTime[4];
    minutes = minutes >= 10 ? minutes : "0" + minutes;
    return hours + ":" + minutes;
}

function parseLocalDate(dateTime) {
    return dateTime[2] + "/" + dateTime[1] + "/" + dateTime[0];
}

function getImageByTypeIfExist(imageBytes, id, type) {
    if (imageBytes.length !== 0) {
        return "<img alt='attached-image' src='" + $(".baseUrl").text() + "/images/get?message-id=" + id + "&img-type=" + type + "'/>";
    }
    return "";
}

function getAccountImageOrDefault(profileImage, id) {
    const baseUrl = $(".baseUrl").text();
    if (profileImage != null && profileImage.imageBytes.length !== 0) {
        return "<img class='block-image' alt='Profile image' src='" + baseUrl + "/images/get?account-id=" + id + "&img-type=account'/>";
    }
    return "<img class='block-image' alt='default-account-image' src='" + baseUrl + "/resources/images/icons/default-account-image.svg'/>";
}

function getGroupImageOrDefault(profileImage, id) {
    const baseUrl = $(".baseUrl").text();
    if (profileImage != null && profileImage.imageBytes.length !== 0) {
        return "<img class='block-image' alt='Profile image' src='" + baseUrl + "/images/get?group-id=" + id + "&img-type=group'/>";
    }
    return "<img class='block-image' alt='default-group-image' src='" + baseUrl + "/resources/images/icons/default-group-image.svg'/>";
}

function onImageAttach() {
    $("#attached-image").on("change", function () {
        if (this.files[0].size > 1000000) {
            $("label[for='attached-image']").after('<span class="alert-error">' +
                $("#max-file-size").val() + '</span>');
            $("input[type='file']").val(null);
        } else {
            const fileName = this.files[0].name;
            $("label[for='attached-image']").html(
                "<i class='fa-solid fa-paperclip'></i> " + fileName
            );
            $("#attachedImage\\.name").val(fileName);
        }
    });
}