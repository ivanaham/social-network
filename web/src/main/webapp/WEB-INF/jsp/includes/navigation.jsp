<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>

<spring:url value="/feed" var="feedUrl"/>
<spring:url value="/account" var="myProfileUrl">
    <spring:param name="account-id" value="${currentUser.id}"/>
</spring:url>
<spring:url value="/account/friends" var="friendsUrl">
    <spring:param name="id" value="${currentUser.id}"/>
    <spring:param name="status" value="accepted"/>
</spring:url>
<spring:url value="/account/friends" var="friendRequestsUrl">
    <spring:param name="id" value="${currentUser.id}"/>
    <spring:param name="status" value="pending"/>
</spring:url>
<spring:url value="/account/groups" var="groupsUrl">
    <spring:param name="account-id" value="${currentUser.id}"/>
    <spring:param name="status" value="accepted"/>
</spring:url>
<spring:url value="/account/groups" var="groupRequestsUrl">
    <spring:param name="account-id" value="${currentUser.id}"/>
    <spring:param name="status" value="pending"/>
    <spring:param name="type" value="outgoing"/>
</spring:url>
<spring:url value="/account/messages" var="messagesUrl"/>

<nav class="container">
    <ul class="nav-list">
        <li><a href="${feedUrl}"><spring:message code="label.feed"/></a></li>
        <li><a href="${myProfileUrl}"><spring:message code="label.my.profile"/></a></li>
        <li><a href="${friendsUrl}"><spring:message code="label.my.friends"/></a></li>
        <li><a href="${friendRequestsUrl}"><spring:message code="label.my.friend.req"/></a></li>
        <li><a href="${groupsUrl}"><spring:message code="label.my.groups"/></a></li>
        <li><a href="${groupRequestsUrl}"><spring:message code="label.my.group.req"/></a></li>
        <li><a href="${messagesUrl}"><spring:message code="label.messages"/></a></li>
    </ul>
</nav>