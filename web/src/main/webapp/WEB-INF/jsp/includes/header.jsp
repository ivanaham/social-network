<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<sec:authentication var="currentUser" property="principal"/>

<header>
    <p class="baseUrl" hidden>${pageContext.request.contextPath}</p>
    <p class="records-per-page" hidden>${cookie.recordsPerPage.value}</p>
    <div>
        <h2 class=" logo header-btn">Social network</h2>
    </div>
    <form:form action="${pageContext.request.contextPath}/search" class="content">
        <spring:message code="search" var="search"/>
        <div class="search-bar">
            <input name="name" type="search" autocomplete="off" placeholder="${search}">
            <div class="table-container"></div>
        </div>
        <button type="submit" class="search-btn"><span class="fa-solid fa-magnifying-glass"></span></button>
        <spring:message code="accounts" var="accounts"/>
        <input hidden id="accounts" value="${accounts}">
        <spring:message code="groups" var="groups"/>
        <input hidden id="groups" value="${groups}">
        <spring:message code="search.accounts.empty" var="emptyAccounts"/>
        <input hidden id="no-accounts" value="${emptyAccounts}">
        <spring:message code="search.groups.empty" var="emptyGroups"/>
        <input hidden id="no-groups" value="${emptyGroups}">
        <spring:message code="label.register.error.max.file.size" var="maxFileSize"/>
        <input hidden id="max-file-size" value="${maxFileSize}">
    </form:form>
    <div class="header-right">
        <div class="h-dropdown header-btn">
            <button class="h-dropbtn"><spring:message code="label.language.change"/></button>
            <div class="h-dropdown-content">
                <a about="en" class="locale-btn" href="javascript:void(0)"><spring:message
                        code="label.language.en"/></a>
                <a about="ru" class="locale-btn" href="javascript:void(0)"><spring:message
                        code="label.language.ru"/></a>
            </div>
        </div>
        <div onclick="location.href='${pageContext.request.contextPath}/account?account-id=${currentUser.id}';"
             title="My Profile" class="header-btn">
            <span class="fa-solid fa-user"></span> ${currentUser.username}
        </div>
        <spring:url value="/logout" var="logoutUrl"/>
        <form:form method="POST" action="${logoutUrl}">
            <button type="submit" class="header-btn logout-btn">
                <span class="fa-solid fa-right-from-bracket"></span> <spring:message code="btn.logout"/>
            </button>
        </form:form>
    </div>
</header>