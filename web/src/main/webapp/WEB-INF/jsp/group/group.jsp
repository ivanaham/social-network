<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>
<c:if test="${membership != null}">
    <jsp:useBean id="membership" scope="request" type="com.getjavajob.ivanaham.domain.Membership"/>
</c:if>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title>${groupDto.name}</title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="group-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="central">
        <div class="intro container">
            <div class="intro-left">
                <div class="profile-image">
                    <c:choose>
                        <c:when test="${fn:length(groupDto.profileImage.imageBytes) != 0}">
                            <spring:url value="/images/get" var="profileImageUrl">
                                <spring:param name="group-id" value="${groupDto.id}"/>
                                <spring:param name="img-type" value="group"/>
                            </spring:url>
                            <img alt="profile-image" src="${profileImageUrl}"/>
                        </c:when>
                        <c:otherwise>
                            <img alt="Group default image"
                                 src="${pageContext.request.contextPath}/resources/images/icons/default-group-image.svg"/>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="intro-info">
                    <h4>${groupDto.name}</h4>
                    <c:if test="${groupDto.description != null}">
                        <p class="group-desc">${groupDto.description}</p>
                    </c:if>
                </div>
            </div>
            <div class="intro-right actions">
                <spring:url value="/group/members" var="membersUrl">
                    <spring:param name="account-id" value="${currentUser.id}"/>
                    <spring:param name="group-id" value="${groupDto.id}"/>
                </spring:url>
                <form:form method="post" action="${membersUrl}">
                    <c:choose>
                        <c:when test="${membership.status == null ||  membership.status == 'DECLINED'}">
                            <input hidden readonly name="action" value="add">
                            <button type="submit" class="blue-btn"><spring:message
                                    code="label.group.req.send"/></button>
                        </c:when>
                        <c:when test="${membership.status == 'PENDING'}">
                            <input hidden readonly name="action" value="cancel">
                            <button type="submit" class="blue-btn"><spring:message
                                    code="label.group.req.cancel"/></button>
                        </c:when>
                        <c:otherwise>
                            <input hidden readonly name="action" value="delete">
                            <button type="submit" class="blue-btn"><spring:message
                                    code="label.group.req.leave"/></button>
                        </c:otherwise>
                    </c:choose>
                </form:form>
                <c:if test="${membership.rights == 'MODERATOR'}">
                    <button type="button" class="blue-btn"
                            onclick="location.href='${pageContext.request.contextPath}/group/edit?group-id=${groupDto.id}';">
                        <span class="fa-solid fa-pen-to-square"></span> <spring:message code="label.account.edit"/>
                    </button>
                </c:if>
            </div>
        </div>
        <div class="dual">
            <div class="to-gradient container">
                <h4 class="light"><spring:message code="label.group.info"/></h4>
                <p><strong><spring:message code="group.creation.date"/>:</strong> <fmt:formatDate
                        value="${groupDto.creationDate}" pattern="dd/MM/yyyy"/></p>
                <spring:url value="/account" var="accountGetUrl">
                    <spring:param name="account-id" value="${groupDto.creator.id}"/>
                </spring:url>
                <p><strong><spring:message code="group.creator"/>:</strong>
                    <a href="${accountGetUrl}">
                        ${groupDto.creator.firstName} ${groupDto.creator.middleName} ${groupDto.creator.lastName}
                    </a>
                </p>
                <button type="button" class="blue-btn"
                        onclick="location.href='${pageContext.request.contextPath}/group/members?group-id=${groupDto.id}&status=accepted'">
                    <spring:message code="btn.members"/>
                </button>
                <c:if test="${membership.rights == 'MODERATOR'}">
                    <button type="button" class="blue-btn"
                            onclick="location.href='${pageContext.request.contextPath}/group/requests?group-id=${groupDto.id}'">
                        <spring:message code="btn.join.requests"/>
                    </button>
                </c:if>
            </div>
            <div class="wall">
                <c:choose>
                    <c:when test="${membership.rights == 'COMMON' || membership.rights == 'MODERATOR'}">
                        <div class="container">
                            <h4 class="light"><spring:message code="label.post.add"/>:</h4>
                                <%--@elvariable id="GroupMessage" type="com.getjavajob.ivanaham.domain.GroupMessage"--%>
                            <form:form method="POST" enctype="multipart/form-data" modelAttribute="groupMessage"
                                       action="${pageContext.request.contextPath}/group/messages">
                                <form:hidden value="${groupDto.id}" path="recipient.id"/>
                                <spring:message code="label.post.placeholder" var="postPlaceholder"/>
                                <form:textarea required="true" name="post-msg" rows="4" cols="50"
                                               placeholder="${postPlaceholder}" path="text"/>
                                <label for="attached-image"><span class="fa-solid fa-paperclip"></span> <spring:message
                                        code="label.post.attach.image"/></label>
                                <form:input accept="image/png, image/jpeg" type="file" id="attached-image"
                                            name="attached-image" path="attachedImage.imageBytes"/>
                                <form:hidden name="file-name" value="" path="attachedImage.name"/>
                                <spring:message code="label.post.send" var="post"/>
                                <input class="blue-btn" type="submit" value="${post}">
                            </form:form>
                        </div>
                        <div class="container">
                            <h4 class="light"><spring:message code="label.posts"/></h4>
                            <hr>
                            <p class="records-empty" hidden><spring:message code="label.posts.empty.msg"/></p>
                            <div class="records-container" about="${groupDto.id}"></div>
                            <ul class="pages"></ul>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="container">
                            <h4 class="light"><spring:message code="label.posts"/></h4>
                            <hr>
                            <p class="less-visible"><em><spring:message code="label.posts.not.member"/></em></p>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
</body>
</html>