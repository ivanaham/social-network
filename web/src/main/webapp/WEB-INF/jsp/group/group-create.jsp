<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="header.group.create"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/form-manager.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <c:if test="${pageContext.response.locale.toString().startsWith('ru')}">
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/localization/messages_ru.js"></script>
    </c:if>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <div class="container edit-block">
        <button type="button" name="back"
                onclick="location.href='${pageContext.request.contextPath}/account/groups?account-id=${currentUser.id}&status=accepted';">
            <span class="fa-solid fa-chevron-left"></span>
        </button>
        <h2 class="light edit-header"><spring:message code="btn.group.create"/></h2>

        <c:choose>
            <c:when test="${message eq 'unique'}">
                <div class="alert-error"><spring:message code="message.group.edit.error.unique"/></div>
            </c:when>
            <c:when test="${message eq 'max-file-size'}">
                <div class="alert-error"><spring:message code="label.register.error.max.file.size"/></div>
            </c:when>
        </c:choose>

        <spring:url value="/group" var="groupPostUrl">
            <spring:param name="creator-id" value="${currentUser.id}"/>
        </spring:url>
        <form:form id="edit-form" action="${groupPostUrl}" enctype="multipart/form-data" method="POST"
                   modelAttribute="groupDto">
            <fieldset>
                <legend class="light"><spring:message code="label.group.info"/></legend>
                <hr>
                <div>
                    <div class="common">
                        <spring:message code="group.name" var="name"/>
                        <label for="name">${name}:<strong>*</strong> </label>
                        <form:input class="input-field" name="name" id="name" placeholder="${name}" required="required"
                                    path="name"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <label for="description"><spring:message code="group.desc"/>: </label>
                        <spring:message code="group.desc.placeholder" var="descPlaceholder"/>
                        <form:textarea class="input-field" rows="5" name="description"
                                       placeholder="${descPlaceholder}" id="description" path="description"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <label for="upload-image"><spring:message code="label.profile.image"/>: </label>
                        <div class="input-field file-upload">
                            <label class="placeholder upload-text" for="upload-image">
                                <spring:message code="label.browse"/>
                            </label>
                        </div>
                    </div>
                </div>
                <form:input accept="image/png, image/jpeg" id="upload-image" name="profile-image" type="file"
                            path="profileImage.imageBytes"/>
                <form:hidden name="file-name" value="" path="profileImage.name"/>
            </fieldset>
            <div>
                <spring:message code="btn.save" var="save"/>
                <input class="blue-btn dialog-open" type="button" value="${save}">
                <input class="blue-btn submit" hidden type="submit">
            </div>
        </form:form>
        <spring:message code="dialog.title.group.create" var="title"/>
        <div class="dialog-form" title="${title}">
            <p><spring:message code="dialog.text.group.create"/></p>
        </div>
    </div>
</div>
</body>
</html>