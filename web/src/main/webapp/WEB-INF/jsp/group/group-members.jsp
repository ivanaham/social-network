<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>
<c:if test="${membership != null}">
    <jsp:useBean id="membership" scope="request" type="com.getjavajob.ivanaham.domain.Membership"/>
</c:if>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="group.members"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="group-members-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="content container">
        <h3 class="light">${groupDto.name} <spring:message code="group.members"/></h3>
        <hr>
        <p class="rights" hidden>${membership.rights}</p>
        <p class="currentUserId" hidden>${currentUser.id}</p>
        <h4 class="req-header light"><spring:message code="header.moderators"/></h4>
        <p class="moderator-empty" hidden><spring:message code="group.moderators.empty"/></p>
        <p class="email-label" hidden><spring:message code="label.email"/></p>
        <p class="register-date-label" hidden><spring:message code="label.register.date"/></p>
        <p class="message-label" hidden><spring:message code="label.friend.message"/></p>
        <div class="moderator-container" about="${groupId}"></div>
        <ul class="pages-moderator"></ul>

        <h4 class="req-header light"><spring:message code="header.common.members"/></h4>
        <p class="common-empty" hidden><spring:message code="group.members.empty"/></p>
        <p class="rights-label" hidden><spring:message code="group.members.change.to.moderator"/></p>
        <div class="common-container" about="${groupId}"></div>
        <ul class="pages-common"></ul>
    </div>
</body>
</html>