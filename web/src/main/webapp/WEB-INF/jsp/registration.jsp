<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <meta charset="UTF-8">
    <title><spring:message code="label.register"/></title>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/phone-box-manager.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/form-manager.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <c:if test="${pageContext.response.locale.toString().startsWith('ru')}">
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/localization/messages_ru.js"></script>
        <script type="text/javascript"
                src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/i18n/jquery-ui-i18n.min.js"></script>
    </c:if>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="flex-center">
<div class="dropdown">
    <button class="dropbtn"><spring:message code="label.language.change"/></button>
    <div class="dropdown-content">
        <a href="?locale=en"><spring:message code="label.language.en"/></a>
        <a href="?locale=ru"><spring:message code="label.language.ru"/></a>
    </div>
</div>
<div class="reg container">
    <h2 class="light text-center"><spring:message code="label.register"/></h2>

    <c:choose>
        <c:when test="${message eq 'max-file-size'}">
            <div class="alert-error"><spring:message code="label.register.error.max.file.size"/></div>
        </c:when>
        <c:otherwise>
            <div class="alert-error">${message}</div>
        </c:otherwise>
    </c:choose>

    <spring:url value="/account" var="registerUrl"/>
    <form:form id="reg-form" class="reg-form" action="${registerUrl}"
               enctype="multipart/form-data" method="POST" modelAttribute="accountDto">
        <div class="inputs">
            <div class="left">
                <h3 class="light"><spring:message code="label.personal"/></h3>
                <hr>
                <spring:message code="label.first.name" var="firstName"/>
                <form:input class="reg-field" name="first-name" placeholder="${firstName}*" required="required"
                            path="firstName"/>
                <spring:message code="label.middle.name" var="middleName"/>
                <form:input class="reg-field" name="middle-name" placeholder="${middleName}" path="middleName"/>
                <spring:message code="label.last.name" var="lastName"/>
                <form:input class="reg-field" name="last-name" placeholder="${lastName}*" required="required"
                            path="lastName"/>
                <spring:message code="label.birthdate.placeholder" var="birthdate"/>
                <form:input class="reg-field" placeholder="${birthdate}" id="datepicker" name="birth-date"
                            path="birthdate"/>
                <h3 class="light"><spring:message code="label.other"/></h3>
                <hr>

                <spring:message code="label.register.error.max.file.size" var="maxFileSize"/>
                <input hidden id="max-file-size" value="${maxFileSize}">
                <div class="reg-field reg-file-upload">
                    <form:label class="placeholder upload-text" for="upload-image" path="profileImage.imageBytes">
                        <spring:message code="label.browse"/>
                    </form:label>
                </div>
                <form:input accept="image/png, image/jpeg" id="upload-image" type="file"
                            path="profileImage.imageBytes"/>
                <form:hidden name="file-name" value="" path="profileImage.name"/>

                <spring:message code="label.additional" var="additional"/>
                <form:input class="reg-field" name="additional" placeholder="${additional}" path="additional"/>
                <spring:message code="label.password" var="password"/>
                <form:password minlength="6" class="reg-field" name="password" placeholder="${password}*"
                               required="required" path="password"/>
            </div>
            <div class="right">
                <h3 class="light"><spring:message code="label.contacts"/></h3>
                <hr>
                <spring:message code="label.email" var="email"/>
                <form:input class="reg-field" name="email" placeholder="${email}*" required="required" type="email"
                            path="email"/>

                <div class="reg-phone-box">
                    <spring:message code="label.phone.number" var="phone"/>
                    <input maxlength="15" class="phone-left reg-field" name="phone-setter" placeholder="${phone}"
                           type="tel">
                    <select name="type-setter" id="type" class="phone-right reg-field">
                        <option value="null" selected disabled><spring:message code="phone.type"/></option>
                        <option value="HOME"><spring:message code="label.phone.type.home"/></option>
                        <option value="WORK"><spring:message code="label.phone.type.work"/></option>
                    </select>
                    <button type="button" id="reg-add" class="reg-field phone-btn">
                        <span class="fa-solid fa-circle-plus"></span>
                    </button>
                </div>

                <spring:message code="label.skype" var="skype"/>
                <form:input class="reg-field" name="skype" placeholder="${skype}" path="skype"/>
                <spring:message code="label.icq" var="icq"/>
                <form:input class="reg-field" name="icq" placeholder="${icq}" type="number" path="icq"/>
                <spring:message code="label.home.address" var="homeAddress"/>
                <form:input class="reg-field" name="home-address" placeholder="${homeAddress}" path="homeAddress"/>
                <spring:message code="label.work.address" var="workAddress"/>
                <form:input class="reg-field" name="work-address" placeholder="${workAddress}" path="workAddress"/>
            </div>
        </div>
        <div class="flex-spaced">
            <spring:message code="label.submit" var="submit"/>
            <a href="${pageContext.request.contextPath}/login"><spring:message code="label.login"/></a>
            <input class="float-right btn" type="submit" value="${submit}">
        </div>
    </form:form>
</div>
</body>
</html>