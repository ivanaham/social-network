<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="header.not.found"/></title>
    <meta charset="UTF-8">
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="error-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<section class="error-content">
    <h1 class="error-sign"><spring:message code="label.not.found"/></h1>
</section>
</body>
</html>