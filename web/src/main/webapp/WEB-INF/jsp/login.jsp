<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <meta charset="UTF-8">
    <title><spring:message code="label.login"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="flex-center">
<div class="dropdown">
    <button class="dropbtn"><spring:message code="label.language.change"/></button>
    <div class="dropdown-content">
        <a href="?locale=en"><spring:message code="label.language.en"/></a>
        <a href="?locale=ru"><spring:message code="label.language.ru"/></a>
    </div>
</div>
<div class="container login">
    <h2 class="light text-center"><spring:message code="label.login"/></h2>

    <c:choose>
        <c:when test="${isRegistered eq 'true'}">
            <div class="alert-success"><spring:message code="label.registration.success"/></div>
        </c:when>
        <c:when test="${param.message eq 'error'}">
            <div class="alert-error"><spring:message code="label.login.error"/></div>
        </c:when>
    </c:choose>

    <spring:url value="/login" var="loginUrl"/>
    <form:form id="login" method="post" action="${loginUrl}">
        <spring:message code="label.email" var="email"/>
        <input class="reg-field" type="email" name="email" placeholder="${email}" required>
        <spring:message code="label.password" var="password"/>
        <input class="reg-field" type="password" name="password" placeholder="${password}" required>
        <label class="checkbox">
            <input type="checkbox" name="remember-me"><spring:message code="label.remember"/>
        </label>
        <div class="flex-spaced">
            <a href="${pageContext.request.contextPath}/registration"><spring:message code="label.register"/></a>
            <spring:message code="label.enter" var="submit"/>
            <input class="btn" type="submit" value="${submit}">
        </div>
    </form:form>
</div>
</body>
</html>