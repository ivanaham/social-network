<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="search"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="search-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="content container">
        <h3 class="light"><spring:message code="search.results"/>: ${requestScope['searchRequest']}</h3>
        <hr>
        <h4 class="light"><spring:message code="accounts"/></h4>
        <p class="accounts-empty" hidden><spring:message code="search.accounts.empty"/></p>
        <p class="email-label" hidden><spring:message code="label.email"/></p>
        <p class="register-date-label" hidden><spring:message code="label.register.date"/></p>
        <div class="accounts-container" about="${requestScope['searchRequest']}"></div>
        <ul class="pages-accounts"></ul>

        <h4 class="light"><spring:message code="groups"/></h4>
        <p class="groups-empty" hidden><spring:message code="search.groups.empty"/></p>
        <div class="groups-container" about="${requestScope['searchRequest']}"></div>
        <ul class="pages-groups"></ul>
    </div>
</div>
</body>
</html>