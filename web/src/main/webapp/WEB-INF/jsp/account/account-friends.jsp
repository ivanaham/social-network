<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="label.my.friends"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="account-friends-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="content container">
        <h3 class="light"><spring:message code="label.my.friends"/></h3>
        <p class="records-empty" hidden><spring:message code="label.friends.empty"/></p>
        <p class="email-label" hidden><spring:message code="label.email"/></p>
        <p class="register-date-label" hidden><spring:message code="label.register.date"/></p>
        <p class="message-label" hidden><spring:message code="label.friend.message"/></p>
        <p class="remove-label" hidden><spring:message code="btn.remove"/></p>
        <div class="records-container" about="${currentUser.id}"></div>
        <ul class="pages"></ul>
    </div>
</div>
</body>
</html>