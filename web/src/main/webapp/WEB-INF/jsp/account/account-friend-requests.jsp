<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="label.my.friend.req"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="account-friends-requests-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="content container">
        <h3 class="light"><spring:message code="label.my.friend.req"/></h3>
        <hr>
        <h4 class="req-header light"><spring:message code="header.requests.incoming"/></h4>
        <p class="incoming-empty" hidden><spring:message code="label.requests.incoming.empty"/></p>
        <p class="email-label" hidden><spring:message code="label.email"/></p>
        <p class="register-date-label" hidden><spring:message code="label.register.date"/></p>
        <p class="accept-label" hidden><spring:message code="btn.accept"/></p>
        <p class="decline-label" hidden><spring:message code="btn.decline"/></p>
        <div class="incoming-container" about="${currentUser.id}"></div>
        <ul class="pages-incoming"></ul>

        <h4 class="req-header light"><spring:message code="header.requests.outgoing"/></h4>
        <p class="outgoing-empty" hidden><spring:message code="label.requests.outgoing.empty"/></p>
        <p class="cancel-label" hidden><spring:message code="btn.cancel"/></p>
        <div class="outgoing-container" about="${currentUser.id}"></div>
        <ul class="pages-outgoing"></ul>
    </div>
</div>
</body>
</html>