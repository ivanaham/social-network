<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="label.edit.profile"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/phone-box-manager.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/form-manager.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <c:if test="${pageContext.response.locale.toString().startsWith('ru')}">
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/localization/messages_ru.js"></script>
        <script type="text/javascript"
                src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/i18n/jquery-ui-i18n.min.js"></script>
    </c:if>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="account-edit-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main edit">
    <div class="container edit-block">
        <div class="block-header">
            <div class="block-header-left">
                <button type="button" name="back"
                        onclick="location.href='${pageContext.request.contextPath}/account?account-id=${updatableAccountDto.id}';">
                    <span class="fa-solid fa-chevron-left"></span>
                </button>
                <h2 class="light edit-header"><spring:message code="label.edit.profile"/></h2>
            </div>
            <div class="block-header-right">
                <spring:url value="/account/export" var="exportUrl">
                    <spring:param name="account-id" value="${updatableAccountDto.id}"/>
                </spring:url>
                <a href="${exportUrl}"
                   download="account" class="export-btn"><spring:message code="export.xml"/></a>
                <div class="import-btn button">
                    <spring:url value="/account/import" var="importUrl">
                        <spring:param name="account-id" value="${updatableAccountDto.id}"/>
                    </spring:url>
                    <form:form method="POST" enctype="multipart/form-data" action="${importUrl}">
                        <input hidden name="_method" value="PUT">
                        <label for="import"><spring:message code="import.xml"/></label>
                        <input type="file" name="account" accept=".xml" hidden id="import">
                        <input type="submit" id="import-submit" hidden>
                    </form:form>
                </div>
            </div>
        </div>

        <c:choose>
            <c:when test="${message eq 'success'}">
                <div class="alert-success"><spring:message code="message.account.update.success"/></div>
            </c:when>
            <c:when test="${message eq 'max-file-size'}">
                <div class="alert-error"><spring:message code="label.register.error.max.file.size"/></div>
            </c:when>
            <c:otherwise>
                <div class="alert-error">${message}</div>
            </c:otherwise>
        </c:choose>

        <spring:url value="/account" var="accountPutUrl">
            <spring:param name="account-id" value="${updatableAccountDto.id}"/>
        </spring:url>
        <form:form id="edit-form" class="account-form" action="${accountPutUrl}" enctype="multipart/form-data"
                   method="POST" modelAttribute="updatableAccountDto">
            <input hidden name="_method" value="PUT">
            <form:hidden path="id" value="${updatableAccountDto.id}"/>
            <fieldset>
                <legend class="light"><spring:message code="label.personal"/></legend>
                <hr>
                <div>
                    <div class="common">
                        <spring:message code="label.first.name" var="firstName"/>
                        <label for="first-name">${firstName}:<strong>*</strong> </label>
                        <form:input class="input-field" name="first-name" id="first-name" placeholder="${firstName}"
                                    required="required" path="firstName"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.middle.name" var="middleName"/>
                        <label for="middle-name">${middleName}: </label>
                        <form:input class="input-field" name="middle-name" id="middle-name" placeholder="${middleName}"
                                    path="middleName"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.last.name" var="lastName"/>
                        <label for="last-name">${lastName}:<strong>*</strong> </label>
                        <form:input class="input-field" name="last-name" id="last-name" placeholder="${lastName}"
                                    required="required" path="lastName"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <label for="datepicker"><spring:message code="label.birth.date"/>: </label>
                        <spring:message code="label.birthdate.placeholder" var="birthdate"/>
                        <form:input class="input-field" placeholder="${birthdate}" name="birth-date"
                                    id="datepicker" path="birthdate"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.additional" var="additional"/>
                        <label for="additional">${additional}: </label>
                        <form:input class="input-field" name="additional" id="additional" placeholder="${additional}"
                                    path="additional"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <label for="upload-image"><spring:message code="label.profile.image"/>: </label>
                        <div class="input-field file-upload">
                            <label class="placeholder upload-text" for="upload-image">
                                <spring:message code="label.browse"/>
                            </label>
                        </div>
                    </div>
                </div>
                <form:input accept="image/png, image/jpeg" id="upload-image" name="profile-image" type="file"
                            path="profileImage.imageBytes"/>
                <form:hidden name="file-name" value="" path="profileImage.name"/>
            </fieldset>
            <fieldset>
                <legend class="light"><spring:message code="label.contacts"/></legend>
                <hr>
                <div>
                    <div class="common">
                        <spring:message code="label.email" var="email"/>
                        <label for="email">${email}:<strong>*</strong> </label>
                        <form:input class="input-field" name="email" id="email" placeholder="${email}*"
                                    required="required" type="email" path="email"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.phone.number" var="phoneNumber"/>
                        <label for="phone-setter">${phoneNumber}: </label>
                        <div class="phone-box">
                            <input maxlength="15" class="phone-left input-field" name="phone-setter"
                                   id="phone-setter" placeholder="${phoneNumber}" type="tel"/>
                            <select name="type-setter" id="type" class="phone-right input-field">
                                <option value="null" selected disabled><spring:message code="phone.type"/></option>
                                <option value="HOME"><spring:message code="label.phone.type.home"/></option>
                                <option value="WORK"><spring:message code="label.phone.type.work"/></option>
                            </select>
                            <button type="button" id="edit-add" class="input-field phone-btn">
                                <span class="fa-solid fa-circle-plus"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <c:forEach items="${updatableAccountDto.phones}" var="phone" varStatus="status">
                    <div>
                        <div class="phone-box">
                            <input readonly maxlength="15" class="phone-left input-field" name="numbers"
                                   placeholder="${phoneNumber}" type="tel" value="${phone.number}"/>
                            <select name="types" class="phone-right input-field">
                                <c:choose>
                                    <c:when test="${phone.type == 'HOME'}">
                                        <option value="HOME"><spring:message code="label.phone.type.home"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="WORK"><spring:message code="label.phone.type.work"/></option>
                                    </c:otherwise>
                                </c:choose>
                            </select>
                            <input name="isDeleted" hidden value="false"/>
                            <button type="button" class="remove input-field phone-btn">
                                <span class="fa-solid fa-circle-minus"></span>
                            </button>
                        </div>
                    </div>
                </c:forEach>
                <div>
                    <div class="common">
                        <spring:message code="label.skype" var="skype"/>
                        <label for="skype">${skype}: </label>
                        <form:input class="input-field" name="skype" id="skype" placeholder="${skype}"
                                    path="skype"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.icq" var="icq"/>
                        <label for="icq">${icq}: </label>
                        <form:input class="input-field" name="icq" id="icq" placeholder="${icq}" type="number"
                                    path="icq"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.home.address" var="homeAddress"/>
                        <label for="home-address">${homeAddress}: </label>
                        <form:input class="input-field" name="home-address" id="home-address"
                                    placeholder="${homeAddress}" path="homeAddress"/>
                    </div>
                </div>
                <div>
                    <div class="common">
                        <spring:message code="label.work.address" var="workAddress"/>
                        <label for="work-address">${workAddress}: </label>
                        <form:input class="input-field" name="work-address" id="work-address"
                                    placeholder="${workAddress}" path="workAddress"/>
                    </div>
                </div>
            </fieldset>
            <sec:authorize access="hasRole('ADMIN')">
                <fieldset class="roles">
                    <legend class="light"><spring:message code="header.account.role"/></legend>
                    <hr>
                    <div>
                        <div class="common">
                            <label for="role"><spring:message code="label.role"/>:</label>
                            <select name="role" id="role" class="input-field">
                                <spring:message code="label.role.user" var="roleUser"/>
                                <spring:message code="label.role.admin" var="roleAdmin"/>
                                <option value="ADMIN">${roleAdmin}</option>
                                <option value="USER">${roleUser}</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
                <c:forEach items="${updatableAccountDto.roles}" var="role">
                    <div class="role-group">
                        <input class="role-id" hidden value="${role.id}">
                        <input class="role-name" hidden value="${role.name}">
                        <input class="role-deleted" hidden value="false"/>
                    </div>
                </c:forEach>
            </sec:authorize>
            <div>
                <spring:message code="btn.confirm" var="save"/>
                <input class="blue-btn save dialog-open" type="button" value="${save}">
                <input class="blue-btn save submit" hidden type="submit">
            </div>
        </form:form>
        <spring:message code="dialog.title.account.edit" var="generalTitle"/>
        <div class="dialog-form" title="${generalTitle}">
            <p><spring:message code="dialog.text.default"/></p>
        </div>
    </div>

    <c:if test="${updatableAccountDto.id == currentUser.id}">
        <div class="container pass-msg edit-block">
            <h2 class="light edit-header"><spring:message code="header.reset.password"/></h2>

            <c:if test="${passwordMessage eq 'success'}">
                <div class="alert-success"><spring:message code="message.password.reset.success"/></div>
            </c:if>

            <spring:url value="/account" var="accountPatchUrl"/>
            <form:form id="password-form" action="${accountPatchUrl}" method="POST">
                <input hidden name="_method" value="PATCH">
                <fieldset>
                    <spring:message code="label.password" var="password"/>
                    <div>
                        <div class="common">
                            <label for="password"><spring:message code="label.password.new"/>: </label>
                            <input minlength="6" class="input-field" name="password" placeholder="${password}"
                                   type="password" id="password" required/>
                        </div>
                    </div>
                    <div>
                        <div class="common">
                            <label for="confirm-password"><spring:message code="label.password.confirm"/>: </label>
                            <input minlength="6" class="input-field" name="confirm-password" placeholder="${password}"
                                   type="password" required id="confirm-password"/>
                        </div>
                    </div>
                </fieldset>
                <div>
                    <spring:message code="btn.confirm" var="confirm"/>
                    <input class="blue-btn password-dialog-open" type="button" value="${confirm}">
                    <input class="blue-btn password-submit" hidden type="submit">
                </div>
            </form:form>
            <spring:message code="dialog.title.account.password.edit" var="passwordTitle"/>
            <div class="dialog-password-form" title="${passwordTitle}">
                <p><spring:message code="dialog.text.account.password.edit"/></p>
            </div>
        </div>
    </c:if>
</div>
</body>
</html>