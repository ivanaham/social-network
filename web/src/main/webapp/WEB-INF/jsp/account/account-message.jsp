<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>
<jsp:useBean id="recordsPerPageInDirect" scope="request" type="java.lang.Integer"/>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title><spring:message code="label.messages"/></title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.5.0/sockjs.js"
            integrity="sha512-lyIq9fRcCeSCXhp41XC/250UBmypAHV8KW+AhLcSEIksWHBfhzub6XXwDe67wTpOG8zrO2NAU/TYmEaCW+aQSg=="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"
            integrity="sha512-iKDtgDyTHjAitUDdLljGhenhPwrbBfqTKWO1mkhSFH3A7blITC9MhYon6SjnMhp4o0rADGw9yAC6EW4t5a4K3g=="
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/messaging.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="account-messages-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <aside>
        <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
        <div class="buddies-list container">
            <p class="records-empty" hidden><spring:message code="label.conversations.empty.page"/></p>
            <p class="account-to-id" hidden>${param['account-to-id']}</p>
            <div class="buddies-container" about="${currentUser.id}"></div>
            <ul class="pages"></ul>
        </div>
    </aside>
    <p class="active-buddy-id" hidden></p>
    <p class="chat-with-label" hidden><spring:message code="header.chat.with"/></p>
    <p class="load-more-label" hidden><spring:message code="btn.load.more"/></p>
    <p class="direct-records" hidden>${recordsPerPageInDirect}</p>
    <p class="current-account-id" hidden>${currentUser.id}</p>
    <p class="message-placeholder" hidden><spring:message code="label.message.placeholder"/></p>
    <p class="attach-label" hidden><spring:message code="label.post.attach.image"/></p>
    <p class="send-label" hidden><spring:message code="btn.message.send"/></p>
    <p class="messages-empty" hidden><spring:message code="label.message.empty"/></p>
    <div class="content container">
        <h2 class="light edit-header"><spring:message code="header.chat"/></h2>
        <p class="less-visible"><em><spring:message code="label.conversations.empty"/></em></p>
    </div>
</div>
</body>
</html>