<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="currentUser" property="principal"/>
<c:if test="${friendship != null}">
    <jsp:useBean id="friendship" scope="request" type="com.getjavajob.ivanaham.domain.Friendship"/>
</c:if>

<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
<head>
    <title>
        <c:choose>
            <c:when test="${accountDto.id == currentUser.id}"><spring:message code="label.my.profile"/></c:when>
            <c:otherwise>${accountDto.firstName} ${accountDto.lastName}</c:otherwise>
        </c:choose>
    </title>
    <meta charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <script src="https://kit.fontawesome.com/f31568e018.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous">
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/all-inner-pages-default.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/default.css"/>
</head>
<body class="account-page">
<jsp:include page="/WEB-INF/jsp/includes/header.jsp"/>
<div class="main">
    <jsp:include page="/WEB-INF/jsp/includes/navigation.jsp"/>
    <div class="central">
        <div class="intro container">
            <div class="intro-left">
                <div class="profile-image">
                    <c:choose>
                        <c:when test="${fn:length(accountDto.profileImage.imageBytes) != 0}">
                            <spring:url value="/images/get" var="profileImageUrl">
                                <spring:param name="account-id" value="${accountDto.id}"/>
                                <spring:param name="img-type" value="account"/>
                            </spring:url>
                            <img alt="profile-image" src="${profileImageUrl}"/>
                        </c:when>
                        <c:otherwise>
                            <img alt="default-account-image"
                                 src="${pageContext.request.contextPath}/resources/images/icons/default-account-image.svg"/>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="intro-info">
                    <h4>${accountDto.firstName} ${accountDto.middleName} ${accountDto.lastName}</h4>
                    <p><strong><spring:message code="label.email"/>:</strong> ${accountDto.email}</p>
                    <p><strong><spring:message code="label.register.date"/>:</strong> <fmt:formatDate
                            value="${accountDto.registerDate}" pattern="dd/MM/yyyy"/></p>
                </div>
            </div>
            <div class="intro-right actions">
                <c:if test="${accountDto.id != currentUser.id}">
                    <c:choose>
                        <c:when test="${friendship == null ||  friendship.status == 'DECLINED'}">
                            <spring:url value="/account/friends" var="sendRequestUrl">
                                <spring:param name="action-maker-id" value="${currentUser.id}"/>
                                <spring:param name="friend-id" value="${accountDto.id}"/>
                                <spring:param name="action" value="send"/>
                            </spring:url>
                            <form:form method="post" action="${sendRequestUrl}">
                                <button type="submit" class="blue-btn">
                                    <spring:message code="label.friend.req.send"/>
                                </button>
                            </form:form>
                        </c:when>
                        <c:when test="${friendship.status == 'PENDING' && friendship.lastActedAccount.id == currentUser.id}">
                            <spring:url value="/account/friends" var="cancelRequestUrl">
                                <spring:param name="action-maker-id" value="${currentUser.id}"/>
                                <spring:param name="friend-id" value="${accountDto.id}"/>
                                <spring:param name="action" value="cancel"/>
                            </spring:url>
                            <form:form method="post" action="${cancelRequestUrl}">
                                <button type="submit" class="blue-btn">
                                    <spring:message code="label.friend.req.cancel"/>
                                </button>
                            </form:form>
                        </c:when>
                        <c:when test="${friendship.status == 'PENDING' && friendship.lastActedAccount.id == accountDto.id}">
                            <spring:url value="/account/friends" var="acceptRequestUrl">
                                <spring:param name="action-maker-id" value="${currentUser.id}"/>
                                <spring:param name="friend-id" value="${accountDto.id}"/>
                                <spring:param name="action" value="accept"/>
                            </spring:url>
                            <form:form method="post" action="${acceptRequestUrl}">
                                <button type="submit" class="blue-btn">
                                    <spring:message code="label.friend.req.accept"/>
                                </button>
                            </form:form>
                            <spring:url value="/account/friends" var="declineRequestUrl">
                                <spring:param name="action-maker-id" value="${currentUser.id}"/>
                                <spring:param name="friend-id" value="${accountDto.id}"/>
                                <spring:param name="action" value="decline"/>
                            </spring:url>
                            <form:form method="post" action="${declineRequestUrl}">
                                <button type="submit" class="blue-btn">
                                    <spring:message code="label.friend.req.decline"/>
                                </button>
                            </form:form>
                        </c:when>
                        <c:otherwise>
                            <spring:url value="/account/friends" var="deleteRequestUrl">
                                <spring:param name="action-maker-id" value="${currentUser.id}"/>
                                <spring:param name="friend-id" value="${accountDto.id}"/>
                                <spring:param name="action" value="delete"/>
                            </spring:url>
                            <form:form method="post" action="${deleteRequestUrl}">
                                <button type="submit" class="blue-btn">
                                    <spring:message code="label.friend.req.remove"/>
                                </button>
                            </form:form>
                        </c:otherwise>
                    </c:choose>
                </c:if>
                <c:if test="${accountDto.id != currentUser.id}">
                    <button type="button" class="blue-btn"
                            onclick="location.href='${pageContext.request.contextPath}/account/messages?account-to-id=${accountDto.id}';">
                        <spring:message code="label.friend.message"/>
                    </button>
                </c:if>
                <sec:authorize access="hasRole('ADMIN') or ${accountDto.id} eq authentication.principal.id">
                    <button type="button" class="blue-btn"
                            onclick="location.href='${pageContext.request.contextPath}/account/edit?account-id=${accountDto.id}';">
                        <span class="fa-solid fa-pen-to-square"></span> <spring:message code="label.account.edit"/>
                    </button>
                </sec:authorize>
            </div>
        </div>
        <div class="dual">
            <div class="to-gradient container">
                <h4 class="light"><spring:message code="label.account.info"/></h4>
                <c:if test="${accountDto.birthdate != null}">
                    <p><strong><spring:message code="label.birth.date"/>:</strong> <fmt:formatDate
                            value="${accountDto.birthdate}" pattern="dd/MM/yyyy"/></p>
                </c:if>
                <c:if test="${accountDto.homeAddress != null}">
                    <p><strong><spring:message code="label.home.address"/>:</strong> ${accountDto.homeAddress}</p>
                </c:if>
                <c:if test="${accountDto.workAddress != null}">
                    <p><strong><spring:message code="label.work.address"/>:</strong> ${accountDto.workAddress}</p>
                </c:if>
                <c:if test="${accountDto.icq != null}">
                    <p><strong><spring:message code="label.icq"/>:</strong> ${accountDto.icq}</p>
                </c:if>
                <c:if test="${accountDto.skype != null}">
                    <p><strong><spring:message code="label.skype"/>:</strong> ${accountDto.skype}</p>
                </c:if>
                <c:if test="${accountDto.additional != null}">
                    <p><strong><spring:message code="label.additional"/>:</strong> ${accountDto.additional}</p>
                </c:if>
                <c:if test="${!accountDto.phones.isEmpty()}">
                    <h3 class="light"><spring:message code="label.account.phones"/></h3>
                    <c:forEach items="${accountDto.phones}" var="phone">
                        <c:choose>
                            <c:when test="${phone.type == 'HOME'}">
                                <spring:message code="label.phone.type.home" var="phoneType"/>
                            </c:when>
                            <c:otherwise>
                                <spring:message code="label.phone.type.work" var="phoneType"/>
                            </c:otherwise>
                        </c:choose>
                        <p><strong>${phoneType} <spring:message code="label.number"/>:</strong> ${phone.number}</p>
                    </c:forEach>
                </c:if>
                <sec:authorize access="hasRole('ADMIN') or ${accountDto.id} eq authentication.principal.id">
                    <button type="button" class="blue-btn"
                            onclick="location.href='${pageContext.request.contextPath}/account/edit?account-id=${accountDto.id}';">
                        <span class="fa-solid fa-pen-to-square"></span> <spring:message code="label.account.edit"/>
                    </button>
                </sec:authorize>
            </div>
            <div class="wall">
                <div class="container">
                    <h4 class="light"><spring:message code="label.post.add"/>:</h4>
                    <spring:url value="/account/messages" var="messageSendUrl">
                        <spring:param name="type" value="account"/>
                    </spring:url>
                    <%--@elvariable id="message" type="com.getjavajob.ivanaham.domain.AccountMessage"--%>
                    <form:form method="POST" enctype="multipart/form-data" modelAttribute="accountMessage"
                               action="${messageSendUrl}">
                        <form:hidden path="messageType" value="ACCOUNT"/>
                        <form:hidden value="${accountDto.id}" path="recipient.id"/>
                        <spring:message code="label.post.placeholder" var="postPlaceholder"/>
                        <form:textarea required="true" name="post-msg" rows="4" cols="50"
                                       placeholder="${postPlaceholder}" path="text"/>
                        <label for="attached-image"><span class="fa-solid fa-paperclip"></span> <spring:message
                                code="label.post.attach.image"/></label>
                        <form:input accept="image/png, image/jpeg" type="file" id="attached-image" name="attached-image"
                                    path="attachedImage.imageBytes"/>
                        <form:hidden name="file-name" value="" path="attachedImage.name"/>
                        <spring:message code="label.post.send" var="post"/>
                        <input class="blue-btn" type="submit" value="${post}">
                    </form:form>
                </div>
                <div class="container">
                    <h4 class="light"><spring:message code="label.posts"/></h4>
                    <hr>
                    <p class="records-empty" hidden><spring:message code="label.posts.empty.msg"/></p>
                    <div class="records-container" about="${accountDto.id}"></div>
                    <ul class="pages"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>