package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.RequestType.INCOMING;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FriendshipServiceTest {

    @Mock
    private FriendshipDao friendshipDao;

    @Mock
    private AccountDao accountDao;

    @InjectMocks
    private FriendshipService friendshipService;

    private Friendship getFriendship(RequestStatus status) {
        Friendship friendship = new Friendship();
        friendship.setStatus(status);
        return friendship;
    }

    @Test
    void testSendFriendRequestTrue_WhenFriendshipIsNull() {
        when(friendshipDao.findFriendship(2, 3)).thenReturn(null);
        friendshipService.sendFriendRequest(2, 3);
        verify(friendshipDao, times(1)).insert(getFriendship(PENDING));
    }

    @Test
    void testSendFriendRequestNotInserts_WhenFriendshipIsPending() {
        Friendship friendship = getFriendship(PENDING);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.sendFriendRequest(2, 3));
        verify(friendshipDao, never()).insert(friendship);
    }

    @Test
    void testSendFriendRequestNotInserts_WhenFriendshipIsAccepted() {
        Friendship friendship = getFriendship(ACCEPTED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.sendFriendRequest(2, 3));
        verify(friendshipDao, never()).insert(friendship);
    }

    @Test
    void testSendFriendRequestTrue_WhenFriendshipIsDeclined() {
        Friendship friendship = getFriendship(DECLINED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        friendshipService.sendFriendRequest(2, 3);
        verify(friendshipDao, times(1))
                .update(getFriendship(PENDING));
    }

    @Test
    void testAcceptFriendRequest_WhenFriendshipIsPending() {
        Friendship friendship = getFriendship(PENDING);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        friendshipService.acceptFriendRequest(2, 3);
        verify(friendshipDao, times(1)).update(getFriendship(ACCEPTED));
    }

    @Test
    void testAcceptFriendRequestFalse_WhenFriendshipIsAccepted() {
        Friendship friendship = getFriendship(ACCEPTED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.acceptFriendRequest(2, 3));
    }

    @Test
    void testAcceptFriendRequestFalse_WhenFriendshipIsDeclined() {
        Friendship friendship = getFriendship(DECLINED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.acceptFriendRequest(2, 3));
    }

    @Test
    void testAcceptFriendRequestFalse_WhenFriendshipIsNull() {
        when(friendshipDao.findFriendship(2, 3)).thenReturn(null);
        assertThrows(IllegalStateException.class, () -> friendshipService.acceptFriendRequest(2, 3));
    }

    @Test
    void testDeclineFriendRequestTrue_WhenFriendshipIsPending() {
        Friendship friendship = getFriendship(PENDING);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        friendshipService.declineFriendRequest(2, 3);
        verify(friendshipDao, times(1)).update(getFriendship(DECLINED));
    }

    @Test
    void testDeclineFriendRequestFalse_WhenFriendshipIsAccepted() {
        Friendship friendship = getFriendship(ACCEPTED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.declineFriendRequest(2, 3));
    }

    @Test
    void testDeclineFriendRequestThrows_WhenFriendshipIsDeclined() {
        Friendship friendship = getFriendship(DECLINED);
        when(friendshipDao.findFriendship(2, 3)).thenReturn(friendship);
        assertThrows(IllegalStateException.class, () -> friendshipService.declineFriendRequest(2, 3));
    }

    @Test
    void testDeclineFriendRequestFalse_WhenFriendshipIsNull() {
        when(friendshipDao.findFriendship(2, 3)).thenReturn(null);
        assertThrows(IllegalStateException.class, () -> friendshipService.declineFriendRequest(2, 3));
    }

    @Test
    void testDeleteFriendTrue() {
        when(friendshipDao.deleteFriendship(1, 2)).thenReturn(true);
        assertTrue(friendshipService.deleteFriend(1, 2));
    }

    @Test
    void testDeleteFriendFalse() {
        when(friendshipDao.deleteFriendship(1, 2)).thenReturn(false);
        assertFalse(friendshipService.deleteFriend(1, 2));
    }

    @Test
    void shouldGetFiendsListWhenFindFriendsWithStatus() {
        List<Friendship> friendships = new ArrayList<>(asList(
                getFriendship(ACCEPTED),
                getFriendship(ACCEPTED)
        ));
        when(friendshipDao.find(1, ACCEPTED, 2, 0)).thenReturn(friendships);
        assertEquals(friendships, friendshipService.find(1, ACCEPTED, 2, 0));
    }

    @Test
    void testFind() {
        List<Friendship> friendships = new ArrayList<>(asList(
                getFriendship(ACCEPTED),
                getFriendship(ACCEPTED)
        ));
        when(friendshipDao.find(1, ACCEPTED)).thenReturn(friendships);
        assertEquals(friendships, friendshipService.find(1, ACCEPTED));
    }

    @Test
    void shouldGetFiendsListWhenFindFriendsWithType() {
        List<Friendship> friendships = new ArrayList<>(asList(
                getFriendship(PENDING),
                getFriendship(PENDING)
        ));
        when(friendshipDao.findPending(1, INCOMING, 2, 0)).thenReturn(friendships);
        assertEquals(friendships, friendshipService.findPending(1, INCOMING, 2, 0));
    }


}