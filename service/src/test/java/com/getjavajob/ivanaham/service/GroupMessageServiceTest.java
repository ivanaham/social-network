package com.getjavajob.ivanaham.service;

import static java.time.LocalDateTime.of;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.GroupMessageDaoImpl;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.GroupMessage;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GroupMessageServiceTest {

    private static final LocalDateTime dummyDateTime = of(2022, 2, 1, 12, 20, 36);
    private static final GroupMessage dummyMsgA = new GroupMessage();
    private static final GroupMessage dummyMsgB = new GroupMessage();

    static {
        Account account = new Account();
        Group group = new Group();
        dummyMsgA.setId(1);
        dummyMsgA.setAuthor(account);
        dummyMsgA.setRecipient(group);
        dummyMsgA.setText("Hello!");
        dummyMsgA.setDateTimeSent(dummyDateTime);
        dummyMsgA.setId(2);
        dummyMsgB.setAuthor(account);
        dummyMsgB.setRecipient(group);
        dummyMsgB.setText("Hi");
        dummyMsgB.setDateTimeSent(dummyDateTime);
    }

    @Mock
    private GroupMessageDaoImpl groupMessageDao;

    @InjectMocks
    private GroupMessageService messageService;

    @Test
    void testFindById() {
        when(groupMessageDao.findById(dummyMsgA.getId())).thenReturn(dummyMsgA);
        assertEquals(dummyMsgA, messageService.findById(dummyMsgA.getId()));
    }

    @Test
    void testFindAll() {
        List<GroupMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(groupMessageDao.findAll()).thenReturn(messages);
        assertEquals(messages, messageService.findAll());
    }

    @Test
    void testFindByRecipientId() {
        List<GroupMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(groupMessageDao.findByRecipientId(2, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findByRecipientId(2, 3, 0));
    }

    @Test
    void testMessageCreate() {
        messageService.create(dummyMsgA);
        verify(groupMessageDao, times(1)).insert(dummyMsgA);
    }

    @Test
    void testDeleteById() {
        messageService.deleteById(dummyMsgA.getId());
        verify(groupMessageDao, times(1)).deleteById(dummyMsgA.getId());
    }

}