package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.Phone.Type.HOME;
import static com.getjavajob.ivanaham.domain.Phone.Type.WORK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.PhoneDaoImpl;
import com.getjavajob.ivanaham.domain.Phone;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PhoneServiceTest {

    private static final Set<Phone> phones = new HashSet<>();

    static {
        Phone currentPhone = new Phone();
        currentPhone.setType(HOME);
        currentPhone.setNumber("38093465783");
        phones.add(currentPhone);
        currentPhone = new Phone();
        currentPhone.setType(WORK);
        currentPhone.setNumber("38093123231");
        phones.add(currentPhone);
    }

    @Mock
    private PhoneDaoImpl phoneDao;

    @InjectMocks
    private PhoneService phoneService;

    @Test
    void testInsert() {
        phoneService.insert(phones);
        verify(phoneDao, times(1)).insert(phones);
    }

    @Test
    void testDeleteByAccountId_True() {
        when(phoneDao.deleteByAccountId(1)).thenReturn(1);
        assertTrue(phoneService.deleteByAccountId(1));
    }

    @Test
    void testDeleteByAccountId_False() {
        when(phoneDao.deleteByAccountId(1)).thenReturn(0);
        assertFalse(phoneService.deleteByAccountId(1));
    }

    @Test
    void testDeleteById() {
        phoneService.deleteById(1);
        verify(phoneDao, times(1)).deleteById(1);
    }

    @Test
    void testFindByAccountId() {
        when(phoneDao.findByAccountId(1)).thenReturn(phones);
        assertEquals(phones, phoneService.findByAccountId(1));
    }

}