package com.getjavajob.ivanaham.service;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.getjavajob.ivanaham.domain.Role;
import com.getjavajob.ivanaham.service.security.CurrentUser;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

class CurrentUserTest {

    @Test
    void testGetterAndSetterId() {
        CurrentUser currentUser = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUser.setId(10);
        assertEquals(10, currentUser.getId());
    }

    @Test
    void testGetterAndSetterRoles() {
        CurrentUser currentUser = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        Set<Role> roles = new HashSet<>();
        roles.add(new Role("ROLE_ADMIN"));
        currentUser.setRoles(roles);
        assertEquals(roles, currentUser.getRoles());
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualHashcode() {
        CurrentUser currentUserA = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        CurrentUser currentUserB = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUserA.setId(10);
        currentUserB.setId(10);
        assertTrue(currentUserA.equals(currentUserB));
        assertEquals(currentUserA.hashCode(), currentUserB.hashCode());
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsTrue() {
        CurrentUser currentUserA = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        CurrentUser currentUserB = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUserA.setId(10);
        currentUserB.setId(10);
        assertTrue(currentUserA.equals(currentUserB));
    }

    @SuppressWarnings({"EqualsWithItself", "SimplifiableAssertion"})
    @Test
    void testEqualsTrue_SameObject() {
        CurrentUser currentUser = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUser.setId(10);
        assertTrue(currentUser.equals(currentUser));
    }

    @SuppressWarnings({"ConstantConditions", "SimplifiableAssertion"})
    @Test
    void testEqualsFalse_Null() {
        CurrentUser currentUser = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUser.setId(10);
        assertFalse(currentUser.equals(null));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsFalse_SuperFieldIsDifferent() {
        CurrentUser currentUserA = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        CurrentUser currentUserB = new CurrentUser("bambino", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        assertFalse(currentUserA.equals(currentUserB));
    }

    @SuppressWarnings("SimplifiableAssertion")
    @Test
    void testEqualsFalse() {
        CurrentUser currentUserA = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        CurrentUser currentUserB = new CurrentUser("username", "password", true,
                true, true, true,
                singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        currentUserA.setId(10);
        assertFalse(currentUserA.equals(currentUserB));
    }

}