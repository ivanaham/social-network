package com.getjavajob.ivanaham.service;

import static java.sql.Date.valueOf;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.GroupDaoImpl;
import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GroupServiceTest {

    private static final Group group = new Group();

    static {
        group.setName("Beer");
        group.setCreationDate(valueOf("2022-06-06"));
        group.setCreator(new Account());
    }

    @Mock
    private GroupDaoImpl groupDao;

    @Mock
    private MembershipDao membershipDao;

    @InjectMocks
    private GroupService groupService;

    @Test
    void testGroupCreate() {
        groupService.create(group);
        verify(groupDao, times(1)).insert(group);
    }

    @Test
    void testGroupEditTrue() {
        groupService.edit(group);
        verify(groupDao, times(1)).update(group);
    }

    @Test
    void testGroupDelete() {
        groupService.delete(1);
        verify(groupDao, times(1)).deleteById(1);
    }

    @Test
    void testGroupFindById() {
        when(groupDao.findById(1)).thenReturn(group);
        assertEquals(group, groupService.findById(1));
    }

    @Test
    void testGroupFindAll() {
        List<Group> groups = asList(group, group, group);
        when(groupDao.findAll()).thenReturn(groups);
        assertEquals(groups, groupService.findAll());
    }

    @Test
    void testGroupFindByName() {
        List<Group> groups = singletonList(group);
        when(groupDao.findByName("bee", 1, 0)).thenReturn(groups);
        assertEquals(groups, groupService.findByName("bee", 1, 0));
    }

}