package com.getjavajob.ivanaham.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.AccountDaoImpl;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
class JpaUserDetailsServiceTest {

    @Mock
    private AccountDaoImpl accountDao;

    @InjectMocks
    private JpaUserDetailsService userDetailsService;

    @Test
    void testLoadUserByUsername() {
        Account account = new Account();
        account.setEmail("test@gmail.com");
        account.setPassword("123456");
        account.addRole(new Role("ROLE_USER"));
        when(accountDao.findByEmail(account.getEmail())).thenReturn(account);
        assertEquals(account.getEmail(), userDetailsService.loadUserByUsername(account.getEmail()).getUsername());
    }

    @Test
    void testLoadUserByUsername_UserNotExist() {
        Account account = new Account();
        account.setEmail("test@gmail.com");
        account.setPassword("123456");
        account.addRole(new Role("ROLE_USER"));
        when(accountDao.findByEmail(account.getEmail())).thenReturn(null);
        assertThrows(UsernameNotFoundException.class,
                () -> userDetailsService.loadUserByUsername(account.getEmail()).getUsername());
    }

}