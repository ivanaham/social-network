package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MembershipServiceTest {

    @Mock
    private MembershipDao membershipDao;

    @Mock
    private AccountDao accountDao;

    @Mock
    private GroupDao groupDao;

    @InjectMocks
    private MembershipService membershipService;

    private Membership getMembership(RequestStatus status, Rights rights) {
        Membership membership = new Membership();
        membership.setRights(rights);
        membership.setStatus(status);
        return membership;
    }

    @Test
    void testEmptyMembership() {
        assertEquals(new Membership(), membershipService.emptyMembership());
    }

    @Test
    void testFindByGroupIdWithStatus() {
        List<Membership> memberships = asList(
                getMembership(PENDING, COMMON),
                getMembership(PENDING, COMMON),
                getMembership(ACCEPTED, COMMON)
        );
        when(membershipDao.findByGroupId(2, PENDING, 3, 0)).thenReturn(memberships);
        assertEquals(memberships, membershipService.findByGroupId(2, PENDING, 3, 0));
    }

    @Test
    void testFindByGroupIdWithType() {
        List<Membership> memberships = asList(
                getMembership(PENDING, COMMON),
                getMembership(ACCEPTED, MODERATOR),
                getMembership(ACCEPTED, COMMON)
        );
        when(membershipDao.findByGroupId(2, ACCEPTED, MODERATOR, 3, 0))
                .thenReturn(memberships.subList(1, 2));
        assertEquals(memberships.subList(1, 2),
                membershipService.findByGroupId(2, ACCEPTED, MODERATOR, 3, 0));
    }

    @Test
    void testFindByAccountId() {
        List<Membership> memberships = asList(
                getMembership(PENDING, COMMON),
                getMembership(PENDING, COMMON),
                getMembership(ACCEPTED, COMMON)
        );
        when(membershipDao.findByAccountId(1, PENDING, 3, 0)).thenReturn(memberships);
        assertEquals(memberships, membershipService.findByAccountId(1, PENDING, 3, 0));
    }

    @Test
    void testSendMembershipRequest_WhenMembershipIsEmpty() {
        when(membershipDao.findMembership(2, 3)).thenReturn(new Membership());
        membershipService.sendMembershipRequest(2, 3);
        verify(membershipDao, times(1)).insert(getMembership(PENDING, null));
    }

    @Test
    void testSendMembershipRequestNotInserts_WhenMembershipIsPending() {
        Membership membership = getMembership(PENDING, null);
        when(membershipDao.findMembership(2, 3)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.sendMembershipRequest(2, 3));
        verify(membershipDao, never()).insert(membership);
    }

    @Test
    void testSendMembershipRequestThrows_WhenMembershipIsAccepted() {
        Membership membership = getMembership(ACCEPTED, null);
        when(membershipDao.findMembership(2, 3)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.sendMembershipRequest(2, 3));
        verify(membershipDao, never()).insert(membership);
    }

    @Test
    void testSendMembershipRequest_WhenMembershipIsDeclined() {
        Membership membership = getMembership(DECLINED, null);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        membershipService.sendMembershipRequest(1, 2);
        verify(membershipDao, times(1))
                .update(getMembership(PENDING, null));
    }

    @Test
    void testAcceptMembershipRequestTrue_WhenMembershipIsPending() {
        when(membershipDao.findMembership(1, 2))
                .thenReturn(getMembership(PENDING, null));
        membershipService.acceptMembershipRequest(1, 2);
        verify(membershipDao, times(1)).update(getMembership(ACCEPTED, COMMON));
    }

    @Test
    void testAcceptMembershipRequestFalse_WhenMembershipIsNull() {
        when(membershipDao.findMembership(1, 2)).thenReturn(new Membership());
        assertThrows(IllegalStateException.class, () -> membershipService.acceptMembershipRequest(1, 2));
    }

    @Test
    void testAcceptMembershipRequestFalse_WhenMembershipIsAccepted() {
        Membership membership = getMembership(ACCEPTED, COMMON);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.acceptMembershipRequest(1, 2));
    }

    @Test
    void testAcceptMembershipRequestFalse_WhenMembershipIsDeclined() {
        Membership membership = getMembership(DECLINED, null);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.acceptMembershipRequest(1, 2));
    }

    @Test
    void testDeclineMembershipRequest_WhenMembershipIsPending() {
        when(membershipDao.findMembership(1, 2))
                .thenReturn(getMembership(PENDING, null));
        membershipService.declineMembershipRequest(1, 2);
        verify(membershipDao, times(1)).update(getMembership(DECLINED, null));
    }

    @Test
    void testDeclineMembershipRequestFalse_WhenMembershipIsNull() {
        when(membershipDao.findMembership(1, 2)).thenReturn(new Membership());
        assertThrows(IllegalStateException.class, () -> membershipService.declineMembershipRequest(1, 2));
    }

    @Test
    void testDeclineMembershipRequestFalse_WhenMembershipIsAccepted() {
        Membership membership = getMembership(ACCEPTED, COMMON);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.declineMembershipRequest(1, 2));
    }

    @Test
    void testDeclineMembershipRequestFalse_WhenMembershipIsDeclined() {
        Membership membership = getMembership(DECLINED, null);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.declineMembershipRequest(1, 2));
    }

    @Test
    void testDeleteByAccountId() {
        membershipService.deleteByAccountId(1);
        verify(membershipDao, times(1)).deleteByAccountId(1);
    }

    @Test
    void testDeleteMember() {
        membershipService.deleteMember(1, 2);
        verify(membershipDao, times(1)).deleteMembership(1, 2);
    }

    @Test
    void testDeleteByGroupId() {
        membershipService.deleteByGroupId(1);
        verify(membershipDao, times(1)).deleteByGroupId(1);
    }

    @Test
    void testFindByIds() {
        Membership membership = getMembership(ACCEPTED, MODERATOR);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertEquals(Optional.of(membership), membershipService.findMembership(1, 2));
    }

    @Test
    void testUpdateRights_WhenMembershipIsNull() {
        when(membershipDao.findMembership(1, 2)).thenReturn(new Membership());
        assertThrows(IllegalStateException.class, () -> membershipService.updateRights(1, 2, COMMON));
    }

    @Test
    void testUpdateRights_WhenMembershipIsPending() {
        Membership membership = getMembership(PENDING, null);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.updateRights(1, 2, COMMON));
    }

    @Test
    void testUpdateRightsFalse_WhenMembershipIsDeclined() {
        Membership membership = getMembership(DECLINED, null);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        assertThrows(IllegalStateException.class, () -> membershipService.updateRights(1, 2, COMMON));
    }

    @Test
    void testUpdateRights_WhenMembershipIsAccepted() {
        Membership membership = getMembership(ACCEPTED, COMMON);
        when(membershipDao.findMembership(1, 2)).thenReturn(membership);
        membershipService.updateRights(1, 2, MODERATOR);
        verify(membershipDao, times(1)).update(getMembership(ACCEPTED, MODERATOR));
    }

}
