package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.Message.Type.ACCOUNT;
import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static java.time.LocalDateTime.of;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.AccountMessageDaoImpl;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.AccountMessage;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountMessageServiceTest {

    private static final LocalDateTime dummyDateTime = of(2022, 2, 1, 12, 20, 36);
    private static final AccountMessage dummyMsgA = new AccountMessage();
    private static final AccountMessage dummyMsgB = new AccountMessage();

    static {
        Account account = new Account();
        dummyMsgA.setId(1);
        dummyMsgA.setAuthor(account);
        dummyMsgA.setRecipient(account);
        dummyMsgA.setText("Hello!");
        dummyMsgA.setDateTimeSent(dummyDateTime);
        dummyMsgA.setId(2);
        dummyMsgB.setAuthor(account);
        dummyMsgB.setRecipient(account);
        dummyMsgB.setText("Hi");
        dummyMsgB.setDateTimeSent(dummyDateTime);
    }

    @Mock
    private AccountMessageDaoImpl accountMessageDao;

    @InjectMocks
    private AccountMessageService messageService;

    @Test
    void testFindById() {
        when(accountMessageDao.findById(dummyMsgA.getId())).thenReturn(dummyMsgA);
        assertEquals(dummyMsgA, messageService.findById(dummyMsgA.getId()));
    }

    @Test
    void testFindAll() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findAll()).thenReturn(messages);
        assertEquals(messages, messageService.findAll());
    }

    @Test
    void testFindConversationsLastMessagesByAccountId() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findLastMessagesByAccountId(1, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findLastMessagesByAccountId(1, 3, 0));
    }

    @Test
    void testFindConversationsLastMessagesByAccountIdExcluding() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findLastMessagesByAccountIdExcluding(1, 10, 3, 0))
                .thenReturn(messages);
        assertEquals(messages,
                messageService.findLastMessagesByAccountIdExcluding(1, 10, 3, 0));
    }

    @Test
    void testFindByParticipantIds() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findByParticipantIds(1, 2, DIRECT, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findByParticipantIds(1, 2, 3, 0));
    }

    @Test
    void testFindLastDirectMessage_Exist() {
        when(accountMessageDao.findByParticipantIds(1, 2, DIRECT, 1, 0))
                .thenReturn(singletonList(dummyMsgA));
        assertEquals(dummyMsgA, messageService.findLastDirectMessage(1, 2));
    }

    @Test
    void testFindLastDirectMessage_NotExist() {
        when(accountMessageDao.findByParticipantIds(1, 2, DIRECT, 1, 0))
                .thenReturn(emptyList());
        assertEquals(new AccountMessage(), messageService.findLastDirectMessage(1, 2));
    }

    @Test
    void testFindByRecipientId() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findByRecipientId(2, ACCOUNT, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findByRecipientId(2, 3, 0));
    }

    @Test
    void testFindByAuthorId() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findByAuthorId(2, ACCOUNT, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findByAuthorId(2, 3, 0));
    }

    @Test
    void testFindFeedMessages() {
        List<AccountMessage> messages = asList(dummyMsgA, dummyMsgB);
        when(accountMessageDao.findFeedMessages(1, 3, 0)).thenReturn(messages);
        assertEquals(messages, messageService.findFeedMessages(1, 3, 0));
    }

    @Test
    void testMessageCreate() {
        messageService.create(dummyMsgA);
        verify(accountMessageDao, times(1)).insert(dummyMsgA);
    }

    @Test
    void testDeleteById() {
        messageService.deleteById(dummyMsgA.getId());
        verify(accountMessageDao, times(1)).deleteById(dummyMsgA.getId());
    }

}