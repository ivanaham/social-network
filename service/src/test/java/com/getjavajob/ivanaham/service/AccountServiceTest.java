package com.getjavajob.ivanaham.service;

import static java.sql.Date.valueOf;
import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.ivanaham.dao.impl.AccountDaoImpl;
import com.getjavajob.ivanaham.dao.impl.RoleDaoImpl;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Role;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    private static final Account account = new Account();

    static {
        account.setFirstName("Mykola");
        account.setLastName("Tester");
        account.setEmail("e@mail.com");
        account.setPassword("pass");
        account.setRegisterDate(valueOf(of(2000, 1, 1)));
        account.addRole(new Role("ROLE_USER"));
    }

    @InjectMocks
    private AccountService accountService;

    @Mock
    private AccountDaoImpl accountDao;

    @Mock
    private RoleDaoImpl roleDao;

    @Test
    void testAccountCreate() {
        accountService.create(account);
        verify(accountDao, times(1)).insert(account);
    }

    @Test
    void testAccountFindById() {
        when(accountDao.findById(1)).thenReturn(account);
        assertEquals(account, accountService.findById(1));
    }

    @Test
    void testFindCurrentBirthdays() {
        List<Account> birthdayAccounts = asList(account, account, account);
        when(accountDao.findCurrentBirthdays()).thenReturn(birthdayAccounts);
        assertEquals(birthdayAccounts, accountService.findCurrentBirthdays());
    }

    @Test
    void testAccountEdit() {
        accountService.edit(account);
        verify(accountDao, times(1)).update(account);
    }

    @Test
    void testAccountDelete() {
        accountService.delete(1);
        verify(accountDao, times(1)).deleteById(1);
    }

    @Test
    void shouldGetAccountListWhenFindAll() {
        List<Account> accounts = asList(account, account, account);
        when(accountDao.findAll()).thenReturn(accounts);
        assertEquals(accounts, accountService.findAll());
    }

    @Test
    void shouldBeAbleToFindByEmail() {
        when(accountDao.findByEmail("e@mail.com")).thenReturn(account);
        assertEquals(account, accountService.findByEmail("e@mail.com"));
    }

    @Test
    void shouldBeAbleToFindByName() {
        List<Account> accounts = new ArrayList<>(singletonList(account));
        when(accountDao.findByName("myk", 1, 0)).thenReturn(accounts);
        assertEquals(accounts, accountService.findByName("myk", 1, 0));
    }

    @Test
    void testAccountResetPasswordTrue() {
        accountService.resetPasswordById(1, account.getPassword());
        verify(accountDao, times(1)).resetPasswordById(1, account.getPassword());
    }

}