package com.getjavajob.ivanaham.service;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.PhoneDao;
import com.getjavajob.ivanaham.domain.Phone;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PhoneService {

    private static final Logger LOGGER = getLogger();

    private final PhoneDao phoneDao;

    public PhoneService(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }

    public void insert(Set<Phone> phones) {
        LOGGER.debug("insert() is called; phones = {}", phones);
        phoneDao.insert(phones);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #accountId == authentication.principal.id")
    public boolean deleteByAccountId(long accountId) {
        LOGGER.debug("deleteByAccountId() is called; id = {}", accountId);
        return phoneDao.deleteByAccountId(accountId) != 0;
    }

    public Set<Phone> findByAccountId(long accountId) {
        LOGGER.debug("findByAccountId() is called; id = {}", accountId);
        return phoneDao.findByAccountId(accountId);
    }

    public void deleteById(long phoneId) {
        phoneDao.deleteById(phoneId);
    }

}
