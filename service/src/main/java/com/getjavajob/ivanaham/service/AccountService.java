package com.getjavajob.ivanaham.service;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.RoleDao;
import com.getjavajob.ivanaham.domain.Account;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountService {

    private static final Logger LOGGER = getLogger();

    private final AccountDao accountDao;
    private final RoleDao roleDao;

    public AccountService(AccountDao accountDao, RoleDao roleDao) {
        this.accountDao = accountDao;
        this.roleDao = roleDao;
    }

    public void create(Account account) {
        LOGGER.debug("create() is called: account email = {}", account.getEmail());
        account.addRole(roleDao.findByName("ROLE_USER"));
        accountDao.insert(account);
    }

    public Account findById(long id) {
        LOGGER.debug("findById() is called: id = {}", id);
        return accountDao.findById(id);
    }

    public Account findByEmail(String email) {
        LOGGER.debug("findByEmail() is called: email = {}", email);
        return accountDao.findByEmail(email);
    }

    public List<Account> findCurrentBirthdays() {
        LOGGER.debug("findCurrentBirthdays() is called");
        return accountDao.findCurrentBirthdays();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Account> findAll() {
        LOGGER.debug("findAll() is called");
        return accountDao.findAll();
    }

    public List<Account> findByName(String name, int limit, int offset) {
        LOGGER.debug("findByName() is called: name = {}, limit = {}, offset = {}", name, limit, offset);
        return accountDao.findByName(name, limit, offset);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or (#account.id == authentication.principal.id and !#account.roles.contains(@roleDaoImpl.findByName('ROLE_ADMIN')))")
    public void edit(Account account) {
        LOGGER.debug("edit() is called: account id = {}", account.getId());
        accountDao.update(account);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public void resetPasswordById(long accountId, String password) {
        LOGGER.debug("resetPasswordById() is called: account id = {}", accountId);
        accountDao.resetPasswordById(accountId, password);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #accountId == authentication.principal.id")
    public void delete(long accountId) {
        LOGGER.debug("delete() is called: account id = {}", accountId);
        accountDao.deleteById(accountId);
    }

}
