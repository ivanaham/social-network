package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.Message.Type.ACCOUNT;
import static com.getjavajob.ivanaham.domain.Message.Type.DIRECT;
import static java.util.Collections.reverse;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.AccountMessageDao;
import com.getjavajob.ivanaham.domain.AccountMessage;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountMessageService {

    private static final Logger LOGGER = getLogger();

    private final AccountMessageDao accountMessageDao;

    public AccountMessageService(AccountMessageDao accountMessageDao) {
        this.accountMessageDao = accountMessageDao;
    }

    public AccountMessage findById(long id) {
        LOGGER.debug("findById() is called: id = {}", id);
        return accountMessageDao.findById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<AccountMessage> findAll() {
        LOGGER.debug("findAll() is called");
        return accountMessageDao.findAll();
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<AccountMessage> findLastMessagesByAccountId(long accountId, int limit, int offset) {
        LOGGER.debug("findLastMessagesByAccountId() is called: id = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return accountMessageDao.findLastMessagesByAccountId(accountId, limit, offset);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<AccountMessage> findLastMessagesByAccountIdExcluding(long accountId, long accountToId, int limit, int offset) {
        LOGGER.debug("findLastMessagesByAccountIdExcluding() is called: "
                        + "accountId = {}, accountToId = {}, limit = {}, offset = {}",
                accountId, accountToId, limit, offset);
        return accountMessageDao.findLastMessagesByAccountIdExcluding(accountId, accountToId, limit, offset);
    }

    @PreAuthorize("#accountIdA == authentication.principal.id")
    public List<AccountMessage> findByParticipantIds(long accountIdA, long accountIdB, int limit, int offset) {
        LOGGER.debug("findByParticipantIds() is called: accountIdA = {}, accountIdB = {}, limit = {}, offset = {}",
                accountIdA, accountIdB, limit, offset);
        List<AccountMessage> messages = accountMessageDao.findByParticipantIds(accountIdA, accountIdB, DIRECT, limit, offset);
        reverse(messages);
        return messages;
    }

    @PreAuthorize("#accountIdA == authentication.principal.id")
    public AccountMessage findLastDirectMessage(long accountIdA, long accountIdB) {
        LOGGER.debug("findLastDirectMessage() is called: accountIdA = {}, accountIdB = {}",
                accountIdA, accountIdB);
        List<AccountMessage> messages = accountMessageDao.findByParticipantIds(accountIdA, accountIdB, DIRECT, 1, 0);
        return messages.isEmpty() ? new AccountMessage() : messages.get(0);
    }

    public List<AccountMessage> findByRecipientId(long accountId, int limit, int offset) {
        LOGGER.debug("findByRecipientId() is called: accountId = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return accountMessageDao.findByRecipientId(accountId, ACCOUNT, limit, offset);
    }

    public List<AccountMessage> findByAuthorId(long accountId, int limit, int offset) {
        LOGGER.debug("findByRecipientId() is called: accountId = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return accountMessageDao.findByAuthorId(accountId, ACCOUNT, limit, offset);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<AccountMessage> findFeedMessages(long accountId, int limit, int offset) {
        LOGGER.debug("findFeedMessages() is called: accountId = {}, limit = {}, offset = {}",
                accountId, limit, offset);
        return accountMessageDao.findFeedMessages(accountId, limit, offset);
    }

    @PreAuthorize("#message.author.id == authentication.principal.id")
    public void create(AccountMessage message) {
        LOGGER.debug("create() is called: message = {}", message);
        accountMessageDao.insert(message);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or @accountMessageService.findById(#messageId).author.id == authentication.principal.id")
    public void deleteById(long messageId) {
        LOGGER.debug("deleteById() is called: id = {}", messageId);
        accountMessageDao.deleteById(messageId);
    }

}
