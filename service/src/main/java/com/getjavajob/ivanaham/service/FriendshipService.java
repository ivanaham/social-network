package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.AccountDao;
import com.getjavajob.ivanaham.dao.interfaces.FriendshipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Friendship;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.RequestType;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FriendshipService {

    private static final Logger LOGGER = getLogger();

    private final FriendshipDao friendshipDao;
    private final AccountDao accountDao;

    public FriendshipService(FriendshipDao friendshipDao, AccountDao accountDao) {
        this.friendshipDao = friendshipDao;
        this.accountDao = accountDao;
    }

    @PreAuthorize("#senderId == authentication.principal.id")
    public void sendFriendRequest(long senderId, long recipientId) {
        LOGGER.debug("sendFriendRequest() is called: senderId = {}, recipientId = {}", senderId, recipientId);
        Friendship friendship = findFriendship(senderId, recipientId);
        if (nonNull(friendship)) {
            switch (friendship.getStatus()) {
                case DECLINED:
                    friendship.setStatus(PENDING);
                    friendship.setLastActedAccount(accountDao.findById(senderId));
                    friendshipDao.update(friendship);
                    return;
                case PENDING:
                case ACCEPTED:
                default:
                    LOGGER.error("sendFriendRequest(): senderId = {}, recipientId = {}; "
                            + "Request cannot be set to PENDING for Friendship {}", senderId, recipientId, friendship);
                    throw new IllegalStateException("Request cannot be set to PENDING for Friendship " + friendship);
            }
        }
        friendshipDao.insert(getNewFriendship(senderId, recipientId));
    }

    private Friendship getNewFriendship(long senderId, long recipientId) {
        Friendship friendship = new Friendship();
        Account sender = accountDao.findById(senderId);
        friendship.setStatus(PENDING);
        friendship.setAccountA(sender);
        friendship.setLastActedAccount(sender);
        friendship.setAccountB(accountDao.findById(recipientId));
        return friendship;
    }

    @PreAuthorize("#actionMakerId == authentication.principal.id")
    public void acceptFriendRequest(long actionMakerId, long friendId) {
        LOGGER.debug(
                "acceptFriendRequest() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        Friendship accountsFriendship = findFriendship(actionMakerId, friendId);
        RequestStatus status = nonNull(accountsFriendship) ? accountsFriendship.getStatus() : null;
        if (isNull(accountsFriendship) || status != PENDING) {
            LOGGER.error("acceptFriendRequest(): actionMakerId = {}, friendId = {}; "
                    + "Request cannot be set to ACCEPTED. Current status is {}", actionMakerId, friendId, status);
            throw new IllegalStateException("Request cannot be set to ACCEPTED. Current status is " + status);
        }
        accountsFriendship.setStatus(ACCEPTED);
        accountsFriendship.setLastActedAccount(accountDao.findById(actionMakerId));
        friendshipDao.update(accountsFriendship);
    }

    @PreAuthorize("#actionMakerId == authentication.principal.id")
    public void declineFriendRequest(long actionMakerId, long friendId) {
        LOGGER.debug("declineFriendRequest() is called: actionMakerId = {}, friendId = {}",
                actionMakerId, friendId);
        Friendship accountsFriendship = findFriendship(actionMakerId, friendId);
        RequestStatus status = nonNull(accountsFriendship) ? accountsFriendship.getStatus() : null;
        if (isNull(accountsFriendship) || status != PENDING) {
            LOGGER.error("acceptFriendRequest(): actionMakerId = {}, friendId = {}; "
                    + "Request cannot be set to DECLINED. Current status is {}", actionMakerId, friendId, status);
            throw new IllegalStateException("Request cannot be set to DECLINED. Current status is " + status);
        }
        accountsFriendship.setStatus(DECLINED);
        accountsFriendship.setLastActedAccount(accountDao.findById(actionMakerId));
        friendshipDao.update(accountsFriendship);
    }

    @PreAuthorize("#actionMakerId == authentication.principal.id")
    public boolean deleteFriend(long actionMakerId, long friendId) {
        LOGGER.debug("deleteFriend() is called: actionMakerId = {}, friendId = {}", actionMakerId, friendId);
        return friendshipDao.deleteFriendship(actionMakerId, friendId);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<Friendship> find(long accountId, RequestStatus status, int limit, int offset) {
        LOGGER.debug("find() is called: accountId = {}, status = {}, limit = {}, offset = {}",
                accountId, status, limit, offset);
        return friendshipDao.find(accountId, status, limit, offset);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<Friendship> find(long accountId, RequestStatus status) {
        LOGGER.debug("find() is called: accountId = {}, status = {}", accountId, status);
        return friendshipDao.find(accountId, status);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<Friendship> findPending(long accountId, RequestType type, int limit, int offset) {
        LOGGER.debug("findPending() is called: accountId = {}, type = {}, limit = {}, offset = {}",
                accountId, type, limit, offset);
        return friendshipDao.findPending(accountId, type, limit, offset);
    }

    @PreAuthorize("#currentUserId == authentication.principal.id")
    public Friendship findFriendship(long currentUserId, long friendId) {
        LOGGER.debug("findFriendship() is called: currentUserId = {}, accountIdB = {}", currentUserId, friendId);
        return friendshipDao.findFriendship(currentUserId, friendId);
    }

}
