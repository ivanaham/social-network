package com.getjavajob.ivanaham.service;

import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.GroupMessageDao;
import com.getjavajob.ivanaham.domain.GroupMessage;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupMessageService {

    private static final Logger LOGGER = getLogger();

    private final GroupMessageDao groupMessageDao;

    public GroupMessageService(GroupMessageDao groupMessageDao) {
        this.groupMessageDao = groupMessageDao;
    }

    public GroupMessage findById(long id) {
        LOGGER.debug("findById() is called: id = {}", id);
        return groupMessageDao.findById(id);
    }

    @PreAuthorize("@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() "
            + "matches 'MODERATOR' or 'COMMON'")
    public List<GroupMessage> findByRecipientId(long groupId, int limit, int offset) {
        LOGGER.debug("findByRecipientId() is called: groupId = {}, limit = {}, offset = {}",
                groupId, limit, offset);
        return groupMessageDao.findByRecipientId(groupId, limit, offset);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<GroupMessage> findAll() {
        LOGGER.debug("findAll() is called");
        return groupMessageDao.findAll();
    }

    @PreAuthorize("@membershipService.findMembership(#message.author.id, #message.recipient.id).orElse(@membershipService.emptyMembership()).rights.name() matches "
            + "'MODERATOR' or 'COMMON'")
    public void create(GroupMessage message) {
        LOGGER.debug("create() is called: message = {}", message);
        groupMessageDao.insert(message);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or @groupMessageService.findById(#messageId).author.id == authentication.principal.id")
    public void deleteById(long messageId) {
        LOGGER.debug("deleteById() is called: id = {}", messageId);
        groupMessageDao.deleteById(messageId);
    }

}
