package com.getjavajob.ivanaham.service;

import com.getjavajob.ivanaham.dao.DataConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@Import(DataConfig.class)
@ComponentScan("com.getjavajob.ivanaham.service")
public class ServiceConfig {

    @Bean
    public TransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

}
