package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.DECLINED;
import static com.getjavajob.ivanaham.domain.enums.RequestStatus.PENDING;
import static com.getjavajob.ivanaham.domain.enums.Rights.COMMON;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Membership;
import com.getjavajob.ivanaham.domain.enums.RequestStatus;
import com.getjavajob.ivanaham.domain.enums.Rights;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MembershipService {

    private static final Logger LOGGER = getLogger();
    private static final Membership EMPTY_MEMBERSHIP = new Membership();
    private final MembershipDao membershipDao;

    public MembershipService(MembershipDao membershipDao) {
        this.membershipDao = membershipDao;
    }

    public Membership emptyMembership() {
        return EMPTY_MEMBERSHIP;
    }

    public List<Membership> findByGroupId(long groupId, RequestStatus status, int limit, int offset) {
        LOGGER.debug("findByGroupId() is called: groupId = {}, status = {}, limit = {}, offset = {}",
                groupId, status, limit, offset);
        return membershipDao.findByGroupId(groupId, status, limit, offset);
    }

    public List<Membership> findByGroupId(
            long groupId, RequestStatus status, Rights rights, int limit, int offset) {
        LOGGER.debug("findByGroupId() is called: groupId = {}, status = {}, rights = {}, limit = {}, offset = {}",
                groupId, status, rights, limit, offset);
        return membershipDao.findByGroupId(groupId, status, rights, limit, offset);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public List<Membership> findByAccountId(long accountId, RequestStatus status, int limit, int offset) {
        LOGGER.debug("findByAccountId() is called: accountId = {}, status = {}, pageable = {}",
                accountId, status, limit);
        return membershipDao.findByAccountId(accountId, status, limit, offset);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public void sendMembershipRequest(long accountId, long groupId) {
        LOGGER.debug("sendMembershipRequest() is called: accountId = {}, groupId = {}", accountId, groupId);
        Membership membership = findMembership(accountId, groupId).orElse(EMPTY_MEMBERSHIP);
        if (membership.equals(EMPTY_MEMBERSHIP)) {
            membershipDao.insert(getNewMembership(accountId, groupId));
            return;
        }
        switch (membership.getStatus()) {
            case DECLINED:
                membership.setStatus(PENDING);
                membershipDao.update(membership);
                return;
            case PENDING:
            case ACCEPTED:
            default:
                LOGGER.error("sendMembershipRequest(): accountId = {}, groupId = {}; Request cannot be set "
                        + "to PENDING for Membership {}", accountId, groupId, membership);
                throw new IllegalStateException("Request cannot be set to PENDING for Membership " + membership);
        }
    }

    private Membership getNewMembership(long accountId, long groupId) {
        Membership membership = new Membership();
        Account account = new Account();
        account.setId(accountId);
        membership.setAccount(account);
        Group group = new Group();
        group.setId(groupId);
        membership.setGroup(group);
        membership.setStatus(PENDING);
        return membership;
    }

    @PreAuthorize("@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() eq 'MODERATOR'")
    public void acceptMembershipRequest(long accountId, long groupId) {
        LOGGER.debug("acceptMembershipRequest() is called: accountId = {}, groupId = {}", accountId, groupId);
        Membership membership = findMembership(accountId, groupId).orElse(EMPTY_MEMBERSHIP);
        RequestStatus status = membership.getStatus();
        if (status != PENDING) {
            LOGGER.error("acceptMembershipRequest(): accountId = {}, groupId = {}; "
                    + "Request cannot be set to ACCEPTED. membership = {}", accountId, groupId, membership);
            throw new IllegalStateException("Request cannot be set to ACCEPTED. membership = " + membership);
        }
        membership.setStatus(ACCEPTED);
        membership.setRights(COMMON);
        membershipDao.update(membership);
    }

    @PreAuthorize("#accountId == authentication.principal.id or "
            + "@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() eq 'MODERATOR'")
    public void declineMembershipRequest(long accountId, long groupId) {
        LOGGER.debug("declineMembershipRequest() is called: accountId = {}, groupId = {}", accountId, groupId);
        Membership membership = findMembership(accountId, groupId).orElse(EMPTY_MEMBERSHIP);
        RequestStatus status = membership.getStatus();
        if (status != PENDING) {
            LOGGER.error("declineMembershipRequest(): accountId = {}, groupId = {}; "
                    + "Request cannot be set to DECLINED. membership = {}", accountId, groupId, membership);
            throw new IllegalStateException("Request cannot be set to DECLINED. membership = " + membership);
        }
        membership.setStatus(DECLINED);
        membershipDao.update(membership);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or "
            + "@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() eq 'MODERATOR'")
    public void updateRights(long accountId, long groupId, Rights accountRights) {
        LOGGER.debug("updateRights() is called: accountId = {}, groupId = {}, rights = {}",
                accountId, groupId, accountRights);
        Membership membership = findMembership(accountId, groupId).orElse(EMPTY_MEMBERSHIP);
        RequestStatus status = membership.getStatus();
        if (status != ACCEPTED) {
            LOGGER.error("updateRights(): accountId = {}, groupId = {}, rights = {}; "
                            + "Rights cannot be updated as user didn't join the group. membership = {}",
                    accountId, groupId, accountRights, membership);
            throw new IllegalStateException("Rights cannot be updated as user didn't join the group. membership = "
                    + membership);
        }
        membership.setRights(accountRights);
        membershipDao.update(membership);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN' or authentication.principal.id == #accountId)")
    public void deleteByAccountId(long accountId) {
        LOGGER.debug("deleteByAccountId(): id = {}", accountId);
        membershipDao.deleteByAccountId(accountId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or "
            + "@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() eq 'MODERATOR'")
    public void deleteByGroupId(long groupId) {
        LOGGER.debug("deleteByGroupId(): id = {}", groupId);
        membershipDao.deleteByGroupId(groupId);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or #accountId == authentication.principal.id or "
            + "@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() eq 'MODERATOR'")
    public void deleteMember(long accountId, long groupId) {
        LOGGER.debug("deleteMember(): accountId = {}, groupId = {}", accountId, groupId);
        membershipDao.deleteMembership(accountId, groupId);
    }

    @PreAuthorize("#accountId == authentication.principal.id")
    public Optional<Membership> findMembership(long accountId, long groupId) {
        LOGGER.debug("findMembership(): accountId = {}, groupId = {}", accountId, groupId);
        return Optional.ofNullable(membershipDao.findMembership(accountId, groupId));
    }

}
