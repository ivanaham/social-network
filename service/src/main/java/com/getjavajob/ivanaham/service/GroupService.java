package com.getjavajob.ivanaham.service;

import static com.getjavajob.ivanaham.domain.enums.RequestStatus.ACCEPTED;
import static com.getjavajob.ivanaham.domain.enums.Rights.MODERATOR;
import static org.apache.logging.log4j.LogManager.getLogger;

import com.getjavajob.ivanaham.dao.interfaces.GroupDao;
import com.getjavajob.ivanaham.dao.interfaces.MembershipDao;
import com.getjavajob.ivanaham.domain.Account;
import com.getjavajob.ivanaham.domain.Group;
import com.getjavajob.ivanaham.domain.Membership;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupService {

    private static final Logger LOGGER = getLogger();

    private final GroupDao groupDao;
    private final MembershipDao membershipDao;

    public GroupService(GroupDao groupDao, MembershipDao membershipDao) {
        this.groupDao = groupDao;
        this.membershipDao = membershipDao;
    }

    public void create(Group group) {
        LOGGER.debug("create() is called: group name = {}", group.getName());
        groupDao.insert(group);
        long creatorId = group.getCreator().getId();
        Membership creatorMembership = new Membership();
        Account creator = new Account();
        creator.setId(creatorId);
        creatorMembership.setAccount(creator);
        creatorMembership.setGroup(group);
        creatorMembership.setStatus(ACCEPTED);
        creatorMembership.setRights(MODERATOR);
        membershipDao.insert(creatorMembership);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or "
            + "@membershipService.findMembership(authentication.principal.id, #group.id).orElse(@membershipService.emptyMembership()).rights.name() == 'MODERATOR'")
    public void edit(Group group) {
        LOGGER.debug("edit() is called: group id = {}", group.getId());
        groupDao.update(group);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or "
            + "@membershipService.findMembership(authentication.principal.id, #groupId).orElse(@membershipService.emptyMembership()).rights.name() == 'MODERATOR'")
    public void delete(long groupId) {
        LOGGER.debug("delete() is called: group id = {}", groupId);
        groupDao.deleteById(groupId);
    }

    public Group findById(long id) {
        LOGGER.debug("findById() is called: id = {}", id);
        return groupDao.findById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Group> findAll() {
        LOGGER.debug("findAll() is called");
        return groupDao.findAll();
    }

    public List<Group> findByName(String name, int limit, int offset) {
        LOGGER.debug("findByName() is called: name = {}, limit = {}, offset = {}", name, limit, offset);
        return groupDao.findByName(name, limit, offset);
    }

}
